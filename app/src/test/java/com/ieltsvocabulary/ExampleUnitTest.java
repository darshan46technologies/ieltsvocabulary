package com.ieltsvocabulary;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit phonetic_word_item, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">HomeScreen documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }
}