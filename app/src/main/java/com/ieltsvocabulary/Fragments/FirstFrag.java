package com.ieltsvocabulary.Fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Activity.CrossWordScreen;
import com.ieltsvocabulary.Activity.GapFillScreen;
import com.ieltsvocabulary.Activity.ListeningScreen;
import com.ieltsvocabulary.Activity.MatchTheMeaning;
import com.ieltsvocabulary.Activity.MatchThePairsScreen;
import com.ieltsvocabulary.Activity.MissingLettters;
import com.ieltsvocabulary.Activity.PronunciationScreen;
import com.ieltsvocabulary.Activity.SubjectScreen;
import com.ieltsvocabulary.Activity.TrueFalseScreen;
import com.ieltsvocabulary.Activity.WordType;
import com.ieltsvocabulary.Adapter.DataAdapter;

import com.ieltsvocabulary.Interface.AdapterItemClickCallback;
import com.ieltsvocabulary.Interface.FrgPositionAnimation;
import com.ieltsvocabulary.R;
import com.ieltsvocabulary.Activity.TranslationScreen;


/**
 * A simple {@link Fragment} subclass.
 */
public class FirstFrag extends Fragment implements View.OnClickListener {

    RelativeLayout relMain,relImageWithTitle,relInDetailCard;
    ImageView imgBackgroundImage,imgCenterIcon,imgDetilsCard;
    TextView txtTile,txtDetailsCard,txtTitle,txtWordTitle,txtCount;
    CardView crdDetails,crdRecyclerViewWithTitle,crdTasks,crdDictionary,crdPronunciation,
            crdListning,crdTrueFalse,crdCrossword,crdWordType,crdSpelling,crdMatching,crdMatchPair,crdGapFill,crdStart;
    RecyclerView rcyData;

    DataAdapter dataAdapter;



    Animation animation, animationtwo, animationthree;
    public Context context;


    public FirstFrag() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_first, container, false);


        findViewByid(view);
        init(view);
        setListner(view);

        crdRecyclerViewWithTitle.setVisibility(View.GONE);
        crdTasks.setVisibility(View.GONE);
        crdStart.setVisibility(View.GONE);


        imgBackgroundImage.setImageResource(getArguments().getInt("image"));
        txtDetailsCard.setText(getArguments().getInt("title"));
        imgCenterIcon.setImageResource(getArguments().getInt("smallimg"));
        txtTile.setText(getArguments().getString("msg"));
//        txtuptitle.setText(getArguments().getString("msg"));

        return view;


    }

    private void findViewByid(View view) {

        relMain = view.findViewById(R.id.relMain);
        relImageWithTitle = view.findViewById(R.id.relImageWithTitle);
        imgBackgroundImage = view.findViewById(R.id.imgBackgroundImage);
        imgCenterIcon = view.findViewById(R.id.imgCenterIcon);
        txtTile = view.findViewById(R.id.txtTile);
        crdDetails = view.findViewById(R.id.crdDetails);
        relInDetailCard = view.findViewById(R.id.relInDetailCard);
        imgDetilsCard = view.findViewById(R.id.imgDetilsCard);
        txtDetailsCard = view.findViewById(R.id.txtDetailsCard);
        crdRecyclerViewWithTitle = view.findViewById(R.id.crdRecyclerViewWithTitle);
        txtTitle = view.findViewById(R.id.txtTitle);
        txtWordTitle = view.findViewById(R.id.txtWordTitle);
        txtCount = view.findViewById(R.id.txtCount);
        rcyData = view.findViewById(R.id.rcyData);
        crdTasks = view.findViewById(R.id.crdTasks);
        crdDictionary = view.findViewById(R.id.crdDictionary);
        crdPronunciation = view.findViewById(R.id.crdPronunciation);
        crdListning = view.findViewById(R.id.crdListning);
        crdTrueFalse = view.findViewById(R.id.crdTrueFalse);
        crdCrossword = view.findViewById(R.id.crdCrossword);
        crdWordType = view.findViewById(R.id.crdWordType);
        crdSpelling = view.findViewById(R.id.crdSpelling);
        crdMatching = view.findViewById(R.id.crdMatching);
        crdMatchPair = view.findViewById(R.id.crdMatchPair);
        crdGapFill = view.findViewById(R.id.crdGapFill);
        crdStart = view.findViewById(R.id.crdStart);

    }

    private void init(View view) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rcyData.setLayoutManager(linearLayoutManager);

//        dataAdapter = new DataAdapter(getActivity(), new AdapterItemClickCallback() {
//            @Override
//            public void response(Object o, int type) {
//            }
//        });
        rcyData.setAdapter(dataAdapter);
    }


    private void setListner(View view) {

        imgBackgroundImage.setOnClickListener(this);
//        imgplay.setOnClickListener(this);
        crdStart.setOnClickListener(this);


        crdDictionary.setOnClickListener(this);
        crdPronunciation.setOnClickListener(this);
        crdListning.setOnClickListener(this);
        crdTrueFalse.setOnClickListener(this);
        crdCrossword.setOnClickListener(this);
        crdWordType.setOnClickListener(this);
        crdSpelling.setOnClickListener(this);
        crdMatching.setOnClickListener(this);
        crdMatchPair.setOnClickListener(this);
        crdGapFill.setOnClickListener(this);

        // TODO: Ready Animation........
        ((SubjectScreen) getActivity()).setinterface(new FrgPositionAnimation() {
            @Override
            public void frgposition(String flag, int currentPosition) {

                if (flag.equalsIgnoreCase("start")) {
                    if (SubjectScreen.vp.getCurrentItem() == currentPosition)
                        crdDetails.setVisibility(View.GONE);

                    animation = AnimationUtils.loadAnimation(getActivity(), R.anim.scale_animation_up);

                    animationtwo = AnimationUtils.loadAnimation(getActivity(), R.anim.cross_up);
                    imgCenterIcon.startAnimation(animationtwo);


                    imgBackgroundImage.startAnimation(animation);

                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            crdRecyclerViewWithTitle.setVisibility(View.VISIBLE);
                            crdTasks.setVisibility(View.GONE);
                            crdStart.setVisibility(View.VISIBLE);
                            txtTile.setVisibility(View.GONE);


                            dataAdapter.notifyDataSetChanged();
                            crdRecyclerViewWithTitle.scheduleLayoutAnimation();

                            animationthree = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in_with_scorll_up);
                            crdRecyclerViewWithTitle.startAnimation(animationthree);
                            animationthree.setAnimationListener(new Animation.AnimationListener() {
                                @Override
                                public void onAnimationStart(Animation animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {

                                    crdRecyclerViewWithTitle.setAnimation(null);
//                                    txtuptitle.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {

                                }
                            });


                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            imgBackgroundImage.setClickable(false);
                            crdRecyclerViewWithTitle.setClickable(true);
                            crdStart.setClickable(true);


                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                }
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (this.isVisible()) {
            if (isVisibleToUser) {
                onResume();
            }
        }
    }


    public static FirstFrag newInstance(int image, int title, int msg, int smallimg) {

        FirstFrag fragment = new FirstFrag();
        Bundle args = new Bundle();
        args.putInt("image", image);
        args.putInt("title", title);
        args.putInt("msg", msg);
        args.putInt("smallimg", smallimg);

        fragment.setArguments(args);
        return fragment;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBackgroundImage:
                break;

            case R.id.crdStart:
                crdTasks.setVisibility(View.VISIBLE);
                crdRecyclerViewWithTitle.setVisibility(View.GONE);
                animationthree = AnimationUtils.loadAnimation(getActivity(),
                        R.anim.fade_in_with_scorll_up);
                crdTasks.startAnimation(animationthree);

                animationthree.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                        crdTasks.setAnimation(null);
//                        crdstart.setClickable(false);
                        crdStart.setVisibility(View.GONE);

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                break;


            case R.id.crdDictionary:
                Intent intent = new Intent(getActivity(), TranslationScreen.class);
                startActivity(intent);
                break;
            case R.id.crdPronunciation:
                Intent pronuciaation = new Intent(getActivity(), PronunciationScreen.class);
                startActivity(pronuciaation);
                break;
            case R.id.crdListning:
                Intent listningintent = new Intent(getActivity(), ListeningScreen.class);
                startActivity(listningintent);
                break;
            case R.id.crdTrueFalse:
                Intent truefalseintent = new Intent(getActivity(), TrueFalseScreen.class);
                startActivity(truefalseintent);
                break;
            case R.id.crdCrossword:
                Intent crosswordintent = new Intent(getActivity(), CrossWordScreen.class);
                startActivity(crosswordintent);
                break;
            case R.id.crdWordType:
                Intent wordType = new Intent(getActivity(), WordType.class);
                startActivity(wordType);
                break;
            case R.id.crdSpelling:
                Intent translateintent = new Intent(getActivity(), MissingLettters.class);
                startActivity(translateintent);
                break;
            case R.id.crdMatching:
                Intent matchmeaningintent = new Intent(getActivity(), MatchTheMeaning.class);
                startActivity(matchmeaningintent);
                break;

            case R.id.crdMatchPair:
                Intent matchpairintent = new Intent(getActivity(), MatchThePairsScreen.class);
                startActivity(matchpairintent);
                break;

            case R.id.crdGapFill:
                Intent selectgap = new Intent(getActivity(), GapFillScreen.class);
                startActivity(selectgap);
                break;

        }
    }

}
