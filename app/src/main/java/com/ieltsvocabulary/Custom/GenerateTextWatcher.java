package com.ieltsvocabulary.Custom;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.ieltsvocabulary.R;

public class GenerateTextWatcher implements TextWatcher {
    private View view;

    public GenerateTextWatcher(View view) {
        this.view = view;

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String text = s.toString();

        switch (view.getId()) {
            case R.id.edtOne:
                if (text.length() == 1)
                    view.requestFocus();
                break;
            case R.id.edtTwo:
                if (text.length() == 1)
                    view.requestFocus();
                break;

            case R.id.edtThree:
                if (text.length() == 1)
                    view.requestFocus();
                break;

            case R.id.edtFour:
                if (text.length() == 1)
                    view.requestFocus();
                break;

            case R.id.edtFive:
                if (text.length() == 1)
                    view.requestFocus();
                break;

            case R.id.edtSix:
                if (text.length() == 1)
                    view.requestFocus();
                break;
        }


    }
}
