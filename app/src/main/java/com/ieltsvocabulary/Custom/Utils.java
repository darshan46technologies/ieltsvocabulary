package com.ieltsvocabulary.Custom;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Adapter.HintItemAdapter;
import com.ieltsvocabulary.Adapter.PhoneticWordAdapter;
import com.ieltsvocabulary.Interface.AdapterItemClickCallback;
import com.ieltsvocabulary.Model.HintItemModel;
import com.ieltsvocabulary.Model.PhoneticWordModel;
import com.ieltsvocabulary.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Utils {

    Context context;
    Dialog dialog;
    RecyclerView words_recycler;
    TextView consonants, dipthongs, long_vovels, sort_vovels;
    ArrayList<PhoneticWordModel> phoneticWordArrayList;
    ArrayList<HintItemModel> hintItemLists;
    RecyclerView rcyHintItem;
    TextView txtTitle;
    HintItemAdapter hintItemAdapter;

    public Utils(Context context) {
        this.context = context;
    }

    public void showDialogBox(int layout) {

        dialog = new Dialog(context);
        dialog.setContentView(layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView textView = dialog.findViewById(R.id.tryagain);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());

        Point size = new Point();
        ((Activity) context).getWindowManager().getDefaultDisplay().getSize(size);
        int display_width = size.x;
        int display_height = size.y;

        lp.width = (display_width * 80) / 100;
        lp.height = (display_height * 80) / 100;
        lp.gravity = Gravity.CENTER;
//                lp.verticalMargin = 10;
        dialog.getWindow().setAttributes(lp);

        dialog.show();
    }

    public void dismissDialog() {
        dialog.dismiss();
    }

    public boolean isShowingDialog() {
        if (dialog != null) {
            return dialog.isShowing();
        } else return false;
    }

    public void showTranscriptKeyboard(final EditText editText) {
        dialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View viewLay = LayoutInflater.from(context).inflate(R.layout.keyboard_transcription, null);
        dialog.setContentView(viewLay);
        dialog.setCancelable(true);
        ImageView imgClose, imgBack;
        words_recycler = viewLay.findViewById(R.id.words_recycler);
        sort_vovels = viewLay.findViewById(R.id.sort_vovels);
        long_vovels = viewLay.findViewById(R.id.long_vovels);
        consonants = viewLay.findViewById(R.id.consonants);
        dipthongs = viewLay.findViewById(R.id.dipthongs);
        imgClose = viewLay.findViewById(R.id.imgClose);
        imgBack = viewLay.findViewById(R.id.imgBack);

        setKeyboardData(0, editText);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!editText.getText().toString().isEmpty()) {
                    if (editText.getText().toString().length() > 1) {
                        editText.setSelection(editText.getText().toString().length());
                    }
                    editText.setText(editText.getText().toString().substring(0, editText.getText().toString().length() - 1));
                }
            }
        });

        imgBack.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                editText.getText().clear();
                return true;
            }
        });
        sort_vovels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setKeyboardData(0, editText);
            }
        });
        long_vovels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setKeyboardData(1, editText);
            }
        });
        consonants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setKeyboardData(3, editText);
            }
        });
        dipthongs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setKeyboardData(2, editText);
            }
        });
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();
    }

    private void setKeyboardData(int i, final EditText editText) {
        int colunms = 2;
        phoneticWordArrayList = new ArrayList<>();
        sort_vovels.setTextColor(context.getResources().getColor(R.color.gray));
        long_vovels.setTextColor(context.getResources().getColor(R.color.gray));
        dipthongs.setTextColor(context.getResources().getColor(R.color.gray));
        consonants.setTextColor(context.getResources().getColor(R.color.gray));

        switch (i) {
            case 0:
                sort_vovels.setTextColor(context.getResources().getColor(R.color.green));
                phoneticWordArrayList.add(new PhoneticWordModel("æ", R.drawable.ic_bag));
                phoneticWordArrayList.add(new PhoneticWordModel("e", R.drawable.ic_bread));
                phoneticWordArrayList.add(new PhoneticWordModel("ɒ", R.drawable.ic_hot_dog));
                phoneticWordArrayList.add(new PhoneticWordModel("I", R.drawable.ic_fish));
                phoneticWordArrayList.add(new PhoneticWordModel("ʊ", R.drawable.ic_book));
                phoneticWordArrayList.add(new PhoneticWordModel("ə", R.drawable.ic_banana));
                phoneticWordArrayList.add(new PhoneticWordModel("ʌ", R.drawable.ic_sun));
                break;
            case 1:
                colunms = 1;
                long_vovels.setTextColor(context.getResources().getColor(R.color.green));
                phoneticWordArrayList.add(new PhoneticWordModel("i:", R.drawable.ic_cheese));
                phoneticWordArrayList.add(new PhoneticWordModel("ɑ:", R.drawable.ic_car));
                phoneticWordArrayList.add(new PhoneticWordModel("ɜ:", R.drawable.ic_skirt));
                phoneticWordArrayList.add(new PhoneticWordModel("ɔ:", R.drawable.ic_door));
                phoneticWordArrayList.add(new PhoneticWordModel("u:", R.drawable.ic_shoe));
                break;
            case 2:
                dipthongs.setTextColor(context.getResources().getColor(R.color.green));
                phoneticWordArrayList.add(new PhoneticWordModel("eɪ", R.drawable.ic_eight));
                phoneticWordArrayList.add(new PhoneticWordModel("aɪ", R.drawable.ic_eye));
                phoneticWordArrayList.add(new PhoneticWordModel("əʊ", R.drawable.ic_phone));
                phoneticWordArrayList.add(new PhoneticWordModel("aʊ", R.drawable.ic_house));
                phoneticWordArrayList.add(new PhoneticWordModel("eə", R.drawable.ic_chair));
                phoneticWordArrayList.add(new PhoneticWordModel("ɪə", R.drawable.ic_ear));
                phoneticWordArrayList.add(new PhoneticWordModel("ɔɪ", R.drawable.ic_boy));
                break;
            case 3:
                colunms = 3;
                consonants.setTextColor(context.getResources().getColor(R.color.green));
                phoneticWordArrayList.add(new PhoneticWordModel("d", R.drawable.ic_door_two));
                phoneticWordArrayList.add(new PhoneticWordModel("s", R.drawable.ic_steps));
                phoneticWordArrayList.add(new PhoneticWordModel("z", R.drawable.ic_zebra));
                phoneticWordArrayList.add(new PhoneticWordModel("ʃ", R.drawable.ic_shoef));
                phoneticWordArrayList.add(new PhoneticWordModel("ʒ", R.drawable.ic_treasure));
                phoneticWordArrayList.add(new PhoneticWordModel("θ", R.drawable.ic_bath));
                phoneticWordArrayList.add(new PhoneticWordModel("ð", R.drawable.ic_feather));
                phoneticWordArrayList.add(new PhoneticWordModel("ʈʃ", R.drawable.ic_chicken));
                phoneticWordArrayList.add(new PhoneticWordModel("dʒ", R.drawable.ic_jeans));
                phoneticWordArrayList.add(new PhoneticWordModel("h", R.drawable.ic_heart));
                phoneticWordArrayList.add(new PhoneticWordModel("l", R.drawable.ic_lemon));
                phoneticWordArrayList.add(new PhoneticWordModel("r", R.drawable.ic_rose));
                phoneticWordArrayList.add(new PhoneticWordModel("w", R.drawable.ic_watch));
                phoneticWordArrayList.add(new PhoneticWordModel("j", R.drawable.ic_yoghurt));
                phoneticWordArrayList.add(new PhoneticWordModel("m", R.drawable.ic_mountain));
                phoneticWordArrayList.add(new PhoneticWordModel("n", R.drawable.ic_nurse));
                phoneticWordArrayList.add(new PhoneticWordModel("ŋ", R.drawable.ic_ring));
                break;
        }

        words_recycler.setLayoutManager(new GridLayoutManager(context, 5, GridLayoutManager.VERTICAL, false));
        words_recycler.setAdapter(new PhoneticWordAdapter(context, phoneticWordArrayList, new AdapterItemClickCallback() {
            @Override
            public void response(Object o, int type) {

                PhoneticWordModel model = (PhoneticWordModel) o;
                if (editText.getText().length() > 1) {
                    editText.setSelection(editText.getText().toString().length());
                }
                editText.setText("" + editText.getText().toString().trim() + model.getWord());
            }
        }));
    }

    public void setInJSONFormate(ViewGroup lnrHintView, Context context) {

        JSONArray jsonArray = new JSONArray();

        try {

            for (int a = 0; a < 5; a++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("option", a + 1);
                jsonObject.put("optionValue", "Hello");
                jsonArray.put(jsonObject);

            }
            Log.e("data", String.valueOf(jsonArray));

            usedRecyclerViewDialog(lnrHintView, context, jsonArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void usedRecyclerViewDialog(ViewGroup view, Context diffContext, JSONArray jsonArray) {

        LayoutInflater layoutInflater = (LayoutInflater) diffContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View getView = layoutInflater.inflate(R.layout.row_recycler_view_list, null);

        findViewByid(getView, diffContext, jsonArray);
        init(view, diffContext, jsonArray);
        setLisrner(view, diffContext, jsonArray);


        view.addView(getView);
    }

    private void findViewByid(View view, Context diffContext, JSONArray jsonArray) {
        rcyHintItem = view.findViewById(R.id.rcyHintItem);
        txtTitle = view.findViewById(R.id.txtTitle);
    }

    private void init(View view, Context diffContext, JSONArray jsonArray) {
        hintItemLists = new ArrayList<>();
        rcyHintItem.setLayoutManager(new LinearLayoutManager(diffContext));
        txtTitle.setText("Notes");

        if (jsonArray.length() > 0) {
            for (int a = 0; a < jsonArray.length(); a++) {
                try {
                    JSONObject jsonObject = jsonArray.getJSONObject(a);

                    String option = jsonObject.getString("option");
                    String optionValue = jsonObject.getString("optionValue");
                    hintItemLists.add(new HintItemModel(option, optionValue));
//                    for (Iterator<String> data = jsonObject.keys();data.hasNext();){
//                        String value = data.next();
//                        Log.e("Value",jsonObject.getString(value));
//
//                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            hintItemAdapter = new HintItemAdapter(diffContext, hintItemLists);
            rcyHintItem.setAdapter(hintItemAdapter);
        }

    }

    private void setLisrner(View view, Context diffContext, JSONArray jsonArray) {
    }

    /*public void setVisiblity(View viewOne, View viewTwo, boolean elseFlag) {
        if (viewOne.getVisibility() == View.VISIBLE){
            viewOne.setVisibility(View.GONE);
            viewTwo.setVisibility(View.VISIBLE);
        }else{
            if (elseFlag){
                viewOne.setVisibility(View.VISIBLE);
                viewTwo.setVisibility(View.GONE);
            }
        }
    }*/

    public void openKeyboard(EditText editText){
        editText.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }
}
