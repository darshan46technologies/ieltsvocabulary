package com.ieltsvocabulary.Custom;

import android.util.Log;
import android.view.View;

import androidx.viewpager.widget.ViewPager;

public class ZoomPageTransformer implements ViewPager.PageTransformer {
    private static final float MIN_SCALE = 0.85f;

    public void transformPage(View view, float position) {
        int pageWidth = view.getWidth();
        int pageHeight = view.getHeight();

        if (position <= 1) {
            float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
            float vertMargin = pageHeight * (1 - scaleFactor) / 2;
            float horzMArgin = pageWidth * (1 - scaleFactor) / 2;

            if (position < 0) {
                view.setTranslationX(horzMArgin - vertMargin / 2);
//                view.setTranslationX(pageWidth * -position);
//                float scaleFactors = MIN_SCALE
//                        + (1 - MIN_SCALE) * (1 - Math.abs(position));
//                view.setScaleX(scaleFactors);
//                view.setScaleY(scaleFactors);
                Log.e("motu", String.valueOf(view));
            } else {
                view.setTranslationX(-horzMArgin + vertMargin / 2);
//                view.setScaleX(scaleFactor);
//                view.setScaleY(scaleFactor);
                Log.e("nanu", String.valueOf(view));
            }

            view.setScaleX(scaleFactor);
            view.setScaleY(scaleFactor);

        }

    }
}
