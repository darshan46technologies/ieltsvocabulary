package com.ieltsvocabulary.Custom;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;

import com.ieltsvocabulary.Interface.AnimationEndListener;

public class AnimationClass {

    private Context context;
    private float startX, endX, startY, endY, fromXstart, toXend, fromYstart, toYend;

    private String msges;
    private int duration, secdura;
    private View views, firstView, secondView;
    private Drawable layoutBackGround;
    boolean fillAfters, listners;
    int visibility;
    ObjectAnimator objectAnimator;

    private TranslateAnimation translateAnimation;
    int width;


    public AnimationClass() {
    }

    public AnimationClass(Context context) {
        this.context = context;
       /* if (i == 1){
            displayMetrics = new DisplayMetrics();

            windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);

            width = displayMetrics.widthPixels;
        }else{

        }
        */
    }

    public void viewVisibilityAndSet(View firstView, View secondView, Drawable layout_true_color) {

        this.firstView = firstView;
        this.secondView = secondView;
        this.layoutBackGround = layout_true_color;
//        this.visibility = visibility;

    }

    public void startSecondAnimation(float fromX, float toX, float fromY,
                                     float toY, int durations, String msg, View view) {
        this.startX = fromX;
        this.endX = toX;
        this.startY = fromY;
        this.endY = toY;
        this.duration = durations;
        this.msges = msg;
        this.views = view;
    }


    public void moveTranslateAnimation(float fromX, float toX, float fromY, float toY
            , final int duration, final boolean fillAfter, final boolean endTime, final boolean startTime, boolean repeatTime,
                                       final View view, final boolean viewVisible,
                                       final AnimationEndListener animationEndListener) {


        translateAnimation = new TranslateAnimation(fromX, toX, fromY, toY);
        translateAnimation.setDuration(duration);
        translateAnimation.setFillAfter(fillAfter);

        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if (startTime) {
                    animationEndListener.onAnimationEnd();

                    if (viewVisible) {
                        setBackgroundOnView();
                    }
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (endTime) {
                    animationEndListener.onAnimationEnd();
                    if (viewVisible) {
                        setBackgroundOnView();
                    }
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        view.startAnimation(translateAnimation);

    }


    public void scaleAnimation(View view, float startX, float endX, float startY, float endY,
                               float pivotX, float pivotY, int duration, boolean fillAfter,
                               final boolean startTime, final boolean endTime, final boolean repeatTime
            , final boolean viewVisible, final AnimationEndListener animationEndListener) {

        ScaleAnimation scaleAnimation = new ScaleAnimation(startX, endX, startY, endY,
                Animation.RELATIVE_TO_SELF, pivotX, Animation.RELATIVE_TO_SELF, pivotY);
        scaleAnimation.setDuration(duration);
        scaleAnimation.setFillAfter(fillAfter);
        scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if (startTime) {
                    animationEndListener.onAnimationEnd();
                    if (viewVisible) {
                        setBackgroundOnView();

                    }
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (endTime) {
                    animationEndListener.onAnimationEnd();

                    if (viewVisible) {
                        setBackgroundOnView();
                    }
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                if (repeatTime) {
                    animationEndListener.onAnimationEnd();
                    if (viewVisible) {
                        setBackgroundOnView();
                    }
                }
            }
        });
        view.startAnimation(scaleAnimation);
    }

    private void setBackgroundOnView() {
        secondView.setVisibility(View.VISIBLE);
        firstView.setBackground(layoutBackGround);
    }

    public void objectAnimatorChangeTextColor(View view, String typeColor, int oneColor
            , int secendColor, int duration, int repeatMode, int infintyLoop, boolean start) {

        objectAnimator = ObjectAnimator.ofInt(view, typeColor, oneColor, secendColor);
        objectAnimator.setDuration(duration);
        objectAnimator.setEvaluator(new ArgbEvaluator());
        objectAnimator.setRepeatMode(repeatMode);
        objectAnimator.setRepeatCount(infintyLoop);

        objectAnimator.start();
    }

    public void stopTextColorObjectAnimator() {
        if (objectAnimator != null) {
            objectAnimator.cancel();
        }

    }
}
