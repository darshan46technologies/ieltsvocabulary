package com.ieltsvocabulary.Custom;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.speech.tts.Voice;
import android.util.Log;
import android.view.View;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class SpeakText {

    Context context;
    TextToSpeech textToSpeech;
    SpeechRecognizer recognizer;
    Set<String> a;
    Voice vc = null;
    String storeMessage = "";

    View firstTempView, secondTempView;

    public SpeakText(Context context) {
        this.context = context;

        textToSpeech = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    textToSpeech.setLanguage(Locale.US);
                }
            }
        });

        recognizer = SpeechRecognizer
                .createSpeechRecognizer(context);

    }


    public void ReadText(String text, String gender, String readingPorsitiion) {

        a = new HashSet<>();
        a.add("male");

        readByMaleOrFemale(a, text, gender, readingPorsitiion);
    }

    private void readByMaleOrFemale(Set<String> a, String text, String gender, String readingPorsitiion) {
        if (gender.equalsIgnoreCase("female")) {
            vc = new Voice("en-us-x-sfg#female_2-local", new Locale("en", "US"), 400, 200, true, a);
        } else {
            vc = new Voice("en-us-x-sfg#male_2-local", new Locale("en", "US"), 400, 200, true, a);
        }

        textToSpeech.setVoice(vc);
        textToSpeech.setSpeechRate(0.8f);

        int result = textToSpeech.setVoice(vc);

        if (result == TextToSpeech.LANG_MISSING_DATA
                || result == TextToSpeech.LANG_NOT_SUPPORTED) {
            Log.e("TTS", "This Language is not supported");
        }

        if (readingPorsitiion.equalsIgnoreCase("Text")) {
            textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        } else {
            textToSpeech.speak(storeMessage, TextToSpeech.QUEUE_FLUSH, null);
        }

    }

    public void startAndStoreRecording(View firstView, View secondView) {
        firstTempView = firstView;
        secondTempView = secondView;

        inRecordingProcess();
    }

    private void inRecordingProcess() {

        firstTempView.setVisibility(View.GONE);
        secondTempView.setVisibility(View.VISIBLE);

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                "com.domain.app");

        RecognitionListener listener = new RecognitionListener() {
            @Override
            public void onResults(Bundle results) {
                ArrayList<String> voiceResults = results
                        .getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                if (voiceResults == null) {
                    System.out.println("No voice results");
                } else {
                    System.out.println("Printing matches: ");
                    for (String match : voiceResults) {
                        System.out.println(match);
                        storeMessage = match;

                    }
                }
            }

            @Override
            public void onReadyForSpeech(Bundle params) {
                System.out.println("Ready for speech");
            }

            @Override
            public void onError(int error) {
                System.err.println("Error listening for speech: " + error);
            }

            @Override
            public void onBeginningOfSpeech() {
                System.out.println("Speech starting");
            }

            @Override
            public void onBufferReceived(byte[] buffer) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onEndOfSpeech() {
                // TODO Auto-generated method stub
            }

            @Override
            public void onEvent(int eventType, Bundle params) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPartialResults(Bundle partialResults) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onRmsChanged(float rmsdB) {
                // TODO Auto-generated method stub

            }
        };
        recognizer.setRecognitionListener(listener);
        recognizer.startListening(intent);

    }

    public void stopListning() {
        recognizer.stopListening();
    }


}
