package com.ieltsvocabulary.Custom;

import android.content.Context;
import android.content.DialogInterface;

import com.ieltsvocabulary.Interface.SendActivity;

public class AlertDialogBox {
    android.app.AlertDialog.Builder alertDialog;
    Context context;

    public AlertDialogBox(Context context) {
        this.context = context;
    }

    public void openDialog(String title, String message
            , String positiveText, String nagativeText, final SendActivity sendActivity) {
        alertDialog = new android.app.AlertDialog.Builder(context);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);

        alertDialog.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sendActivity.sendActivity();
            }
        });

        alertDialog.setNegativeButton(nagativeText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        android.app.AlertDialog alert = alertDialog.create();
        alert.show();
    }


}
