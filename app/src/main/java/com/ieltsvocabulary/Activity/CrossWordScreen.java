package com.ieltsvocabulary.Activity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Adapter.CrossWordAdapter;
import com.ieltsvocabulary.Custom.AnimationClass;
import com.ieltsvocabulary.Custom.Utils;
import com.ieltsvocabulary.Interface.CrossWordInterface;
import com.ieltsvocabulary.Model.CrossWordModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;


public class CrossWordScreen extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout relTitle, relNext;
    TextView txtMainTitle;
    ImageView imgBack;
    MotionLayout motionMainNote;
    LinearLayout lnrIcon, lnrListOfHint;
    RecyclerView rcyCrossWord;

    DisplayMetrics displayMetrics;
    CrossWordAdapter crossWordAdapter;
    AnimationClass animationClass;
    Utils utils;

    String text = "@A@@ @      @M@@W@R  @@@@ @@@@";
    String word = "@2@@3@1@@@@@@@@@@@4@@@@@@@@@@@";
    String fullString = " ";
    boolean manageMotionLayout = false;

    ArrayList<String> storeAns;
    ArrayList<CrossWordModel> storeAndGetDataInModel;
    char[] strArray, wordArray;
    int[] arrAnsInt = {6, 7, 8, 9, 10, 11, 00, 1, 7, 13, 19, 25, 00, 4, 10, 16, 00, 18, 19, 20, 00};
    String[] ansString = {"readss", "AeMax", "rsW", "Rad"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cross_word_screen);

        findViewByid();
        init();
        setListner();
        setHintListData();
//        createDynamicView();
    }


    private void findViewByid() {
        relTitle = findViewById(R.id.relTitle);
        txtMainTitle = findViewById(R.id.txtMainTitle);
        imgBack = findViewById(R.id.imgBack);
        rcyCrossWord = findViewById(R.id.rcyCrossWord);
        motionMainNote = findViewById(R.id.motionMainNote);
        lnrIcon = findViewById(R.id.lnrIcon);
        lnrListOfHint = findViewById(R.id.lnrListOfHint);
        relNext = findViewById(R.id.relNext);
    }

    private void init() {
        storeAndGetDataInModel = new ArrayList<>();
        animationClass = new AnimationClass(this);
        utils = new Utils(this);

        strArray = text.toCharArray();
        wordArray = word.toCharArray();
        crossWordAdapter = new CrossWordAdapter(this, strArray, wordArray, storeAndGetDataInModel, new CrossWordInterface() {
            @Override
            public void charWithPosition(char s, int position) {

                setCharInArray(s, position);
            }

        });
        rcyCrossWord.setAdapter(crossWordAdapter);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 6);
        rcyCrossWord.setLayoutManager(gridLayoutManager);
    }

    private void setListner() {
        imgBack.setOnClickListener(this);
        lnrIcon.setOnClickListener(this);
        relNext.setOnClickListener(this);
    }

    private void setHintListData() {
        utils.setInJSONFormate(lnrListOfHint, this);
    }

    private void setCharInArray(char s, int position) {

        for (int a = 0; a < arrAnsInt.length; a++) {
            if (arrAnsInt[a] == position) {
                if (storeAndGetDataInModel.size() == 0) {
                    storeAndGetDataInModel.add(new CrossWordModel(position, s));

                } else {

                    for (int b = 0; b < storeAndGetDataInModel.size(); b++) {

                        if (storeAndGetDataInModel.get(b).getPosition() == position) {

                            storeAndGetDataInModel.remove(b);

//                            storeAndGetDataInModel.add(new CrossWordModel(position,'-'));
                        }
                    }
                    storeAndGetDataInModel.add(new CrossWordModel(position, s));

                }

                return;

            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            /*case R.id.lnrIcon:

                if (crdNotes.getVisibility() != View.VISIBLE) {
                    animationClass.objectAnimation(crdNotes, "alpha", 0f, 1f
                            , 500, true, true, false, true,
                            new AnimationEndListener() {
                                @Override
                                public void onAnimationEnd() {
                                    animationClass.viewVisibilityAndSet(crdNotes, crdNotes, R.color.white);
                                }
                            });
                } else {
                    crdNotes.setVisibility(View.GONE);
                }


//                TranslateAnimation translateAnimation = new TranslateAnimation(-200f, 0f, 0, 0);
//                translateAnimation.setDuration(200);
//                translateAnimation.setFillAfter(true);
//                crdNotes.setVisibility(View.VISIBLE);
//                crdNotes.startAnimation(translateAnimation);
//                crdIcon.startAnimation(translateAnimation);
                break;*/

            case R.id.lnrIcon:
                if (!manageMotionLayout) {
                    manageMotion(R.id.startView, R.id.endView);
                    manageMotionLayout = true;
                } else {
                    manageMotion(R.id.endView, R.id.startView);
                    manageMotionLayout = false;
                }

                break;
            case R.id.relNext:
                storeAns = new ArrayList<>();
//
//                String s = "this is a string";
//                String temp = " ";
//                ArrayList<String>list = new ArrayList<>();
//
//                for (int a = 0 ;a<s.length();a++){
//                    char c = s.charAt(a);
//
//                    if (c != ' '){
//                        if (temp.equalsIgnoreCase(" ")){
//                            temp = c + "-";
//
//                        }else{
//
//                            temp = temp+c+"-";
//                        }
//                    }
//
//
//
//
//                }
//                Log.e("lisst", temp);

                for (int a = 0; a < arrAnsInt.length; a++) {

                    for (int b = 0; b < storeAndGetDataInModel.size(); b++) {

                        if (arrAnsInt[a] != 00) {

                            if (arrAnsInt[a] == storeAndGetDataInModel.get(b).getPosition()) {

                                char c = storeAndGetDataInModel.get(b).getSingleChar();

                                if (fullString.equalsIgnoreCase(" ")) {
                                    fullString = String.valueOf(c);
                                } else {
                                    fullString = fullString + c;
                                }
                                Log.e("fullString", fullString);
                            }
                        } else {

                            for (int c = 0; c < fullString.length(); c++) {

                                if (fullString.charAt(c) == '-') {

                                    Toast.makeText(this, "Please Fill All Boxes", Toast.LENGTH_SHORT).show();
                                }
                            }
                            if (!fullString.equalsIgnoreCase(" ")) {
                                storeAns.add(fullString);
                                Log.e("storeAns", String.valueOf(storeAns));
                                fullString = " ";
                            }

                            break;
                        }

                    }


                }

                compareString();


                break;
        }

    }

    private void manageMotion(int startView, int endView) {
        motionMainNote.setTransition(startView, endView);
        motionMainNote.transitionToEnd();
        motionMainNote.setTransitionDuration(500);
    }

    private void compareString() {
        if (ansString.length == storeAns.size()) {
            for (int d = 0; d < storeAns.size(); d++) {
                String getString = storeAns.get(d);
                if (!getString.equalsIgnoreCase(ansString[d])) {
                    Toast.makeText(this, "Wrong" + " " + (d + 1), Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            Toast.makeText(this, "Right All", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Please Fill All Boxes", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
