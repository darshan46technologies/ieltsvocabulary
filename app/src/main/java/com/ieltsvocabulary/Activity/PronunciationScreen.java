package com.ieltsvocabulary.Activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.ieltsvocabulary.Custom.AnimationClass;
import com.ieltsvocabulary.Custom.SpeakText;
import com.ieltsvocabulary.Custom.Utils;
import com.ieltsvocabulary.Interface.AnimationEndListener;
import com.ieltsvocabulary.R;

public class PronunciationScreen extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout relTitle, relNext;
    TextView txtMainTitle, txtTitle, txtPer, txtPerMsg, txtNext, txtOff, txtOn;
    ImageView imgWarning, imgBack, imgTextPlayAudio, imgMainImg, imgLike, imgRec, imgPause, imgPlayRec, imgStopRec, imgPauseAudio;
    CardView crdMotionLayout,crdPronunciation;
    //    SwitchButton swtOne;
    MotionLayout motion_layout;
    LinearLayout lnrHintView, lnrStartRecording, lnrOptionRecording;
    MotionLayout motionHint;

//    public static final int REQUEST_AUDIO_PERMISSION_CODE = 1;

    String msges = "";
    int newval = 0, width;
    boolean normalAudioMotionFlag = false;

    AnimationClass animationClass;
    DisplayMetrics displayMetrics;
    Utils utils;
    SpeakText speakText;
    boolean clickFlag = false;
    public static final int REQUEST_CODE = 100;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pronunciation_screen);

        findViewByid();
        init();
        setListner();


    }

    private void findViewByid() {
        relTitle = findViewById(R.id.relTitle);
        txtMainTitle = findViewById(R.id.txtMainTitle);
        imgWarning = findViewById(R.id.imgWarning);
        imgBack = findViewById(R.id.imgBack);
        crdPronunciation = findViewById(R.id.crdPronunciation);
        txtTitle = findViewById(R.id.txtTitle);
        imgTextPlayAudio = findViewById(R.id.imgTextPlayAudio);
        imgMainImg = findViewById(R.id.imgMainImg);
        txtPer = findViewById(R.id.txtPer);
        txtPerMsg = findViewById(R.id.txtPerMsg);
        imgLike = findViewById(R.id.imgLike);
        lnrStartRecording = findViewById(R.id.lnrStartRecording);
        imgRec = findViewById(R.id.imgRec);
        imgPause = findViewById(R.id.imgPause);
        lnrOptionRecording = findViewById(R.id.lnrOptionRecording);
        crdMotionLayout = findViewById(R.id.crdMotionLayout);
        motion_layout = findViewById(R.id.motion_layout);
        txtOff = findViewById(R.id.txtOff);
        txtOn = findViewById(R.id.txtOn);
//        swtOne = findViewById(R.id.swtOne);
        imgPlayRec = findViewById(R.id.imgPlayRec);
        imgStopRec = findViewById(R.id.imgStopRec);
        imgPauseAudio = findViewById(R.id.imgPauseAudio);
        lnrHintView = findViewById(R.id.lnrHintView);
        motionHint = findViewById(R.id.motionHint);
        relNext = findViewById(R.id.relNext);
        txtNext = findViewById(R.id.txtNext);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void init() {
        utils = new Utils(this);
        speakText = new SpeakText(this);
        animationClass = new AnimationClass(this);
        displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        width = displayMetrics.widthPixels;


    }


    private void setListner() {
        imgWarning.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        relNext.setOnClickListener(this);
        lnrStartRecording.setOnClickListener(this);
        imgTextPlayAudio.setOnClickListener(this);
        imgPlayRec.setOnClickListener(this);
        imgStopRec.setOnClickListener(this);
        imgPauseAudio.setOnClickListener(this);
        crdMotionLayout.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imgWarning:

                if (!clickFlag) {
                    manageMotion(R.id.startHint, R.id.endHint);
                    clickFlag = true;
                } else {
                    manageMotion(R.id.endHint, R.id.startHint);
                    clickFlag = false;
                }
                //Send JSONArray
//                utils.usedRecyclerViewDialog(lnrHintView,this,passJSONArray);
                utils.setInJSONFormate(lnrHintView, this);
                break;
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.relNext:
                animationClass.moveTranslateAnimation(0, -width, 0, 0,
                        120, true, true, false, false, crdPronunciation, false, new AnimationEndListener() {
                            @Override
                            public void onAnimationEnd() {
                                animationClass.moveTranslateAnimation(
                                        width, 0, 0, 0, 120, true, false, false,
                                        false, crdPronunciation, false, new AnimationEndListener() {
                                            @Override
                                            public void onAnimationEnd() {
                                            }
                                        }
                                );
                            }
                        });
                break;

            case R.id.lnrStartRecording:

                if (check()) {
                    recordVoice();

                } else {
                    request();
                }
                break;

            case R.id.imgTextPlayAudio:
                speakText.ReadText(txtTitle.getText().toString(), "male", "Text");
                break;

            case R.id.imgPlayRec:

                imgPlayRec.setVisibility(View.GONE);
                imgStopRec.setVisibility(View.VISIBLE);
                speakText.ReadText(msges, "male", "storeMessage");

                break;

            case R.id.imgStopRec:
                imgPlayRec.setVisibility(View.VISIBLE);
                imgStopRec.setVisibility(View.GONE);
                speakText.stopListning();
                break;
            case R.id.imgPause:
                break;

            case R.id.imgPauseAudio:
                if (lnrOptionRecording.getVisibility() == View.VISIBLE) {
                    lnrOptionRecording.setVisibility(View.GONE);
                    lnrStartRecording.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.crdMotionLayout:
                if (!normalAudioMotionFlag){
                    normalAudioMotionFlag = true;
                    normalAudioToggleMotion(R.id.start, R.id.end);
                }else{
                    normalAudioMotionFlag =false;
                    normalAudioToggleMotion(R.id.end, R.id.start);
                }
//                if (txtOff.getVisibility() == View.VISIBLE) {
//                    motion_layout.setTransition(R.id.start, R.id.end);
//                    motion_layout.setTransitionDuration(600);
//                    motion_layout.transitionToEnd();
//
//                } else {
//                    motion_layout.setTransition(R.id.end, R.id.start);
//                    motion_layout.setTransitionDuration(600);
//                    motion_layout.transitionToEnd();
//                }
                break;
        }

    }

    private void normalAudioToggleMotion(int start, int end) {
        motion_layout.setTransition(start, end);
        motion_layout.setTransitionDuration(600);
        motion_layout.transitionToEnd();
    }

    private void manageMotion(int startHint, int endHint) {
        motionHint.setTransition(startHint, endHint);
        motionHint.setTransitionDuration(500);
        motionHint.transitionToEnd();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void recordVoice() {
        if (imgRec.getVisibility() == View.VISIBLE) {

            speakText.startAndStoreRecording(imgRec, imgPause);
        } else {

            imgLike.setVisibility(View.VISIBLE);
            lnrStartRecording.setVisibility(View.GONE);
            lnrOptionRecording.setVisibility(View.VISIBLE);
            speakText.stopListning();

            newval = 0;


            String txtText = txtTitle.getText().toString().trim();
            int a = msges.length();
            int b = txtText.length();

            Log.d("newValue", String.valueOf(msges));

            for (int i = 0; i < msges.length(); i++) {
                char c = msges.charAt(i);
                for (int s = i; s < txtText.length(); s++) {
                    char k = txtText.charAt(s);

                    if (c == k) {

                        newval += 1;
                        Log.d("newValue", String.valueOf(newval));
                        break;
                    }

                }

            }
        }

    }

    private boolean check() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);

        return result == PackageManager.PERMISSION_GRANTED;

    }

    private void request() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    speakText.startAndStoreRecording(imgRec, imgPause);
                }
                break;
        }

    }
}
