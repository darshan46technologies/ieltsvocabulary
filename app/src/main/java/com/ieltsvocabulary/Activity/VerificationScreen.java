package com.ieltsvocabulary.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.ieltsvocabulary.R;

public class VerificationScreen extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout relTopSide;
    ImageView imgLogo,imgBack;
    LinearLayout lnrVerify;
    TextView txtVerificationcode, txtNotice;
    EditText edtOne, edtTwo, edtThree, edtFour, edtFive, edtSix;
    Button btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_screen);

        findViewByid();
        init();
        setListner();
    }

    private void findViewByid() {
        relTopSide = findViewById(R.id.relTopSide);
        imgLogo = findViewById(R.id.imgLogo);
        imgBack = findViewById(R.id.imgBack);
        lnrVerify = findViewById(R.id.lnrVerify);
        txtVerificationcode = findViewById(R.id.txtVerificationcode);
        edtOne = findViewById(R.id.edtOne);
        edtTwo = findViewById(R.id.edtTwo);
        edtThree = findViewById(R.id.edtThree);
        edtFour = findViewById(R.id.edtFour);
        edtFive = findViewById(R.id.edtFive);
        edtSix = findViewById(R.id.edtSix);
        txtNotice = findViewById(R.id.txtNotice);
        btnNext = findViewById(R.id.btnNext);
    }

    private void init() {
    }

    private void setListner() {
        imgBack.setOnClickListener(this);
        btnNext.setOnClickListener(this);

        edtOne.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1)
                    edtTwo.requestFocus();
            }
        });
        edtTwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1)
                    edtThree.requestFocus();
                else if (s.length() == 0)
                    edtOne.requestFocus();
            }
        });
        edtThree.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1)
                    edtFour.requestFocus();
                else if (s.length() == 0)
                    edtTwo.requestFocus();
            }
        });
        edtFour.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1)
                    edtFive.requestFocus();
                else if (s.length() == 0)
                    edtThree.requestFocus();
            }
        });
        edtFive.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1)
                    edtSix.requestFocus();
                else if (s.length() == 0)
                    edtFour.requestFocus();
            }
        });
        edtSix.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0)
                    edtFive.requestFocus();
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.btnNext:
                startActivity(new Intent(VerificationScreen.this,ChangePassword.class));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}


