package com.ieltsvocabulary.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.ieltsvocabulary.Custom.AlertDialogBox;
import com.ieltsvocabulary.Interface.SendActivity;
import com.ieltsvocabulary.R;

public class ProfileScreen extends AppCompatActivity implements View.OnClickListener {


    ConstraintLayout conProfile;
    ImageView imgUserPhott, imgBack;
    TextView txtUserName, txtAccountInfo, txtTitleUserName, txtTitleUserEmail, txtSetting,
            txtNotification, txtChangePassword, txtLegal, txtPolicy, txtTermsCondition,
            txtTitlePassword;
    LinearLayout lnrUserDetails, lnrChangePass;
    EditText edtUserName, edtEmail, edtNewPassword, edtConfirmPassword;
    CardView crdNotification, crdLogout;
    MotionLayout motion_layout;
    //    SwitchButton swbNotificationButton;
    RelativeLayout relTopSide;
    ImageView imgLogo;

    AlertDialogBox alertDialogBox;
    SendActivity sendActivity;
    boolean notificationFlag = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_screen);
        findViewByid();
        init();
        setListner();
    }

    private void findViewByid() {
        conProfile = findViewById(R.id.conProfile);
        imgUserPhott = findViewById(R.id.imgUserPhott);
        imgBack = findViewById(R.id.imgBack);
        txtUserName = findViewById(R.id.txtUserName);
        lnrUserDetails = findViewById(R.id.lnrUserDetails);
        txtAccountInfo = findViewById(R.id.txtAccountInfo);
        txtTitleUserName = findViewById(R.id.txtTitleUserName);
        edtUserName = findViewById(R.id.edtUserName);
        txtTitleUserEmail = findViewById(R.id.txtTitleUserEmail);
        edtEmail = findViewById(R.id.edtEmail);
        txtSetting = findViewById(R.id.txtSetting);
        txtNotification = findViewById(R.id.txtNotification);
        crdNotification = findViewById(R.id.crdNotification);
        motion_layout = findViewById(R.id.motion_layout);
//        swbNotificationButton = findViewById(R.id.swbNotificationButton);
        txtChangePassword = findViewById(R.id.txtChangePassword);
        txtLegal = findViewById(R.id.txtLegal);
        txtPolicy = findViewById(R.id.txtPolicy);
        txtTermsCondition = findViewById(R.id.txtTermsCondition);
        crdLogout = findViewById(R.id.crdLogout);

        relTopSide = findViewById(R.id.relTopSide);
        imgLogo = findViewById(R.id.imgLogo);
        lnrChangePass = findViewById(R.id.lnrChangePass);
        txtTitlePassword = findViewById(R.id.txtTitlePassword);
        edtNewPassword = findViewById(R.id.edtNewPassword);
        edtConfirmPassword = findViewById(R.id.edtConfirmPassword);
    }

    private void init() {
        alertDialogBox = new AlertDialogBox(this);
    }

    private void setListner() {
        imgBack.setOnClickListener(this);
        crdNotification.setOnClickListener(this);
        txtChangePassword.setOnClickListener(this);
        crdLogout.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.txtChangePassword:
                startActivity(new Intent(ProfileScreen.this, ChangePassword.class));
                break;

            case R.id.crdLogout:
                alertDialogBox.openDialog("Alert Box!!", "Are you sure you want to logout ?"
                        , "Yes"
                        , "No", new SendActivity() {
                            @Override
                            public void sendActivity() {
                                Intent intent = new Intent(ProfileScreen.this, LoginAndSignup.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);

                                finish();
                            }
                        });

                break;

            case R.id.crdNotification:
                if (!notificationFlag) {
                    notificationFlag = true;
                    notiToggleMotion(R.id.start, R.id.end);
                } else {
                    notificationFlag = false;
                    notiToggleMotion(R.id.end, R.id.start);
                }
                break;

        }
    }

    private void notiToggleMotion(int start, int end) {
        motion_layout.setTransition(start, end);
        motion_layout.setTransitionDuration(600);
        motion_layout.transitionToEnd();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
