package com.ieltsvocabulary.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.ieltsvocabulary.R;

public class ChangePassword extends AppCompatActivity implements View.OnClickListener {

    ImageView imgBackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        findViewByid();
        init();
        setListner();
    }

    private void findViewByid() {
        imgBackButton = findViewById(R.id.imgBackButton);
    }

    private void init() {
    }

    private void setListner() {
        imgBackButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgBackButton:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
