package com.ieltsvocabulary.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.ieltsvocabulary.R;

public class LoginAndSignup extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout relTopSide;
    ImageView imgLogo, imgBack, imgFacebook, imgGooglePlus;
    LinearLayout lnrSocialMedia, main_data_lay;
    EditText edtEmail, edtPassword, edtName, edtForgetedMail;
    TextView txtForgetPassword, txtLogIn, txtName;
    CheckBox chkTermsCondition;
    Button btnNext;
    ConstraintLayout cnrForget;

    RelativeLayout crdLogin, crdRegistration;
    LinearLayout crdWithLogin, crdWithRegistartion;
    MotionLayout login_reg_motion;
    boolean reg = false, log = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_and_signup);

        findViewByid();
        init();
        setListner();
    }

    private void findViewByid() {
        relTopSide = findViewById(R.id.relTopSide);
        imgLogo = findViewById(R.id.imgLogo);
        imgBack = findViewById(R.id.imgBack);

        txtName = findViewById(R.id.txtName);
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        txtForgetPassword = findViewById(R.id.txtForgetPassword);
        edtName = findViewById(R.id.edtName);
        chkTermsCondition = findViewById(R.id.chkTermsCondition);
        imgFacebook = findViewById(R.id.imgFacebook);
        imgGooglePlus = findViewById(R.id.imgGooglePlus);
        lnrSocialMedia = findViewById(R.id.lnrSocialMedia);
        edtForgetedMail = findViewById(R.id.edtForgetedMail);
        btnNext = findViewById(R.id.btnNext);
        cnrForget = findViewById(R.id.cnrForget);
//        main_data_lay = findViewById(R.id.main_data_lay);
        login_reg_motion = findViewById(R.id.login_reg_motion);

        crdRegistration = findViewById(R.id.crdRegistration);
//        crdLogin = findViewById(R.id.crdLogin);
        login_reg_motion = findViewById(R.id.login_reg_motion);
        crdWithRegistartion = findViewById(R.id.crdWithRegistartion);
        crdWithLogin = findViewById(R.id.crdWithLogin);

        crdLogin = findViewById(R.id.crdLogin);
        txtLogIn = findViewById(R.id.txtLogIn);
        crdWithLogin = findViewById(R.id.crdWithLogin);
        crdRegistration = findViewById(R.id.crdRegistration);
        crdWithRegistartion = findViewById(R.id.crdWithRegistartion);
    }

    private void init() {
        startAnimation(chkTermsCondition,txtForgetPassword,10);
    }


    private void setListner() {
        imgBack.setOnClickListener(this);
        crdLogin.setOnClickListener(this);
        crdRegistration.setOnClickListener(this);
        crdWithLogin.setOnClickListener(this);
        txtForgetPassword.setOnClickListener(this);
        imgFacebook.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        crdLogin.setOnClickListener(this);
        crdRegistration.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
//                Crashlytics.getInstance().crash();
                break;
            case R.id.crdWithLogin:
                startActivity(new Intent(LoginAndSignup.this, HomeScreen.class));
                finish();
                break;
            case R.id.crdLogin:
                setAnimation(0);
                break;
            case R.id.crdRegistration:
                setAnimation(1);
                break;
            case R.id.txtForgetPassword:
                cnrForget.setVisibility(View.VISIBLE);
//                login_reg_motion.setVisibility(View.GONE);
                imgBack.setVisibility(View.VISIBLE);
                login_reg_motion.setVisibility(View.GONE);
                break;
            case R.id.imgFacebook:
                break;
            case R.id.btnNext:
                Intent intent1 = new Intent(LoginAndSignup.this, VerificationScreen.class);
                startActivity(intent1);
                break;
        }
    }

    private void setAnimation(int whichOne) {
        login_reg_motion.setVisibility(View.VISIBLE);
//        login_reg_motion.setVisibility(View.VISIBLE);
        cnrForget.setVisibility(View.GONE);
        imgBack.setVisibility(View.GONE);

        switch (whichOne) {
            case 0://login
                if (!log) {
                    log = true;
                    reg = false;
                    crdLogin.setBackgroundResource(R.drawable.right_corners_red);
                    crdWithLogin.setBackgroundResource(R.drawable.left_corners_red);
                    crdRegistration.setBackgroundResource(R.drawable.right_corners_gray);
                    crdWithRegistartion.setBackgroundResource(R.drawable.left_corners_gray);
                    login_reg_motion.setTransition(R.id.signup_constraint, R.id.login_constraint);
                    login_reg_motion.setTransitionDuration(500);
                    login_reg_motion.transitionToEnd();
//                    txtName.setVisibility(View.GONE);
//                    edtName.setVisibility(View.GONE);
                    startAnimation(chkTermsCondition,txtForgetPassword,500);
//                    chkTermsCondition.setVisibility(View.GONE);
//                    txtForgetPassword.setVisibility(View.VISIBLE);
                }
                break;
            case 1://registration
                if (!reg) {
                    log = false;
                    reg = true;
                    crdRegistration.setBackgroundResource(R.drawable.right_corners_red);
                    crdWithRegistartion.setBackgroundResource(R.drawable.left_corners_red);
                    crdLogin.setBackgroundResource(R.drawable.right_corners_gray);
                    crdWithLogin.setBackgroundResource(R.drawable.left_corners_gray);
                    login_reg_motion.setTransition(R.id.login_constraint, R.id.signup_constraint);
                    login_reg_motion.setTransitionDuration(500);
                    login_reg_motion.transitionToEnd();
//                    txtName.setVisibility(View.VISIBLE);
//                    edtName.setVisibility(View.VISIBLE);
                    startAnimation(txtForgetPassword,chkTermsCondition,500);

//                    chkTermsCondition.setVisibility(View.VISIBLE);
//                    txtForgetPassword.setVisibility(View.GONE);
                }
                break;
        }
    }

    private void startAnimation(View hideView, View showView, int i) {
        hideView.animate().alpha(0f).setDuration(i);
        hideView.setClickable(false);

        showView.animate().alpha(1f).setDuration(i);
        showView.setClickable(true);
    }

    @Override
    public void onBackPressed() {
        if (cnrForget.getVisibility() == View.VISIBLE) {
            cnrForget.setVisibility(View.GONE);
            imgBack.setVisibility(View.GONE);
            login_reg_motion.setVisibility(View.VISIBLE);
//            login_reg_motion.setVisibility(View.VISIBLE);
        } else
            finish();
    }

   /* private void validation() {
        if (edtLoginMailId.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Email", Toast.LENGTH_SHORT).show();
            return;
        }
        if (edtLoginPassword.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Password", Toast.LENGTH_SHORT).show();
            return;
        }
        if (edtRegName.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Name", Toast.LENGTH_SHORT).show();
            return;
        }
        if (edtRegEmailId.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Email", Toast.LENGTH_SHORT).show();
            return;
        }

        if (edtRegPassword.getText().toString().isEmpty()) {
            Toast.makeText(this, "Enter Password", Toast.LENGTH_SHORT).show();
            return;
        }
    }*/

}
