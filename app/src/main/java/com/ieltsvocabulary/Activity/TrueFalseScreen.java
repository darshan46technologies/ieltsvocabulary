package com.ieltsvocabulary.Activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.ieltsvocabulary.Custom.AnimationClass;
import com.ieltsvocabulary.Interface.AnimationEndListener;
import com.ieltsvocabulary.Model.TrueFalseModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

public class TrueFalseScreen extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout relTitle, relNext;
    TextView txtMainTitle, txtWord, txtWordText, txtMeaning, txtOptionQA;
    ImageView imgBack, imgOptionQA;
    CardView crdTrueFalse, crdFalse, crdRight;
    LinearLayout lnrOptionQA;
    String ansMatch;

    AnimationClass animationClass;
    DisplayMetrics displayMetrics;
    ArrayList<TrueFalseModel> dataList;

    int width;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_true_false_screen);

        findViewByid();
        init();
        setListner();
    }

    private void findViewByid() {
        relTitle = findViewById(R.id.relTitle);
        txtMainTitle = findViewById(R.id.txtMainTitle);
        imgBack = findViewById(R.id.imgBack);
        crdTrueFalse = findViewById(R.id.crdTrueFalse);
        txtWord = findViewById(R.id.txtWord);
        txtWordText = findViewById(R.id.txtWordText);
        txtMeaning = findViewById(R.id.txtMeaning);
//        crdOption = findViewById(R.id.crdOption);
        lnrOptionQA = findViewById(R.id.lnrOptionQA);
        imgOptionQA = findViewById(R.id.imgOptionQA);
        txtOptionQA = findViewById(R.id.txtOptionQA);
        crdFalse = findViewById(R.id.crdFalse);
        crdRight = findViewById(R.id.crdRight);
        relNext = findViewById(R.id.relNext);

    }

    private void init() {
        animationClass = new AnimationClass(this);
        displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        width = displayMetrics.widthPixels;

        ansMatch = "(to) enrol(on)";
        dataList = new ArrayList<>();

        dataList.add(new TrueFalseModel(R.string.app_name, 0));
        dataList.add(new TrueFalseModel(R.string.open, 0));
        dataList.add(new TrueFalseModel(R.string.mainMessage, 0));
        dataList.add(new TrueFalseModel(R.string.sentenceMsg, 0));

        imgOptionQA.setVisibility(View.GONE);
    }


    private void setListner() {
        imgBack.setOnClickListener(this);
        crdRight.setOnClickListener(this);
        crdFalse.setOnClickListener(this);
        relNext.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.relNext:
                checkAns("ScreenNext");

                break;

            case R.id.crdRight:
                checkAns("Yes");

                break;

            case R.id.crdFalse:
                checkAns("No");

                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void checkAns(String msg) {
        String txtqus = txtOptionQA.getText().toString();
        if (msg.equalsIgnoreCase("Yes")) {

            setContent("Well Done", getResources().getColor(R.color.white),
                    R.drawable.trsmiley, getResources().getDrawable(R.drawable.layout_true_color), msg);

        } else {
            setContent("Oppss ! Wrong Answer", getResources().getColor(R.color.white),
                    R.drawable.whitewarning, getResources().getDrawable(R.drawable.layout_false_color), msg);

        }

    }

    private void setContent(final String msg, final int color, final int trsmiley, final Drawable layoutBg, String msges) {

        if (msges.equalsIgnoreCase("ScreenNext")) {
            moveAnimation(crdTrueFalse);
        } else {

            animationClass.scaleAnimation(lnrOptionQA, 1f, 1f, 1f, 0f,
                    0.5f, 0.5f, 250, true, false, true,
                    false, true, new AnimationEndListener() {
                        @Override
                        public void onAnimationEnd() {
                            txtOptionQA.setText(msg);
                            txtOptionQA.setTextColor(color);
                            imgOptionQA.setImageResource(trsmiley);
                            animationClass.viewVisibilityAndSet(lnrOptionQA, lnrOptionQA, layoutBg);
                            imgOptionQA.setVisibility(View.VISIBLE);
                            imgOptionQA.setImageResource(trsmiley);

                            animationClass.scaleAnimation(lnrOptionQA, 1f, 1f, 0f, 1f,
                                    0.5f, 0.5f, 250, true, false, true,
                                    false, false, new AnimationEndListener() {
                                        @Override
                                        public void onAnimationEnd() {

                                            moveAnimation(lnrOptionQA);
                                        }
                                    });
                        }
                    });
        }
    }

    private void moveAnimation(final View view) {
        animationClass.moveTranslateAnimation(0,
                -width, 0, 0, 150,
                false, true, false, false, view, false,
                new AnimationEndListener() {
                    @Override
                    public void onAnimationEnd() {
                        animationClass.moveTranslateAnimation(width,
                                0, 0, 0, 150
                                , false, true, false, false, view, false,
                                new AnimationEndListener() {
                                    @Override
                                    public void onAnimationEnd() {

                                    }
                                });
                    }
                });
    }

}
