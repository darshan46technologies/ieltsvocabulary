package com.ieltsvocabulary.Activity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Adapter.TopicHorizontalAdpater;
import com.ieltsvocabulary.Adapter.TopicNameAdapter;
import com.ieltsvocabulary.Model.PiHorizontalModel;
import com.ieltsvocabulary.Model.TopicNameModel;
import com.ieltsvocabulary.R;
import com.razerdp.widget.animatedpieview.AnimatedPieView;
import com.razerdp.widget.animatedpieview.AnimatedPieViewConfig;
import com.razerdp.widget.animatedpieview.data.SimplePieInfo;

import java.util.ArrayList;

public class PiTopicScreen extends AppCompatActivity implements View.OnClickListener {

    ImageView imgBack;
    RecyclerView rcylistTopic, rcyhorizontaltopic;
    TopicNameAdapter topicNameAdapter;
    ArrayList<TopicNameModel> topiclist;
    ArrayList<PiHorizontalModel> listHorizontalData;
    ArrayList<String> childItem;
    TopicHorizontalAdpater horizontalAdpater;
    AnimatedPieView piechart;
    AnimatedPieViewConfig animatedPieViewConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pi_topic_screen);

        findViewByid();
        init();
        setListner();
    }

    private void findViewByid() {
        imgBack = findViewById(R.id.imgBack);
        rcylistTopic = findViewById(R.id.rcylistTopic);
        rcyhorizontaltopic = findViewById(R.id.rcyhorizontaltopic);
        piechart = findViewById(R.id.piechart);
    }

    private void init() {
        animatedPieViewConfig = new AnimatedPieViewConfig();
        topiclist = new ArrayList<>();
        childItem = new ArrayList<>();
        listHorizontalData = new ArrayList<>();

        childItem.add("(to) enrol(on)");
        childItem.add("(to) benefit(from)");
        childItem.add("(to) study");
        childItem.add("(to) follow");
        childItem.add("(to) specilise in");
        childItem.add("(to) opt for");
        childItem.add("(to) teach");
        childItem.add("(to) train");
        childItem.add("(to) educate");
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rcylistTopic.setLayoutManager(linearLayoutManager);

        listHorizontalData.add(new PiHorizontalModel(R.drawable.message, R.string.titleDictionary, true));
        listHorizontalData.add(new PiHorizontalModel(R.drawable.message, R.string.pronunciation, false));
        listHorizontalData.add(new PiHorizontalModel(R.drawable.message, R.string.listening, false));
        listHorizontalData.add(new PiHorizontalModel(R.drawable.message, R.string.mainTitleTrueFalseScreen, false));
        listHorizontalData.add(new PiHorizontalModel(R.drawable.message, R.string.crossword, false));
        listHorizontalData.add(new PiHorizontalModel(R.drawable.message, R.string.wordType, false));
        listHorizontalData.add(new PiHorizontalModel(R.drawable.message, R.string.spelling, false));
        listHorizontalData.add(new PiHorizontalModel(R.drawable.message, R.string.matchAnswer, false));
        listHorizontalData.add(new PiHorizontalModel(R.drawable.message, R.string.matchThePair, false));
        listHorizontalData.add(new PiHorizontalModel(R.drawable.message, R.string.gapFillTitle, false));
        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        rcyhorizontaltopic.setLayoutManager(linearLayoutManager1);
        horizontalAdpater = new TopicHorizontalAdpater(this, listHorizontalData);
        rcyhorizontaltopic.setAdapter(horizontalAdpater);

        rcylistTopic.setHasFixedSize(true);

        animatedPieViewConfig.addData(new SimplePieInfo(2000, Color.parseColor("#3bb784"), "A"));
        animatedPieViewConfig.addData(new SimplePieInfo(1500, Color.parseColor("#232856"), "B"));
        animatedPieViewConfig.addData(new SimplePieInfo(2500, Color.parseColor("#cc5d53"), "C"));
        animatedPieViewConfig.duration(300);
        animatedPieViewConfig.drawText(true);
        animatedPieViewConfig.splitAngle(2);
        animatedPieViewConfig.animatePie(true);
        animatedPieViewConfig.strokeMode(false);
        animatedPieViewConfig.textSize(40);
        piechart.applyConfig(animatedPieViewConfig);
        piechart.start();


        topiclist.add(new TopicNameModel("Remaining", "kkkkk", 1));
        topiclist.add(new TopicNameModel("Completed", "kkkkk", 2));
        topiclist.add(new TopicNameModel("Already Known ", "kkkkk", 3));
       /* topiclist.add(new TopicNameModel("Remainig", "kkkkk", 1));
        topiclist.add(new TopicNameModel("Completed", "kkkkk", 2));
        topiclist.add(new TopicNameModel("Already Known ", "kkkkk", 3));
        topiclist.add(new TopicNameModel("Remainig", "kkkkk", 1));
        topiclist.add(new TopicNameModel("Completed", "kkkkk", 2));
        topiclist.add(new TopicNameModel("Already Known ", "kkkkk", 3));*/

        topicNameAdapter = new TopicNameAdapter(this, topiclist, childItem);
        rcylistTopic.setAdapter(topicNameAdapter);
        topicNameAdapter.notifyDataSetChanged();
    }

    private void setListner() {
        imgBack.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
