package com.ieltsvocabulary.Activity;

import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Adapter.GapFillAdapter;
import com.ieltsvocabulary.Custom.AnimationClass;
import com.ieltsvocabulary.Interface.AnimationEndListener;
import com.ieltsvocabulary.Interface.MatchThePairsInterface;
import com.ieltsvocabulary.Model.SelectCorrectWordModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

public class GapFillScreen extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout relTitle, relNext;
    TextView txtMainTitle, txtSentenceTitle, txtSentenceSubTitle;
    ImageView imgBack;
    CardView crdGapFill;
    LinearLayout lnrSentenceSection;
    RecyclerView rcyGapFill;

    ArrayList<SelectCorrectWordModel> listTheWord;
    GapFillAdapter dataAdapter;
    LinearLayoutManager linearLayoutManager;

    DisplayMetrics displayMetrics;
    AnimationClass animationClass;
    String sentanceMsg;
    int width;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gap_fill_word);

        findViewByid();
        init();
        setListner();
        setTextInTextView(false, " ");

    }

    private void setTextInTextView(boolean setValue, String optionText) {
        sentanceMsg = " ";
        String setColor;

        String msg = "to put @ else to a list of members of an organisation (usually a college, university or school)";
        for (int a = 0; a < msg.length(); a++) {

            char singleChar = msg.charAt(a);

            if (singleChar == '@' && !setValue) {
                setColor = "<font color='blue'>__</font>";
                sentanceMsg = sentanceMsg + setColor;
            } else if (singleChar == '@' && setValue) {
                setColor = "<font color='blue'><u>" + optionText + "</u></font>";
                sentanceMsg = sentanceMsg + setColor;
            } else {
                sentanceMsg = sentanceMsg + singleChar;
            }
        }
        txtSentenceSubTitle.setText(Html.fromHtml(sentanceMsg));
    }

    private void findViewByid() {
        relTitle = findViewById(R.id.relTitle);
        txtMainTitle = findViewById(R.id.txtMainTitle);
        imgBack = findViewById(R.id.imgBack);
        crdGapFill = findViewById(R.id.crdGapFill);
        lnrSentenceSection = findViewById(R.id.lnrSentenceSection);
        txtSentenceTitle = findViewById(R.id.txtSentenceTitle);
        txtSentenceSubTitle = findViewById(R.id.txtSentenceSubTitle);
        rcyGapFill = findViewById(R.id.rcyGapFill);
        relNext = findViewById(R.id.relNext);
    }

    private void init() {
        listTheWord = new ArrayList<>();

        animationClass = new AnimationClass(this);
        displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        width = displayMetrics.widthPixels;

        linearLayoutManager = new LinearLayoutManager(this);
        listTheWord.add(new SelectCorrectWordModel(R.string.to_enrol_on, false));
        listTheWord.add(new SelectCorrectWordModel(R.string.to_benefit_from, false));
        listTheWord.add(new SelectCorrectWordModel(R.string.to_study, false));
        listTheWord.add(new SelectCorrectWordModel(R.string.to_follow, false));
        listTheWord.add(new SelectCorrectWordModel(R.string.to_specialise_in, false));
//        listTheWord.add(new SelectCorrectWordModel(R.string.commonMesg, false));

        dataAdapter = new GapFillAdapter(this, listTheWord, new MatchThePairsInterface() {
            @Override
            public void fillColorInView(int position, CardView cardView, TextView textView) {

                ScaleAnimation scaleAnimation = new ScaleAnimation(0.9f, 1f, 0.9f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                scaleAnimation.setDuration(400);
                scaleAnimation.setFillAfter(true);
                textView.setTextColor(getResources().getColor(R.color.white));
                cardView.setBackgroundResource(R.drawable.layout_maroon_select_bg);
                setTextInTextView(true, textView.getText().toString());
                cardView.startAnimation(scaleAnimation);
            }
        });
        rcyGapFill.setLayoutManager(linearLayoutManager);
        rcyGapFill.setAdapter(dataAdapter);
    }

    private void setListner() {
        imgBack.setOnClickListener(this);
        relNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.relNext:
                animationClass.moveTranslateAnimation(0, -width, 0, 0, 120,
                        false, true, false, false, crdGapFill, false, new AnimationEndListener() {
                            @Override
                            public void onAnimationEnd() {
                                animationClass.moveTranslateAnimation(width, 0, 0, 0, 120,
                                        false, false, false, false, crdGapFill, false, new AnimationEndListener() {

                                            @Override
                                            public void onAnimationEnd() {

                                            }
                                        });
                            }
                        });
                break;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
