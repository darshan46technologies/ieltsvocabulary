package com.ieltsvocabulary.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.ieltsvocabulary.Custom.AnimationClass;
import com.ieltsvocabulary.Custom.Utils;
import com.ieltsvocabulary.Interface.AnimationEndListener;
import com.ieltsvocabulary.R;
import com.razerdp.widget.animatedpieview.utils.Util;

import java.util.ArrayList;

public class LettersPractise extends AppCompatActivity implements View.OnClickListener {


    RelativeLayout relTitle, relRepeat, relNext;
    TextView txtMainTitle, txtTitle, txtSetText, txtSetUpdatedText;
    ImageView imgBack;
    CardView crdLatterPractise, crdText;
    EditText edtWritText;
    LinearLayout lnrIncorrect;

    AnimationClass animationClass;
    DisplayMetrics displayMetrics;

    int width;
    String storeChar;
    ArrayList<String> tempSpannd;
    int a = -1, tempLength = 0;

    Utils utils;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_letters_practise);


        findViewByid();
        init();
        setListner();

    }

    private void findViewByid() {
        relTitle = findViewById(R.id.relTitle);
        txtMainTitle = findViewById(R.id.txtMainTitle);
        imgBack = findViewById(R.id.imgBack);
        crdLatterPractise = findViewById(R.id.crdLatterPractise);
        crdText = findViewById(R.id.crdText);
        txtTitle = findViewById(R.id.txtTitle);
        txtSetText = findViewById(R.id.txtSetText);
        edtWritText = findViewById(R.id.edtWritText);
        txtSetUpdatedText = findViewById(R.id.txtSetUpdatedText);
        relRepeat = findViewById(R.id.relRepeat);
        lnrIncorrect = findViewById(R.id.lnrIncorrect);
        relNext = findViewById(R.id.relNext);

    }

    private void init() {
        utils = new Utils(this);
        animationClass = new AnimationClass(this);
        displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        width = displayMetrics.widthPixels;

        tempSpannd = new ArrayList<>();

        txtSetText.setText(Html.fromHtml(String.valueOf(getText(R.string.to_enrol_on))));
    }

    private void setListner() {
        imgBack.setOnClickListener(this);
        txtSetUpdatedText.setOnClickListener(this);
        relNext.setOnClickListener(this);

        edtWritText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                edtWritText.setVisibility(View.INVISIBLE);
                edtWritText.setFocusable(true);

                checkText(s, start, before, count);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void checkText(CharSequence s, int start, int before, int count) {

        String text = "(to) enrol (on)";

        if (tempLength < s.length() && tempLength != s.length() && text.length() >= s.length()) {
            tempLength = s.length();

            char textChar;
            if (count != 0) {
                textChar = text.charAt(s.length() - 1);
            } else {
                textChar = text.charAt(s.length());
            }

            char currentChar = s.charAt(s.length() - 1);
            checkCharacter(textChar, currentChar, text);
//            } else if (s.charAt(s.length() - 1) == ' ') {
//            a = a + 1;

//            String addSpace = "<font color='#26a784'><u>" + '_' + "</u></font>";
//            if (tempSpannd.size() != 0) {
            //Art = Array and Txt = TextView

            //In this Method Store Character In Array and get perticuller  Character in Array then set Character in  TextView with color
//            storeAryAndSetTxtDataWithColor(a, addSpace, "inDirect", text);

//                }
//                else{
//                    storeAryAndSetTxtDataWithColor(a, addSpace, "direct", text);
//
//                }
//            }
        } else if (tempLength > s.length() && tempLength != s.length() && text.length() >= s.length() && tempSpannd.size() != s.length()) {

            tempSpannd.remove(s.length());
            tempLength = s.length();
            a = a - 1;
            if (tempSpannd.size() != 0) {
                for (int b = 0; b < tempSpannd.size(); b++) {
                    txtSetText.setText(Html.fromHtml(tempSpannd.get(b) + "<u>" + text.substring(a + 1, text.length()) + "</u>"));
                }
            } else {
                txtSetText.setText(Html.fromHtml("<u>" + text.substring(a + 1, text.length()) + "</u>"));
            }
        }


    }

    private void checkCharacter(char textChar, char currentChar, String text) {
        a = a + 1;

        if (textChar == currentChar) {

            if (currentChar == ' ') {
                currentChar = '_';
                storeChar = "<font color='#26a784'>" + currentChar + "</font>";
            } else {
                storeChar = "<font color='#26a784'><u>" + currentChar + "</u></font>";
            }

            storeAryAndSetTxtDataWithColor(a, storeChar, text);

        } else {

            if (currentChar == ' ') {
                currentChar = '_';
                storeChar = "<font color='#DB483D'>" + currentChar + "</font>";
            } else {
                storeChar = "<font color='#DB483D'><u>" + currentChar + "</u></font>";
            }

            storeAryAndSetTxtDataWithColor(a, storeChar, text);

        }
    }

    private void storeAryAndSetTxtDataWithColor(int a, String addSpace, String text) {
        if (tempSpannd.size() != 0) {
            tempSpannd.add(tempSpannd.get(a - 1) + addSpace);
            txtSetText.setText(Html.fromHtml(tempSpannd.get(a) + "<u>" + text.substring(a + 1, text.length()) + "</u>"));
        } else {
            tempSpannd.add(a, addSpace);
            txtSetText.setText(Html.fromHtml(tempSpannd.get(a) + "<u>" + text.substring(a + 1, text.length()) + "</u>"));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.relNext:

                animationClass.moveTranslateAnimation(0, -width, 0, 0,
                        120, true, true, false, false, crdLatterPractise, false, new AnimationEndListener() {
                            @Override
                            public void onAnimationEnd() {
                                animationClass.moveTranslateAnimation(
                                        width, 0, 0, 0, 120, true, true, false, false,
                                        crdLatterPractise, false, new AnimationEndListener() {
                                            @Override
                                            public void onAnimationEnd() {

                                            }
                                        }
                                );
                            }
                        });

                Intent intent = new Intent(LettersPractise.this, OwnSentance.class);
                startActivity(intent);

                break;

            case R.id.txtSetUpdatedText:
                utils.openKeyboard(edtWritText);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        edtWritText.requestFocus();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

