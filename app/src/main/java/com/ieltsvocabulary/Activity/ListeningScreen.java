package com.ieltsvocabulary.Activity;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Adapter.ListeningAdapter;
import com.ieltsvocabulary.Custom.AnimationClass;
import com.ieltsvocabulary.Custom.SpeakText;
import com.ieltsvocabulary.Custom.Utils;
import com.ieltsvocabulary.Interface.AnimationEndListener;
import com.ieltsvocabulary.Interface.FrgPositionAnimation;
import com.ieltsvocabulary.Model.ListeningModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

public class ListeningScreen extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout relTitle, relNext;
    TextView txtMainTitle;
    ImageView imgBack;
    CardView crdListening;
    ImageView imgAudio;
    RecyclerView rcyListning;
    LinearLayout welDone;

    ArrayList<ListeningModel> listeningList;
    LinearLayoutManager listeningManager;
    ListeningAdapter listeningAdapter;

    AnimationClass animationClass;
    DisplayMetrics displayMetrics;
    Dialog dialog;
    String msg = "to put yourself or someone else to a list of members of an organisation (usually a college, university or school)";
    boolean flag = false;
    int width;
    Utils utils;
    SpeakText speakText;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listening_screen);

        findViewByid();
        intit();
        setListner();

    }

    private void findViewByid() {
        relTitle = findViewById(R.id.relTitle);
        txtMainTitle = findViewById(R.id.txtMainTitle);
        imgBack = findViewById(R.id.imgBack);
        crdListening = findViewById(R.id.crdListening);
        imgAudio = findViewById(R.id.imgAudio);
        rcyListning = findViewById(R.id.rcyListning);
        welDone = findViewById(R.id.welDone);
        relNext = findViewById(R.id.relNext);

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void intit() {

        listeningList = new ArrayList<>();
        animationClass = new AnimationClass(this);
        displayMetrics = new DisplayMetrics();
        utils = new Utils(this);
        speakText = new SpeakText(this);
        listeningManager = new LinearLayoutManager(this);

        rcyListning.setLayoutManager(listeningManager);
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        width = displayMetrics.widthPixels;

        listeningList.add(new ListeningModel("a", R.string.option_a, false));
        listeningList.add(new ListeningModel("b", R.string.option_b, false));
        listeningList.add(new ListeningModel("c", R.string.option_c, false));
        listeningList.add(new ListeningModel("d", R.string.option_d, false));

        listeningAdapter = new ListeningAdapter(this, listeningList, msg, new FrgPositionAnimation() {
            @Override
            public void frgposition(String msgOpenDialog, int currentPosition) {
                flag = false;
                if (msgOpenDialog.equalsIgnoreCase("OpenDialog")) {

                    utils.showDialogBox(R.layout.row_oops_dialog_screen);
                    welDone.setVisibility(View.GONE);
                } else if (msgOpenDialog.equalsIgnoreCase("nextScreen")){
                    flag = true;
                    welDone.setVisibility(View.VISIBLE);
                }else if(msgOpenDialog.equalsIgnoreCase("nothing")){
                    flag = true;
                }
            }
        });

        rcyListning.setAdapter(listeningAdapter);


    }

    private void setListner() {
        imgBack.setOnClickListener(this);
        imgAudio.setOnClickListener(this);
        relNext.setOnClickListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.relNext:

                if (flag) {

                    animationClass.moveTranslateAnimation(0, -width, 0, 0,
                            120, true, true, false, false, crdListening, false, new AnimationEndListener() {
                                @Override
                                public void onAnimationEnd() {
                                    animationClass.moveTranslateAnimation(
                                            width, 0, 0, 0, 120, true, true, false, false,
                                            crdListening, false, new AnimationEndListener() {
                                                @Override
                                                public void onAnimationEnd() {

                                                }
                                            }
                                    );
                                }
                            });

                   /* animationClass.moveTranslateAnimation(0, -width, 0, 0,
                            120, true, true, crdListening);

                    animationClass.startSecondAnimation(width, 0, 0, 0, 120,"no",crdListening);*/
                }

                break;


            case R.id.imgAudio:
                speakText.ReadText(msg,"male","Text");
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
