package com.ieltsvocabulary.Activity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.ieltsvocabulary.Custom.AnimationClass;
import com.ieltsvocabulary.Custom.Utils;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

public class MissingLettters extends AppCompatActivity implements View.OnClickListener {

    ConstraintLayout crdSpelling;
    RelativeLayout relTitle, relNext;
    TextView txtMainTitle;
    ImageView imgWarning, imgBack;
    LinearLayout lnrSetText, lnrHintView;
    CardView crdSetText;
    GridLayout grdOptionButton;
    MotionLayout motionLayout;
    EditText tempEdit, previousEdit = null;

    DisplayMetrics displayMetrics;
    AnimationClass animationClass;
    Utils utils;

    String text = "t-en--io-", option = "toenrois";
    String tempString;
    ArrayList<EditText> edtArray;
    int a = 0, b = 0;
    View view;
    boolean flag = false;
    int divwidth, width;
    boolean manageMotion = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_missing_lettters);

        findViewByid();
        init();
        setListner();
        setTextWithBlanckSpace("onCreate", "");
        setOptionInButtton();
        setHintData();

    }

    private void setHintData() {
        utils.setInJSONFormate(lnrHintView, this);
    }

    private void findViewByid() {
        crdSpelling = findViewById(R.id.crdSpelling);
        relTitle = findViewById(R.id.relTitle);
        txtMainTitle = findViewById(R.id.txtMainTitle);
        imgWarning = findViewById(R.id.imgWarning);
        imgBack = findViewById(R.id.imgBack);
        crdSetText = findViewById(R.id.crdSetText);
        lnrSetText = findViewById(R.id.lnrSetText);
        grdOptionButton = findViewById(R.id.grdOptionButton);
        motionLayout = findViewById(R.id.motionLayout);
        lnrHintView = findViewById(R.id.lnrHintView);


        relNext = findViewById(R.id.relNext);
    }

    private void init() {
        edtArray = new ArrayList<>();
        displayMetrics = new DisplayMetrics();
        utils = new Utils(this);
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        width = displayMetrics.widthPixels;
        animationClass = new AnimationClass(this);
    }

    private void setListner() {
        imgWarning.setOnClickListener(this);
        imgBack.setOnClickListener(this);
    }

    private void setTextWithBlanckSpace(String msg, String secMSG) {

        for (int c = 0; c < text.length(); c++) {
            char textChar = text.charAt(c);

            if (msg.equalsIgnoreCase("onCreate")) {
                if (textChar == '-') {
                    final EditText edtView = new EditText(this);
                    edtView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    a = a + 1;
                    edtView.setId(a);
                    edtView.setBackground(null);
                    edtView.setTextSize(35);
                    edtView.setText("_" );
                    edtView.setTypeface(Typeface.create("overpass_bold", Typeface.BOLD));
                    edtView.setTextColor(getResources().getColor(R.color.black_new));
                    edtView.setClickable(false);
                    edtView.setFocusable(false);
                    edtView.setGravity(Gravity.CENTER);
                    edtArray.add(edtView);

                    edtView.setOnClickListener(new View.OnClickListener() {
                        @SuppressLint("WrongConstant")
                        @Override
                        public void onClick(View v) {
                            String message = edtView.getText().toString();
                            tempEdit = edtView;

                            if (previousEdit == null) {
                                previousEdit = edtView;

                                animationClass.objectAnimatorChangeTextColor(edtView, "textColor",
                                        Color.BLACK, Color.WHITE, 1000,
                                        Animation.REVERSE, Animation.INFINITE, true);

                            } else {

                                animationClass.stopTextColorObjectAnimator();
                                previousEdit.setTextColor(getResources().getColor(R.color.black_new));
                                previousEdit = edtView;

                                animationClass.objectAnimatorChangeTextColor(previousEdit, "textColor",
                                        Color.BLACK, Color.WHITE, 1000,
                                        Animation.REVERSE, Animation.INFINITE, true);
                            }

//                            blink(edtView);

                            setTextInEditText(message, "edtSide", edtView);
                        }

                    });
                    lnrSetText.addView(edtView);
                } else {
                    b = b + 1;
                    final TextView textView = new TextView(this);
                    textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    textView.setPadding(20, 20, 20, 20);
                    textView.setId(b);
                    textView.setTypeface(Typeface.create("overpass_regular", Typeface.NORMAL));
                    textView.setText(String.valueOf(textChar));
                    textView.setTextSize(35);
                    textView.setTypeface(textView.getTypeface(), Typeface.BOLD);
                    textView.setTextColor(Color.WHITE);
                    lnrSetText.addView(textView);
                }
            } else {
                for (int j = 0; j < edtArray.size(); j++) {
                    EditText edtText = edtArray.get(j);
                    tempString = secMSG;

                    if (edtText.getText().toString().equalsIgnoreCase("_")) {
                        edtText.setText(secMSG);

                        return;
                    }
                }
            }
        }
    }

    private void blink(final EditText edtView) {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                int timeBlink = 500;
                try {
                    Thread.sleep(timeBlink);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (edtView.getVisibility() == View.VISIBLE) {
                            edtView.setVisibility(View.INVISIBLE);
                        } else {
                            edtView.setVisibility(View.VISIBLE);
                        }
                        blink(edtView);
                    }
                });
            }
        }).start();
    }

    private void setOptionInButtton() {

        divwidth = width / 4;
        for (int j = 0; j < option.length(); j++) {

            char optionChar = option.charAt(j);
            b = b + 1;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT
                    , LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.gravity =Gravity.CENTER;
            layoutParams.setMargins(20, 20, 20, 20);
            final Button buttona = new Button(this);
            buttona.setEnabled(true);
            buttona.setId(b);
            buttona.setWidth(100);
//            buttona.setPadding(20, 20, 20, 20);
            buttona.setPadding(0, 0, 0, 10);
            buttona.setTextSize(25);
            buttona.setHeight(100);
            buttona.setTypeface(Typeface.create("overpass_regular", Typeface.NORMAL));
            buttona.setGravity(Gravity.CENTER);
            buttona.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            buttona.setLayoutParams(layoutParams);
            buttona.setBackgroundResource(R.drawable.in_missing_latter_button_radious);
            buttona.setTextColor(getResources().getColor(R.color.white));
            buttona.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String message = buttona.getText().toString();
                    setTextInEditText(message, "btnSide", tempEdit);

                }
            });
            buttona.setText(String.valueOf(optionChar));
            buttona.setAllCaps(false);
            grdOptionButton.setRowCount(option.length() / 3);
            grdOptionButton.addView(buttona);
        }
    }

    private void setTextInEditText(String message, String side, EditText temp) {
        if (side.equalsIgnoreCase("edtSide")) {
            flag = true;

        } else {

            if (!flag) {
                setTextWithBlanckSpace("btnClick", message);
            } else {
                animationClass.stopTextColorObjectAnimator();
                temp.setTextColor(getResources().getColor(R.color.black_new));
                temp.setText(message);
                flag = false;
            }

        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgWarning:
                if (!manageMotion) {
                    manageMotionLayout(R.id.startHint, R.id.endHint);
                    manageMotion = true;
                } else {
                    manageMotionLayout(R.id.endHint, R.id.startHint);
                    manageMotion = false;
                }
                break;
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.relNext:

                break;
        }
    }

    private void manageMotionLayout(int startHint, int endHint) {
        motionLayout.setTransition(startHint, endHint);
        motionLayout.setTransitionDuration(500);
        motionLayout.transitionToEnd();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
