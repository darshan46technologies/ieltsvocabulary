package com.ieltsvocabulary.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.motion.widget.MotionLayout;

import com.ieltsvocabulary.R;

public class WordType extends AppCompatActivity implements View.OnClickListener {

    ImageView imgBack;
    //    ConstraintLayout constTwo;
    MotionLayout motion_word_type;
    RelativeLayout relOptionA, relOptionB, relOptionC, relOptionD, relNext;
    ImageView imgFirstCirculer, imgSecondCirculer, imgThirdCirculer, imgFourCirculer;
    TextView txtFirstCircule, txtSecondCircule, txtThirdCircule, txtFourCircule;
    boolean isContinue = true;
    View previewRelativeView;
    ImageView previewImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_type);
        findViewByid();
        init();
        setListner();
    }

    private void findViewByid() {
        imgBack = findViewById(R.id.imgBack);
//        constTwo = findViewById(R.id.constTwo);
        motion_word_type = findViewById(R.id.motion_word_type);

        relOptionA = findViewById(R.id.relOptionA);
        imgFirstCirculer = findViewById(R.id.imgFirstCirculer);
        txtFirstCircule = findViewById(R.id.txtFirstCircule);

        relOptionB = findViewById(R.id.relOptionB);
        imgSecondCirculer = findViewById(R.id.imgSecondCirculer);
        txtSecondCircule = findViewById(R.id.txtSecondCircule);

        relOptionC = findViewById(R.id.relOptionC);
        imgThirdCirculer = findViewById(R.id.imgThirdCirculer);
        txtThirdCircule = findViewById(R.id.txtThirdCircule);

        relOptionD = findViewById(R.id.relOptionD);
        imgFourCirculer = findViewById(R.id.imgFourCirculer);
        txtFourCircule = findViewById(R.id.txtFourCircule);
//        imgFirstCircule = findViewById(R.id.imgFirstCircule);

       /* imgSecondCircule = findViewById(R.id.imgSecondCircule);
        imgThirdCirculer = findViewById(R.id.imgThirdCirculer);
        imgFourCirculer = findViewById(R.id.imgFourCirculer);*/
        relNext = findViewById(R.id.relNext);
    }

    private void init() {
        startAnimation();
//        startRoundAnim();
    }

    private void startAnimation() {
        motion_word_type.setTransition(R.id.wordMotionstart, R.id.wordMotionend);
        motion_word_type.setTransitionDuration(900);
        motion_word_type.transitionToEnd();
    }

/*
    private void startRoundAnim() {
        final RotateAnimation rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(10000);
        rotate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (isContinue)
                    constTwo.startAnimation(rotate);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        constTwo.startAnimation(rotate);
    }
*/

    private void setListner() {
        imgBack.setOnClickListener(this);
        relOptionA.setOnClickListener(this);
        relOptionB.setOnClickListener(this);
        relOptionC.setOnClickListener(this);
        relOptionD.setOnClickListener(this);
//        imgFirstCircule.setOnClickListener(this);
//        imgSecondCircule.setOnClickListener(this);
//        imgThirdCirculer.setOnClickListener(this);
//        imgFourCirculer.setOnClickListener(this);
        relNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.relNext:
                isContinue = true;
                motion_word_type.setTransition(R.id.wordMotionstart, R.id.wordMotionend);
                motion_word_type.setTransitionDuration(900);
                motion_word_type.transitionToEnd();
//                startRoundAnim();
                break;

            case R.id.relOptionA:
                setRuntimeAnimation(relOptionA, 1.2f, 1.2f,
                        1.5f, 400, imgFirstCirculer, txtFirstCircule);
                break;
            case R.id.relOptionB:
                setRuntimeAnimation(relOptionB, 1.2f, 1.2f,
                        1.5f, 400, imgSecondCirculer, txtSecondCircule);
                break;
            case R.id.relOptionC:
                setRuntimeAnimation(relOptionC, 1.2f, 1.2f,
                        1.5f, 400, imgThirdCirculer, txtThirdCircule);
                break;
            case R.id.relOptionD:
                setRuntimeAnimation(relOptionD, 1.2f, 1.2f,
                        1.5f, 400, imgFourCirculer, txtFourCircule);
                break;
//            case R.id.imgFirstCircule:
//            case R.id.imgSecondCircule:
//            case R.id.imgThirdCirculer:
//            case R.id.imgFourCirculer:
//                Toast.makeText(this, "clickeddddddddd", Toast.LENGTH_SHORT).show();
//                isContinue = false;
//                break;
        }
    }

    private void setRuntimeAnimation(RelativeLayout relOption, float scaleY,
                                     float scaleX, float alpha, int duration,
                                     ImageView imgView, TextView txtView) {

        relOption.setAlpha(0.8f);
        relOption.setScaleX(1f);
        relOption.setScaleY(1f);
        relOption.animate().scaleY(scaleY).scaleX(scaleX).alpha(alpha).setDuration(duration);
        if (previewRelativeView == null) {
            previewRelativeView = relOption;
            previewImage = imgView;
        } else {
            previewRelativeView.animate().scaleY(1f).scaleX(1f).alpha(1f).setDuration(duration);
            previewImage.setImageResource(R.drawable.layout_circuler_dark_blue);
            previewRelativeView = relOption;
            previewImage = imgView;
        }
        checkAnsTrueOrFalse(imgView, txtView);

    }

    private void checkAnsTrueOrFalse(ImageView imgView, TextView txtView) {
        String msg = "verb";
        String txtMsg = txtView.getText().toString();
        if (msg.equalsIgnoreCase(txtMsg)) {
            imgView.setImageResource(R.drawable.layout_circuler_green_color);
        }else{
            imgView.setImageResource(R.drawable.layout_circuler_red_color);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
