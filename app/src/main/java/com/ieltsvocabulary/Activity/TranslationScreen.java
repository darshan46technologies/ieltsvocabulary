package com.ieltsvocabulary.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.ieltsvocabulary.Custom.AnimationClass;
import com.ieltsvocabulary.Custom.SpeakText;
import com.ieltsvocabulary.Custom.Utils;
import com.ieltsvocabulary.Interface.AnimationEndListener;
import com.ieltsvocabulary.R;

public class TranslationScreen extends AppCompatActivity implements View.OnClickListener {

    ImageView imgBack, imgSpeachAudio;
    CardView crdTranslate;
    TextView txtMainTitle, txtTitleWordType, txtWordType, txtTitlePhoneticTran, txtPhoneticTran,
            txtTitleMean, txtMeaning, txtTitleTranslate, txtSentanceTitle, txtSentance, txtSentanceTitleTwo, txtSentanceTwo;
    EditText edtTranslation;
    RelativeLayout relNext;
    ScrollView scrollView;

    AnimationClass animationClass;
    DisplayMetrics displayMetrics;
    SpeakText speakText;
    Utils utils;


    private long backpressagain;

//    CustomeLinearLayout customeLinearLayout;

    float width;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_translation_screen);

        findViewId();
        init();
        setListner();
    }

    private void findViewId() {
        imgBack = findViewById(R.id.imgBack);
        crdTranslate = findViewById(R.id.crdTranslate);
        txtMainTitle = findViewById(R.id.txtMainTitle);
        imgSpeachAudio = findViewById(R.id.imgSpeachAudio);
        txtTitleWordType = findViewById(R.id.txtTitleWordType);
        txtWordType = findViewById(R.id.txtWordType);
        txtTitlePhoneticTran = findViewById(R.id.txtTitlePhoneticTran);
        txtPhoneticTran = findViewById(R.id.txtPhoneticTran);
        txtTitleMean = findViewById(R.id.txtTitleMean);
        txtMeaning = findViewById(R.id.txtMeaning);
        txtTitleTranslate = findViewById(R.id.txtTitleTranslate);
        edtTranslation = findViewById(R.id.edtTranslation);
        txtSentanceTitle = findViewById(R.id.txtSentanceTitle);
        txtSentance = findViewById(R.id.txtSentance);
        txtSentanceTitleTwo = findViewById(R.id.txtSentanceTitleTwo);
        txtSentanceTwo = findViewById(R.id.txtSentanceTwo);
        scrollView = findViewById(R.id.scrollView);
        relNext = findViewById(R.id.relNext);
    }

    private void init() {
        animationClass = new AnimationClass(this);
        displayMetrics = new DisplayMetrics();
        utils = new Utils(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            speakText = new SpeakText(this);
        }

        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        width = displayMetrics.widthPixels;
    }

    private void setListner() {
        imgBack.setOnClickListener(this);
        imgSpeachAudio.setOnClickListener(this);
        relNext.setOnClickListener(this);
        edtTranslation.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        edtTranslation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (!utils.isShowingDialog()) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        utils.showTranscriptKeyboard(edtTranslation);
                        scrollView.post(new Runnable() {
                            @Override
                            public void run() {
                                scrollView.scrollTo(0, edtTranslation.getBottom());
                            }
                        });
                    }
                }
                return true;
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.relNext:

                animationClass.moveTranslateAnimation(0, -width, 0, 0,
                        120, true, true, false, false, crdTranslate, false, new AnimationEndListener() {
                            @Override
                            public void onAnimationEnd() {
                                animationClass.moveTranslateAnimation(
                                        width, 0, 0, 0, 120, true, true, false,
                                        false, crdTranslate, false, new AnimationEndListener() {
                                            @Override
                                            public void onAnimationEnd() {

                                            }
                                        }
                                );
                            }
                        });

            /*    animationClass.moveTranslateAnimation(0,-width,0,0,
                        120,true,true,crdInformation);

                animationClass.startSecondAnimation(width,0,0,0,
                        120,"no",crdInformation);*/
                Intent intent = new Intent(TranslationScreen.this, LettersPractise.class);
                startActivity(intent);

                break;

            case R.id.imgSpeachAudio:
                speakText.ReadText(txtMainTitle.getText().toString(), "male", "Text");
                break;
//                onBackPressed();

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
