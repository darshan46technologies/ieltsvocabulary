package com.ieltsvocabulary.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.navigation.NavigationView;
import com.ieltsvocabulary.Adapter.MenuListAdapter;
import com.ieltsvocabulary.Adapter.SubjectViewPagerAdapter;
import com.ieltsvocabulary.Custom.ZoomPageTransformer;
import com.ieltsvocabulary.Interface.FrgPositionAnimation;
import com.ieltsvocabulary.Interface.MenuItemsInterface;
import com.ieltsvocabulary.Model.MenuItemModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SubjectScreen extends FragmentActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {
    //    CardView crdone, crdtwo;

    DrawerLayout drawerLayout;
    ImageView imgProfile, imgOption, imgPlay, imgPiIcon;
    Button btnStart;
    NavigationView navigationView;
    RecyclerView rcyMenuItems;

    ActionBarDrawerToggle actionBarDrawerToggle;
    FrgPositionAnimation frgPositionAnimation;

    public static ViewPager vp;
    SubjectViewPagerAdapter subjectViewPager;
    MenuListAdapter menuListAdapter;
    LinearLayoutManager linearLayoutManager;

    ArrayList<MenuItemModel> listMenuItems;
    int tempPosition = -1;
    CircleImageView tempView = null;
    ImageView tempImageView = null;
    TextView tempTextView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_screen);

        findviewid();
        init();
        setListner();
    }

    private void findviewid() {
        vp = findViewById(R.id.vp);
        drawerLayout = findViewById(R.id.drawerLayout);
        imgProfile = findViewById(R.id.imgProfile);
        imgOption = findViewById(R.id.imgOption);
        btnStart = findViewById(R.id.btnStart);
        imgPlay = findViewById(R.id.imgPlay);
        imgPiIcon = findViewById(R.id.imgPiIcon);

        navigationView = findViewById(R.id.navigationView);
        rcyMenuItems = findViewById(R.id.rcyMenuItems);


    }

    private void init() {
        imgPlay.setVisibility(View.VISIBLE);
        vp.setOffscreenPageLimit(0);
        subjectViewPager = new SubjectViewPagerAdapter(getSupportFragmentManager());
        vp.setPageTransformer(true, new ZoomPageTransformer());
        vp.setAdapter(subjectViewPager);

        linearLayoutManager = new LinearLayoutManager(this);
        rcyMenuItems.setLayoutManager(linearLayoutManager);
        listMenuItems = new ArrayList<>();
        listMenuItems.add(new MenuItemModel(R.drawable.education, R.string.education));
        listMenuItems.add(new MenuItemModel(R.drawable.graphstables, R.string.graphsTables));
        listMenuItems.add(new MenuItemModel(R.drawable.environment, R.string.environment));
        listMenuItems.add(new MenuItemModel(R.drawable.science, R.string.scienceTechnology));
        listMenuItems.add(new MenuItemModel(R.drawable.ttt, R.string.ttt));
        listMenuItems.add(new MenuItemModel(R.drawable.health, R.string.health));
        listMenuItems.add(new MenuItemModel(R.drawable.work, R.string.work));
        listMenuItems.add(new MenuItemModel(R.drawable.money, R.string.money));
        listMenuItems.add(new MenuItemModel(R.drawable.crimelaw, R.string.crimeLaw));
        listMenuItems.add(new MenuItemModel(R.drawable.family, R.string.familyRelationShips));
        listMenuItems.add(new MenuItemModel(R.drawable.architecture, R.string.architecture));
        listMenuItems.add(new MenuItemModel(R.drawable.sport, R.string.sportLeisure));
        listMenuItems.add(new MenuItemModel(R.drawable.advertising, R.string.advertising));
        listMenuItems.add(new MenuItemModel(R.drawable.geography, R.string.geography));
        listMenuItems.add(new MenuItemModel(R.drawable.celebrations, R.string.celebrations));
        listMenuItems.add(new MenuItemModel(R.drawable.generalacedamy, R.string.generalAcademic));
        listMenuItems.add(new MenuItemModel(R.drawable.themedia, R.string.theMedia));
        listMenuItems.add(new MenuItemModel(R.drawable.processes, R.string.processes));
        listMenuItems.add(new MenuItemModel(R.drawable.popular, R.string.popularArtsandCulture));

        menuListAdapter = new MenuListAdapter(this, listMenuItems, new MenuItemsInterface() {
            @Override
            public void items(int position, CircleImageView view, ImageView imageView, TextView textView) {

                if(tempView == null){
//                    setColonView(position,view,imageView,textView);
                    tempView = view;
                    tempImageView = imageView;
                    tempTextView = textView;
                    view.setImageResource(R.color.red);
                    imageView.setColorFilter(getResources().getColor(R.color.red));
                    textView.setTextColor(getResources().getColor(R.color.red));
                }else{
                    tempView.setImageResource(0);
                    tempImageView.setColorFilter(getResources().getColor(R.color.gray));
                    tempTextView.setTextColor(getResources().getColor(R.color.gray));
                    tempView = view;
                    tempImageView = imageView;
                    tempTextView = textView;
                    view.setImageResource(R.color.red);
                    imageView.setColorFilter(getResources().getColor(R.color.red));
                    textView.setTextColor(getResources().getColor(R.color.red));
                }

                view.setImageResource(R.color.red);
                imageView.setColorFilter(getResources().getColor(R.color.red));
                textView.setTextColor(getResources().getColor(R.color.red));

            }
        });
        rcyMenuItems.setAdapter(menuListAdapter);
    }

//    private void setColonView(int position, CircleImageView view, ImageView imageView, TextView textView) {
//    }

    private void setListner() {
        imgOption.setOnClickListener(this);
        imgProfile.setOnClickListener(this);
        navigationView.setNavigationItemSelectedListener(this);
        imgPlay.setOnClickListener(this);
        imgPiIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgProfile:
                Intent intent = new Intent(SubjectScreen.this, ProfileScreen.class);
                startActivity(intent);
                break;
            case R.id.imgOption:
                actionBarDrawerToggle = new ActionBarDrawerToggle(SubjectScreen.this, drawerLayout, R.string.open, R.string.close);
                drawerLayout.addDrawerListener(actionBarDrawerToggle);
                drawerLayout.openDrawer(GravityCompat.END);
                actionBarDrawerToggle.syncState();
                break;
            case R.id.imgPlay:
                vp.setOffscreenPageLimit(0);
                int a = vp.getCurrentItem();
                Log.d("a", String.valueOf(a));

                frgPositionAnimation.frgposition("start", vp.getCurrentItem());

                break;

            case R.id.imgPiIcon:
                Intent piScreen = new Intent(SubjectScreen.this, PiTopicScreen.class);
                startActivity(piScreen);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;

        }
        return super.onOptionsItemSelected(item);

    }

    public void setinterface(FrgPositionAnimation frgPositionAnimation) {
        this.frgPositionAnimation = frgPositionAnimation;

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }
}