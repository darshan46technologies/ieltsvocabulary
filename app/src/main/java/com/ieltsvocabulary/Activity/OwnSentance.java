package com.ieltsvocabulary.Activity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.ieltsvocabulary.Custom.AnimationClass;
import com.ieltsvocabulary.Interface.AnimationEndListener;
import com.ieltsvocabulary.R;

public class OwnSentance extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout relTitle, relNext;
    TextView txtMainTitle;
    ImageView imgBack;
    CardView crdOwnSentence, addLayout;
    LinearLayout lnrMain, lnraddlayout;
    TextView txtSentenceOneTitle, txtSentanceOne, txtSentanceTwoTitle, txtSentanceTwo, txtSentanceThreeTitle,
            txtSentanceThree, txtSentanceFourTitle;
    EditText edtAddSentence;

    AnimationClass animationClass;
    DisplayMetrics displayMetrics;
    int a = 4;

    float width;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_own_sentance);

        findViewByid();
        init();
        setListner();

    }

    private void findViewByid() {
        relTitle = findViewById(R.id.relTitle);
        txtMainTitle = findViewById(R.id.txtMainTitle);
        imgBack = findViewById(R.id.imgBack);
        crdOwnSentence = findViewById(R.id.crdOwnSentence);
        lnrMain = findViewById(R.id.lnrMain);
        lnraddlayout = findViewById(R.id.lnraddlayout);
        txtSentenceOneTitle = findViewById(R.id.txtSentenceOneTitle);
        txtSentanceOne = findViewById(R.id.txtSentanceOne);
        txtSentanceTwoTitle = findViewById(R.id.txtSentanceTwoTitle);
        txtSentanceTwo = findViewById(R.id.txtSentanceTwo);
        txtSentanceThreeTitle = findViewById(R.id.txtSentanceThreeTitle);
        txtSentanceThree = findViewById(R.id.txtSentanceThree);
        txtSentanceFourTitle = findViewById(R.id.txtSentanceFourTitle);
        edtAddSentence = findViewById(R.id.edtAddSentence);
        addLayout = findViewById(R.id.addLayout);
        relNext = findViewById(R.id.relNext);
    }

    private void init() {
        animationClass = new AnimationClass(this);
        displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        width = displayMetrics.widthPixels;
    }

    private void setListner() {
        imgBack.setOnClickListener(this);
        relNext.setOnClickListener(this);
        addLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;

            case R.id.relNext:

                animationClass.moveTranslateAnimation(0, -width, 0, 0,
                        120, true, true, false, false, crdOwnSentence, false, new AnimationEndListener() {
                            @Override
                            public void onAnimationEnd() {
                                animationClass.moveTranslateAnimation(
                                        width, 0, 0, 0, 120, true, true, false,
                                        false, crdOwnSentence, false, new AnimationEndListener() {
                                            @Override
                                            public void onAnimationEnd() {

                                            }
                                        }
                                );
                            }
                        });

               /* animationClass.moveTranslateAnimation(0, -width, 0, 0,
                        120, true, true, crdOwnSentence);
                animationClass.startSecondAnimation(width, 0, 0, 0, 120,"no",crdOwnSentence);*/
                break;

            case R.id.addLayout:

                LinearLayout.LayoutParams linearLayout = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                TextView txtSentanceFourTitle;
                View view = LayoutInflater.from(this).inflate(R.layout.add_sample_senetance_layout, null);
                txtSentanceFourTitle = view.findViewById(R.id.txtSentanceFourTitle);
                linearLayout.setMargins(0, 20, 0, 20);
                txtSentanceFourTitle.setText("Sample Sentence  " + a++);
                lnraddlayout.addView(view, linearLayout);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
