package com.ieltsvocabulary.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.google.android.material.navigation.NavigationView;
import com.ieltsvocabulary.Adapter.DataAdapterHome;
import com.ieltsvocabulary.Adapter.MenuListAdapterNew;
import com.ieltsvocabulary.Custom.ZoomAnimatedLinearManager;
import com.ieltsvocabulary.Interface.AdapterItemClickCallback;
import com.ieltsvocabulary.Interface.Click_Listner_Interface;
import com.ieltsvocabulary.Model.HomeDataModel;
import com.ieltsvocabulary.Model.WordDataModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_DRAGGING;
import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE;
import static androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_SETTLING;

public class HomeScreen extends AppCompatActivity implements View.OnClickListener {

    RecyclerView home_data_items_recy, rcyMenuItems;
    ArrayList<HomeDataModel> homeDataModels;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    ImageView imgProfile, imgOption, img_back;
    int current_menu_position = 0;
    MenuListAdapterNew menuListAdapterNew;
    DataAdapterHome dataAdapterHome;
    RelativeLayout play_btn_graph, play_btn_nxt, back;
    CircleImageView play_btn_nxt_cir, play_btn_graph_cir, back_cir;
    CardView start_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_testing);

        home_data_items_recy = findViewById(R.id.data);
        drawerLayout = findViewById(R.id.drawerLayout);
        navigationView = findViewById(R.id.navigationView);
        rcyMenuItems = findViewById(R.id.rcyMenuItems);
        imgProfile = findViewById(R.id.imgProfile);
        imgOption = findViewById(R.id.imgOption);
        img_back = findViewById(R.id.img_back);
        play_btn_nxt = findViewById(R.id.play_btn_nxt);
        play_btn_graph = findViewById(R.id.play_btn_graph);
        play_btn_nxt_cir = findViewById(R.id.play_btn_nxt_cir);
        play_btn_graph_cir = findViewById(R.id.play_btn_graph_cir);
        start_btn = findViewById(R.id.start_btn);
        back_cir = findViewById(R.id.back_cir);
        back = findViewById(R.id.back);

        getHomeModels();
        setCurrentColor(0, 0f);

        imgOption.setOnClickListener(this);
        imgProfile.setOnClickListener(this);
        play_btn_nxt.setOnClickListener(this);
        play_btn_graph.setOnClickListener(this);
        start_btn.setOnClickListener(this);
        back.setOnClickListener(this);

        drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                menuListAdapterNew.setCurrentPos(getCurrentHomePos());
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        rcyMenuItems.setLayoutManager(new LinearLayoutManager(this));

        menuListAdapterNew = new MenuListAdapterNew(this, homeDataModels, current_menu_position, new Click_Listner_Interface() {
            @Override
            public void setAction(int position) {
                current_menu_position = position;
                drawerLayout.closeDrawer(GravityCompat.END);
                runOnUiThread(new Runnable() {
                    public void run() {
                        home_data_items_recy.smoothScrollToPosition(current_menu_position);
                        setCurrentColor(current_menu_position, 0f);
                    }
                });
            }
        });
        rcyMenuItems.setAdapter(menuListAdapterNew);

        home_data_items_recy.setLayoutManager(new ZoomAnimatedLinearManager(this, LinearLayoutManager.HORIZONTAL, false));
        SnapHelper mSnapHelper = new PagerSnapHelper();
        mSnapHelper.attachToRecyclerView(home_data_items_recy);
        dataAdapterHome = new DataAdapterHome(this, homeDataModels, new AdapterItemClickCallback() {
            @Override
            public void response(Object o, int type) {
                if (type == 0) {
                    homeDataModels.get(getCurrentHomePos()).setCurrent_lay_state(0);
                    setCurrentColor(getCurrentHomePos(), 0f);
                }
            }
        });
        home_data_items_recy.setAdapter(dataAdapterHome);

        home_data_items_recy.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (newState) {
                    case SCROLL_STATE_IDLE:
                        setCurrentColor(getCurrentHomePos(), 0.3f);
                        break;
                    case SCROLL_STATE_DRAGGING:
                    case SCROLL_STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                play_btn_nxt.setAlpha((float) 0.3);
                play_btn_graph.setAlpha((float) 0.3);
                start_btn.setAlpha((float) 0.3);
                back.setVisibility(View.GONE);
            }
        });
    }

    private int getCurrentHomePos() {
        LinearLayoutManager layoutManager = ((LinearLayoutManager) home_data_items_recy.getLayoutManager());
        int firstVisiblePosition = Objects.requireNonNull(layoutManager).findFirstCompletelyVisibleItemPosition();
        return firstVisiblePosition;
    }

    private void setCurrentColor(int current_menu_position, float startAlpha) {
        if (current_menu_position >= 0) {
            play_btn_nxt.setVisibility(View.GONE);
            play_btn_graph.setVisibility(View.GONE);
            start_btn.setVisibility(View.GONE);
            back.setVisibility(View.GONE);

            switch (homeDataModels.get(current_menu_position).getCurrent_lay_state()) {
                case 0:
                    play_btn_nxt.setAlpha(startAlpha);
                    play_btn_graph.setAlpha(startAlpha);
                    play_btn_nxt.setVisibility(View.VISIBLE);
                    play_btn_graph.setVisibility(View.VISIBLE);
                    play_btn_nxt.animate().alpha(1f).setDuration(700);
                    play_btn_graph.animate().alpha(1f).setDuration(700);
                    play_btn_nxt_cir.setImageResource(homeDataModels.get(current_menu_position).getColor_code());
                    play_btn_graph_cir.setImageResource(homeDataModels.get(current_menu_position).getColor_code());
                    break;
                case 1:
                    start_btn.setAlpha(startAlpha);
                    start_btn.setVisibility(View.VISIBLE);
                    start_btn.animate().alpha(1f).setDuration(700);
                    start_btn.setCardBackgroundColor(getResources().getColor(homeDataModels.get(current_menu_position).getColor_code()));
                    break;
                case 2:
                    back.setAlpha(startAlpha);
                    back.setVisibility(View.VISIBLE);
                    back.animate().alpha(1f).setDuration(700);
                    back_cir.setImageResource(homeDataModels.get(current_menu_position).getColor_code());
                    break;
            }
        }
    }

    private void setVisibilityGone(final View view, int action) {
        // 0 for gone // 1 for visible
        if (action == 0) {
            view.animate()
                    .alpha(0f)
                    .setDuration(700)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            view.setVisibility(View.GONE);
                        }
                    });
        }
    }

    private ArrayList<HomeDataModel> getHomeModels() {
        homeDataModels = new ArrayList<>();
        homeDataModels.add(new HomeDataModel(R.drawable.taskcardgreen, R.string.dic_ɪnˈmeaning, R.string.education, R.drawable.education, R.color.education, 0));
        homeDataModels.add(new HomeDataModel(R.drawable.purpal, R.string.dic_ɪnˈmeaning, R.string.graphsTables, R.drawable.graphstables, R.color.graphs, 0));
        homeDataModels.add(new HomeDataModel(R.drawable.blurred, R.string.dic_ɪnˈmeaning, R.string.environment, R.drawable.environment, R.color.environment, 0));
        homeDataModels.add(new HomeDataModel(R.drawable.yellow, R.string.dic_ɪnˈmeaning, R.string.scienceTechnology, R.drawable.science, R.color.science, 0));
        homeDataModels.add(new HomeDataModel(R.drawable.darkpupl, R.string.dic_ɪnˈmeaning, R.string.ttt, R.drawable.ttt, R.color.travel, 0));
        homeDataModels.add(new HomeDataModel(R.drawable.blue, R.string.dic_ɪnˈmeaning, R.string.health, R.drawable.health, R.color.health, 0));
        homeDataModels.add(new HomeDataModel(R.drawable.lightmaroon, R.string.dic_ɪnˈmeaning, R.string.work, R.drawable.work, R.color.work, 0));
        homeDataModels.add(new HomeDataModel(R.drawable.lightblue, R.string.dic_ɪnˈmeaning, R.string.money, R.drawable.money, R.color.money, 0));
        homeDataModels.add(new HomeDataModel(R.drawable.org, R.string.dic_ɪnˈmeaning, R.string.crimeLaw, R.drawable.crimelaw, R.color.crime, 0));
        homeDataModels.add(new HomeDataModel(R.drawable.maroon, R.string.dic_ɪnˈmeaning, R.string.familyRelationShips, R.drawable.family, R.color.family, 0));
        homeDataModels.add(new HomeDataModel(R.drawable.lightpur, R.string.dic_ɪnˈmeaning, R.string.architecture, R.drawable.architecture, R.color.architechture, 0));
        homeDataModels.add(new HomeDataModel(R.drawable.skyblue, R.string.dic_ɪnˈmeaning, R.string.sportLeisure, R.drawable.sport, R.color.sport, 0));
        homeDataModels.add(new HomeDataModel(R.drawable.coffee, R.string.dic_ɪnˈmeaning, R.string.advertising, R.drawable.advertising, R.color.adverti, 0));
        homeDataModels.add(new HomeDataModel(R.drawable.red, R.string.dic_ɪnˈmeaning, R.string.geography, R.drawable.geography, R.color.geography, 0));
        homeDataModels.add(new HomeDataModel(R.drawable.celebration, R.string.dic_ɪnˈmeaning, R.string.celebrations, R.drawable.celebrations, R.color.celebration, 0));
        homeDataModels.add(new HomeDataModel(R.drawable.lightcofee, R.string.dic_ɪnˈmeaning, R.string.generalAcademic, R.drawable.generalacedamy, R.color.general, 0));
        homeDataModels.add(new HomeDataModel(R.drawable.newcolor, R.string.dic_ɪnˈmeaning, R.string.theMedia, R.drawable.themedia, R.color.themedia, 0));
        homeDataModels.add(new HomeDataModel(R.drawable.mahendi, R.string.dic_ɪnˈmeaning, R.string.processes, R.drawable.processes, R.color.process, 0));
        homeDataModels.add(new HomeDataModel(R.drawable.lightorg, R.string.dic_ɪnˈmeaning, R.string.popularArtsandCulture, R.drawable.popular, R.color.popularart, 0));
        return homeDataModels;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgProfile:
                startActivity(new Intent(HomeScreen.this, ProfileScreen.class));
                break;
            case R.id.imgOption:
                drawerLayout.openDrawer(GravityCompat.END);
                break;
            case R.id.play_btn_nxt:
                Objects.requireNonNull(home_data_items_recy.findViewHolderForAdapterPosition(getCurrentHomePos())).itemView.findViewById(R.id.play_btn_nxt).performClick();
                homeDataModels.get(getCurrentHomePos()).setCurrent_lay_state(1);
                setCurrentColor(getCurrentHomePos(), 0f);
                break;
            case R.id.play_btn_graph:
                startActivity(new Intent(this, PiTopicScreen.class));
                break;
            case R.id.start_btn:
                Objects.requireNonNull(home_data_items_recy.findViewHolderForAdapterPosition(getCurrentHomePos())).itemView.findViewById(R.id.start_btn).performClick();
                homeDataModels.get(getCurrentHomePos()).setCurrent_lay_state(2);
                setCurrentColor(getCurrentHomePos(), 0f);
                break;
            case R.id.back:
                Objects.requireNonNull(home_data_items_recy.findViewHolderForAdapterPosition(getCurrentHomePos())).itemView.findViewById(R.id.back).performClick();
                homeDataModels.get(getCurrentHomePos()).setCurrent_lay_state(1);
                setCurrentColor(getCurrentHomePos(), 0f);
                break;
        }
    }
}
