package com.ieltsvocabulary.Activity;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Adapter.MatchTheMeaningPairAdapter;
import com.ieltsvocabulary.Adapter.MatchTheWordPairAdapter;
import com.ieltsvocabulary.Custom.AnimationClass;
import com.ieltsvocabulary.Interface.AnimationEndListener;
import com.ieltsvocabulary.Interface.FrgPositionAnimation;
import com.ieltsvocabulary.Interface.MatchThePairsInterface;
import com.ieltsvocabulary.Model.MatchTheMeaningPairModel;
import com.ieltsvocabulary.Model.MatchTheWordPairModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

public class MatchThePairsScreen extends AppCompatActivity implements View.OnClickListener {

    ConstraintLayout conMain;
    RelativeLayout relTitle, relNext;
    TextView txtMainTitle, txtWord;
    ImageView imgBack;
    CardView crdMatchThePair;
    LinearLayout lnrWord, lnrMeaning;
    RecyclerView rcyWord, rcyMeaning;

    //Model Class
    ArrayList<MatchTheWordPairModel> listWordData;
    ArrayList<MatchTheMeaningPairModel> listMeaningData;

    int colorArray[] = {R.color.black_new, R.color.yellow, R.color.red, R.color.black, R.color.assessment_purple, R.color.blue
            , R.color.coffeeColor, R.color.dark_blue, R.color.dark_maroon, R.color.diff_green, R.color.green
            , R.color.newpurple, R.color.true_green, R.color.bluer_red, R.color.diff_dark_blue};

    MatchTheWordPairAdapter matchTheWordPairAdapter;
    MatchTheMeaningPairAdapter matchTheMeaningPairAdapter;


    AnimationClass animationClass;
    DisplayMetrics displayMetrics;

    float width;

    CardView tempCrdView;
    TextView tempTxtView;

    //used to store view and position
    ArrayList<CardView> listOfWordCard;
    ArrayList<TextView> listOfWordText;
    ArrayList<CardView> listOfMeaningCard;
    ArrayList<TextView> listOfMeaningText;
    ArrayList<Integer> listWordPosition;
    ArrayList<Integer> listMeaningPosition;

    int sameColor = 0;
    int tempPos = -1;
    boolean usedToLocation = true, manageData = false,
            manageSetColorMethodData = true, managePairs = false, removePairMethod = false;

    ColorStateList tempCardWordBgColor, tempCardMeaningBgColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_the_pairs_screen);

        findViewByid();
        init();
        setListner();

    }

    private void findViewByid() {
        conMain = findViewById(R.id.conMain);
        relTitle = findViewById(R.id.relTitle);
        txtMainTitle = findViewById(R.id.txtMainTitle);
        imgBack = findViewById(R.id.imgBack);
        crdMatchThePair = findViewById(R.id.crdMatchThePair);
        lnrWord = findViewById(R.id.lnrWord);
        txtWord = findViewById(R.id.txtWord);
        rcyWord = findViewById(R.id.rcyWord);
        lnrMeaning = findViewById(R.id.lnrMeaning);
        rcyMeaning = findViewById(R.id.rcyMeaning);
        relNext = findViewById(R.id.relNext);
    }

    private void init() {

        animationClass = new AnimationClass(this);

        listOfWordCard = new ArrayList<>();
        listOfWordText = new ArrayList<>();

        listOfMeaningCard = new ArrayList<>();
        listOfMeaningText = new ArrayList<>();

        listMeaningPosition = new ArrayList<>();
        listWordPosition = new ArrayList<>();

        displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        width = displayMetrics.widthPixels;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rcyWord.setLayoutManager(linearLayoutManager);

        listWordData = new ArrayList<>();
        listWordData.add(new MatchTheWordPairModel(R.string.to_enrol_on));
        listWordData.add(new MatchTheWordPairModel(R.string.to_benefit_from));
        listWordData.add(new MatchTheWordPairModel(R.string.to_follow));
        listWordData.add(new MatchTheWordPairModel(R.string.to_specialise_in));
//        listWordData.add(new MatchTheWordPairModel(R.string.open, false));
//        listWordData.add(new MatchTheWordPairModel(R.string.open));
//        listWordData.add(new MatchTheWordPairModel(R.string.close));
        matchTheWordPairAdapter = new MatchTheWordPairAdapter(this, listWordData, new MatchThePairsInterface() {
            @Override
            public void fillColorInView(int position, CardView cardView, TextView textView) {
                setAndCheckPair(position, cardView, textView, "Word");
            }
        }, new FrgPositionAnimation() {
            @Override
            public void frgposition(String msgOpenDialog,int currentPosition) {

            }
        });
        rcyWord.setAdapter(matchTheWordPairAdapter);

        listMeaningData = new ArrayList<>();
        listMeaningData.add(new MatchTheMeaningPairModel(R.string.option_b));
        listMeaningData.add(new MatchTheMeaningPairModel(R.string.option_c));
        listMeaningData.add(new MatchTheMeaningPairModel(R.string.option_d));
        listMeaningData.add(new MatchTheMeaningPairModel(R.string.option_a));
//        listMeaningData.add(new MatchTheMeaningPairModel(R.string.open));
//        listMeaningData.add(new MatchTheMeaningPairModel(R.string.close));

        matchTheMeaningPairAdapter = new MatchTheMeaningPairAdapter(this, listMeaningData, new MatchThePairsInterface() {
            @Override
            public void fillColorInView(int position, CardView cardView, TextView textView) {

                setAndCheckPair(position, cardView, textView, "Meaning");

            }
        }, new FrgPositionAnimation() {
            @Override
            public void frgposition(String flag, int currentPosition) {

            }
        });
        rcyMeaning.setAdapter(matchTheMeaningPairAdapter);

        LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(this);
        rcyMeaning.setLayoutManager(linearLayoutManager1);
    }

    private void setAndCheckPair(int position, CardView cardView, TextView textView, String msg) {

        ColorStateList colorStateList = ColorStateList.valueOf(textView.getCurrentTextColor());
        ColorStateList colorTwo = ColorStateList.valueOf(getResources().getColor(R.color.sky_blue));

        if (colorStateList == colorTwo) {


            if (msg.equalsIgnoreCase("Word") && usedToLocation) {

                usedToLocation = false;
                manageData = false;
                removePairMethod = false;

                manageCurrentAndPreviewView("direct", position, cardView, textView, "normal");

            } else if (msg.equalsIgnoreCase("Word") && !usedToLocation) {
                removePairMethod = false;
                if (!manageData) {

                    manageCurrentAndPreviewView("notDirect", position, cardView, textView, "normal");

                } else {

                    manageCurrentAndPreviewView("direct", position, cardView, textView, "normal");

                    manageData = false;
                }
            } else if (msg.equalsIgnoreCase("Meaning") && !usedToLocation) {
                removePairMethod = false;
                usedToLocation = true;

                if (!manageData) {
                    manageData = true;
                    manageCurrentAndPreviewView("direct", position, cardView, textView, "firstTime");
                } else {
                    manageCurrentAndPreviewView("notDirect", position, cardView, textView, "firstTime");
                }

            } else if (msg.equalsIgnoreCase("Meaning") && usedToLocation) {

                if (tempCrdView != null) {
                    manageData = true;
                    usedToLocation = false;
                    removePairMethod = false;

                    manageCurrentAndPreviewView("notDirect", position, cardView, textView, "firstTime");

                }

            }
        } else if (listMeaningPosition.size() != 0 && msg.equalsIgnoreCase("Meaning")) {

            for (int a = 0; a < listMeaningPosition.size(); a++) {

                if (listMeaningPosition.get(a) == position && !removePairMethod) {

                    removePairAndSingle(a, cardView, textView, position, msg);

                    return;
                }
            }
        } else if (listWordPosition.size() != 0 && msg.equalsIgnoreCase("Word")) {

            for (int a = 0; a < listWordPosition.size(); a++) {

                if (listWordPosition.get(a) == position) {

                    removePairAndSingle(a, cardView, textView, position, msg);
                    return;
                }
            }

        }


    }

    private void removePairAndSingle(int cc, CardView cardView, TextView textView, int position, String msg) {


        if (listOfWordCard.size() >= listOfMeaningCard.size()) {

            if (managePairs && !msg.equalsIgnoreCase("Word")) {

                usedToLocation = true;
                manageData = false;
                manageSetColorMethodData = true;
                removeDataInArrayWithCardColor(cc, "meaningSideChange", position, cardView, textView, msg);


            } else if (!managePairs && msg.equalsIgnoreCase("Word")) {

                removeDataInArrayWithCardColor(cc, "bothPairRemove", position, cardView, textView, msg);

            } else if (!managePairs && !msg.equalsIgnoreCase("Word")) {

                removeDataInArrayWithCardColor(cc, "effectMeaning", position, cardView, textView, msg);

            }
        }
    }

    private void removeDataInArrayWithCardColor(int cc, String msg, int position, CardView cardView, TextView textView, String msges) {

        if (listMeaningPosition.get(listMeaningPosition.size() - 1) != position
                || msges.equalsIgnoreCase("Word")) {

            if (msg.equalsIgnoreCase("bothPairRemove")) {
                if (listOfMeaningCard.get(cc).getCardBackgroundColor() == tempCardMeaningBgColor) {
                    sameColor = 0;
                    usedToLocation = true;
                    manageData = false;
                    tempCrdView = null;
                    removePairMethod = true;
                }
            }


            listOfWordCard.get(cc).setCardBackgroundColor(getResources().getColor(R.color.white));
            listOfWordText.get(cc).setTextColor(getResources().getColor(R.color.sky_blue));

            listOfMeaningCard.get(cc).setCardBackgroundColor(getResources().getColor(R.color.white));
            listOfMeaningText.get(cc).setTextColor(getResources().getColor(R.color.sky_blue));

            listOfWordCard.remove(cc);
            listOfWordText.remove(cc);
            listWordPosition.remove(cc);

            listOfMeaningCard.remove(cc);
            listOfMeaningText.remove(cc);
            listMeaningPosition.remove(cc);


            if (!msg.equalsIgnoreCase("bothPairRemove")) {
                manageCurrentAndPreviewView("different", position, cardView, textView, "firstTime");
            }
        } else if (tempCardMeaningBgColor != tempCardWordBgColor) {
            listOfWordCard.get(cc).setCardBackgroundColor(getResources().getColor(R.color.white));
            listOfWordText.get(cc).setTextColor(getResources().getColor(R.color.sky_blue));


            listOfMeaningCard.get(cc).setCardBackgroundColor(getResources().getColor(R.color.white));
            listOfMeaningText.get(cc).setTextColor(getResources().getColor(R.color.sky_blue));

            listOfWordCard.remove(cc);
            listOfWordText.remove(cc);
            listWordPosition.remove(cc);

            listOfMeaningCard.remove(cc);
            listOfMeaningText.remove(cc);
            listMeaningPosition.remove(cc);

            if (!msg.equalsIgnoreCase("bothPairRemove") && !removePairMethod) {
                manageCurrentAndPreviewView("different", position, cardView, textView, "firstTime");
            }

        }

    }

    private void manageCurrentAndPreviewView(String direct, int position, CardView
            cardView, TextView textView, String msg) {
        if (direct.equalsIgnoreCase("direct")) {
            tempCrdView = cardView;
            tempTxtView = textView;
            setColorOnView(cardView, textView, position, msg);
        } else if (direct.equalsIgnoreCase("notDirect")) {

            tempCrdView.setCardBackgroundColor(getResources().getColor(R.color.white));
            tempTxtView.setTextColor(getResources().getColor(R.color.sky_blue));

            tempCrdView = cardView;
            tempTxtView = textView;
            setColorOnView(cardView, textView, position, msg);
        } else if (direct.equalsIgnoreCase("different")) {

            if (!managePairs) {

                tempCrdView.setCardBackgroundColor(getResources().getColor(R.color.white));
                tempTxtView.setTextColor(getResources().getColor(R.color.sky_blue));
                tempCrdView = cardView;
                tempTxtView = textView;

                if (listOfMeaningCard.size() != 0) {
                    listOfMeaningCard.remove(listOfMeaningCard.size() - 1);
                    listOfMeaningText.remove(listOfMeaningText.size() - 1);
                    listMeaningPosition.remove(listMeaningPosition.size() - 1);

                }


            } else {
                tempCrdView = cardView;
                tempTxtView = textView;


            }
            listOfMeaningCard.add(cardView);
            listOfMeaningText.add(textView);
            listMeaningPosition.add(position);


            Log.d("addd", String.valueOf(listMeaningPosition));
            Log.e("removePairPos", listWordPosition.toString() + listMeaningPosition.toString());
            setColorOnView(cardView, textView, position, "onPairColor");
        }
    }

    private void setColorOnView(CardView cardView, TextView textView, int position, String msg) {

        if (msg.equalsIgnoreCase("normal")) {

            sameColor = colorArray[position];

            cardView.setCardBackgroundColor(getResources().getColor(colorArray[position]));
            textView.setTextColor(getResources().getColor(R.color.white));
            tempCardWordBgColor = cardView.getCardBackgroundColor();
            managePairs = true;

            if (!manageSetColorMethodData && listOfWordCard.size() != 0) {
                listOfWordCard.remove(listOfWordCard.size() - 1);
                listOfWordText.remove(listOfWordText.size() - 1);
                listWordPosition.remove(listWordPosition.size() - 1);
                Log.e("listWordPosRemove", listWordPosition.toString());

                tempCrdView = cardView;
                tempTxtView = textView;
                tempPos = position;
                listOfWordCard.add(tempCrdView);
                listOfWordText.add(tempTxtView);
                listWordPosition.add(tempPos);
                Log.e("AfterRemoveAdd", listWordPosition.toString());

            } else {

                tempPos = position;
                tempCrdView = cardView;
                tempTxtView = textView;
                listOfWordCard.add(tempCrdView);
                listOfWordText.add(tempTxtView);
                listWordPosition.add(tempPos);
                Log.e("FirstWordAdd", listWordPosition.toString());
                manageSetColorMethodData = false;

            }


        } else if (msg.equalsIgnoreCase("firstTime")) {

            managePairs = false;
            if (manageSetColorMethodData && listOfMeaningCard.size() != 0) {


                listOfMeaningCard.remove(listOfMeaningCard.size() - 1);
                listOfMeaningText.remove(listOfMeaningText.size() - 1);
                listMeaningPosition.remove(listMeaningPosition.size() - 1);
                Log.e("listMeaningPosRemove", listMeaningPosition.toString());

                tempCrdView = cardView;
                tempTxtView = textView;
                tempPos = position;
                manageSetColorMethodData = true;
                listOfMeaningCard.add(tempCrdView);
                listOfMeaningText.add(tempTxtView);
                listMeaningPosition.add(tempPos);
                Log.e("AfterRemoveAdd", listMeaningPosition.toString());

            } else {

                listOfMeaningCard.add(cardView);
                listOfMeaningText.add(textView);
                listMeaningPosition.add(position);
                Log.e("FirstMeaningAdd", listMeaningPosition.toString());
                tempCrdView = cardView;
                tempTxtView = textView;
                tempPos = position;
                manageSetColorMethodData = true;

            }

            cardView.setCardBackgroundColor(getResources().getColor(sameColor));
            textView.setTextColor(getResources().getColor(R.color.white));

            tempCardMeaningBgColor = cardView.getCardBackgroundColor();
        } else if (msg.equalsIgnoreCase("onPairColor")) {
            managePairs = false;

            cardView.setCardBackgroundColor(getResources().getColor(sameColor));
            textView.setTextColor(getResources().getColor(R.color.white));
            tempCardMeaningBgColor = cardView.getCardBackgroundColor();
        }
    }

    private void setListner() {
        imgBack.setOnClickListener(this);
        relNext.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.relNext:

                animationClass.moveTranslateAnimation(0, -width, 0, 0,
                        120, true, true,false,false, crdMatchThePair,false, new AnimationEndListener() {
                            @Override
                            public void onAnimationEnd() {
                                animationClass.moveTranslateAnimation(
                                        -width, 0, 0, 0, 120, true, true,
                                        false,false, crdMatchThePair,false, new AnimationEndListener() {
                                            @Override
                                            public void onAnimationEnd() {

                                            }
                                        }
                                );
                            }
                        });
                break;


        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
