package com.ieltsvocabulary.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ieltsvocabulary.Adapter.MeaningMatchAdapter;

import com.ieltsvocabulary.Custom.AnimationClass;
import com.ieltsvocabulary.Interface.AnimationEndListener;
import com.ieltsvocabulary.Model.MatchTheMeaningModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

public class MatchTheMeaning extends AppCompatActivity implements View.OnClickListener {

    RelativeLayout relTitle, relNext;
    TextView txtMainTitle, txtMeaning, txtMeaningAns;
    ImageView imgBack;
    CardView crdMatchTheMeaning;
    LinearLayout lnrTitle;
    RecyclerView rcyMatchTheMeaning;

    ArrayList<MatchTheMeaningModel> listData;
    AnimationClass animationClass;
    DisplayMetrics displayMetrics;
    int width;
    MeaningMatchAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_the_meaning);

        findViewByid();
        init();
        setListner();


    }

    private void findViewByid() {
        relTitle = findViewById(R.id.relTitle);
        txtMainTitle = findViewById(R.id.txtMainTitle);
        imgBack = findViewById(R.id.imgBack);
        crdMatchTheMeaning = findViewById(R.id.crdMatchTheMeaning);
        lnrTitle = findViewById(R.id.lnrTitle);
        txtMeaning = findViewById(R.id.txtMeaning);
        txtMeaningAns = findViewById(R.id.txtMeaningAns);

        rcyMatchTheMeaning = findViewById(R.id.rcyMatchTheMeaning);
        relNext = findViewById(R.id.relNext);

    }

    private void init() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rcyMatchTheMeaning.setLayoutManager(linearLayoutManager);

        animationClass  = new AnimationClass(this);
        displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        width = displayMetrics.widthPixels;
        listData = new ArrayList<>();
        listData.add(new MatchTheMeaningModel("a", R.string.to_benefit_from,false));
        listData.add(new MatchTheMeaningModel("b", R.string.to_enrol_on,false));
        listData.add(new MatchTheMeaningModel("c", R.string.to_study,false));
        listData.add(new MatchTheMeaningModel("d", R.string.to_follow,false));

        adapter = new MeaningMatchAdapter(this, listData);
        rcyMatchTheMeaning.setAdapter(adapter);
       /* adapter = new MeaningMatchAdapter(MatchTheMeaning.this, new OptionAnimationMthMeaning() {
            @Override
            public void lefttoright(CardView card,int position, TextView cardView , TextView view) {
                startAnimations(card,position,cardView,view);

            }

            @Override
            public void righttoleft(CardView card,int position, TextView cardView, TextView view) {
                startReverseAnimations(card,position,cardView,view);
            }
        });*/

    }

    private void setListner() {

        imgBack.setOnClickListener(this);
        relNext.setOnClickListener(this);

    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                onBackPressed();
                break;
            case R.id.relNext:
                /*animationClass.objectAnimation(crdMatchTheMeaning,"translationX",0,-width,
                        1202,false,true,false);*/

                animationClass.moveTranslateAnimation(0, -width, 0, 0,
                        120, true, true,false,false, crdMatchTheMeaning,false, new AnimationEndListener() {
                            @Override
                            public void onAnimationEnd() {
                                animationClass.moveTranslateAnimation(
                                        width, 0, 0, 0, 120, true, true,false,
                                        false, crdMatchTheMeaning,false,new AnimationEndListener() {
                                            @Override
                                            public void onAnimationEnd() {

                                            }
                                        }
                                );
                            }
                        });

             /*   animationClass.moveTranslateAnimation(0, -width, 0, 0,
                        120, true, true, crdMatchTheMeaning);
                animationClass.startSecondAnimation(width, 0, 0, 0, 120,"no",crdMatchTheMeaning);

*/
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
