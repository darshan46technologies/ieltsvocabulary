package com.ieltsvocabulary.Adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ieltsvocabulary.Fragments.FirstFrag;
import com.ieltsvocabulary.R;

// How to used fragment newInstance method in android : -
// - https://stackoverflow.com/questions/18413309/how-to-implement-a-viewpager-with-different-fragments-layouts
public class SubjectViewPagerAdapter extends FragmentPagerAdapter {
    /*private List<androidx.fragment.app.Fragment> Fragment = new ArrayList<>(); //Fragment List
    private List<String> NamePage = new ArrayList<>();*/

    private static int NUM_ITEMS = 19;

    public SubjectViewPagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            /*case 0:
                return FirstFrag.newInstance(R.drawable.taskcardgreen, R.string.commonMesg,
                        "Education",R.drawable.education);
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return FirstFrag.newInstance(R.drawable.purpal, R.string.commonMesg
                       ,"Graphs & Tables",R.drawable.graphstables);
            case 2: // Fragment # 1 - This will show SecondFragment
                return FirstFrag.newInstance(R.drawable.blurred, R.string.commonMesg,
                       "Environment",R.drawable.environment);
            case 3: // Fragment # 1 - This will show SecondFragment
                return FirstFrag.newInstance(R.drawable.yellow, R.string.commonMesg,
                       "Science and Technology",R.drawable.science);
            case 4: // Fragment # 1 - This will show SecondFragment
                return FirstFrag.newInstance(R.drawable.darkpupl, R.string.commonMesg,
                       "Travel,Transpor and tourisum",R.drawable.ttt);
            case 5: // Fragment # 1 - This will show SecondFragment
                return FirstFrag.newInstance(R.drawable.blue, R.string.commonMesg,
                        "Health",R.drawable.health);
            case 6: // Fragment # 1 - This will show SecondFragment
                return FirstFrag.newInstance(R.drawable.lightmaroon, R.string.commonMesg,
                        "Work",R.drawable.work);
            case 7: // Fragment # 1 - This will show SecondFragment
                return FirstFrag.newInstance(R.drawable.lightblue, R.string.commonMesg
                        , "Money and Finance",R.drawable.money);
            case 8: // Fragment # 1 - This will show SecondFragment
                return FirstFrag.newInstance(R.drawable.org, R.string.commonMesg,
                        "Crime and then law",R.drawable.crimelaw);
            case 9: // Fragment # 1 - This will show SecondFragment
                return FirstFrag.newInstance(R.drawable.maroon, R.string.commonMesg,
                        "Family and Relationships",R.drawable.family);
            case 10: // Fragment # 1 - This will show SecondFragment
                return FirstFrag.newInstance(R.drawable.lightpur, R.string.commonMesg,
                        "Architecture",R.drawable.architecture);
            case 11: // Fragment # 1 - This will show SecondFragment
                return FirstFrag.newInstance(R.drawable.skyblue, R.string.commonMesg,
                        "Sport and Leisure",R.drawable.sport);
            case 12: // Fragment # 1 - This will show SecondFragment
                return FirstFrag.newInstance(R.drawable.coffee, R.string.commonMesg,
                        "Advertising",R.drawable.advertising);
            case 13: // Fragment # 1 - This will show SecondFragment
                return FirstFrag.newInstance(R.drawable.red, R.string.commonMesg,
                        "Geography",R.drawable.geography);
            case 14: // Fragment # 1 - This will show SecondFragment
                return FirstFrag.newInstance(R.drawable.lightpur, R.string.commonMesg,
                       "Celebrations",R.drawable.celebrations);
            case 15: // Fragment # 1 - This will show SecondFragment
                return FirstFrag.newInstance(R.drawable.lightcofee, R.string.commonMesg,
                        "General Academic",R.drawable.generalacedamy);
            case 16: // Fragment # 1 - This will show SecondFragment
                return FirstFrag.newInstance(R.drawable.newcolor, R.string.commonMesg
                        ,"The Media",R.drawable.themedia);
            case 17: // Fragment # 1 - This will show SecondFragment
                return FirstFrag.newInstance(R.drawable.mahendi, R.string.commonMesg,
                       "Processes",R.drawable.processes);
            case 18: // Fragment # 1 - This will show SecondFragment
                return FirstFrag.newInstance(R.drawable.lightorg, R.string.commonMesg,
                       "Popular Arts and Culture",R.drawable.popular);*/
            default:
                return null;
        }
//        return Fragment.get(position);


    }


    /*@Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return NamePage.get(position);

    }*/

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    /*public void add(Fragment fragment,String title){

        Fragment.add(fragment);
        NamePage.add(title);
    }*/


}
