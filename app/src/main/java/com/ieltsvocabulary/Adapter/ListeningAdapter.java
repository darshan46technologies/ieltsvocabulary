package com.ieltsvocabulary.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Interface.FrgPositionAnimation;
import com.ieltsvocabulary.Model.ListeningModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

public class ListeningAdapter extends RecyclerView.Adapter<ListeningAdapter.ViewHolder> {

    Context context;
    ArrayList<ListeningModel> listeningOption;
    FrgPositionAnimation frgPositionAnimation;
    String msg, optionText;
    int numCount = 0;
    boolean flag = false;
    int tempPosition = 0;


    public ListeningAdapter(Context context, ArrayList<ListeningModel> listeningOption,
                            String msg, FrgPositionAnimation frgPositionAnimation) {
        this.context = context;
        this.listeningOption = listeningOption;
        this.msg = msg;
        this.frgPositionAnimation = frgPositionAnimation;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.row_listening, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        holder.txtOption.setText(listeningOption.get(position).getOption());
        holder.txtOptionText.setText(listeningOption.get(position).getMessage());
        optionText = holder.txtOptionText.getText().toString();

        if (listeningOption.get(position).isSelected()) {
            //count for user select option
            numCount = numCount + 1;

            if (optionText.equalsIgnoreCase(msg)) {


                changeColors(holder,
                        R.drawable.layout_true_color,
                        context.getResources().getColor(R.color.white)
                        , context.getResources().getColor(R.color.white), "nextScreen");
            } else {


                changeColors(holder,
                        R.drawable.layout_false_color,
                        context.getResources().getColor(R.color.white),
                        context.getResources().getColor(R.color.white), "OpenDialog");

            }
        } else {

            changeColors(holder,
                    R.drawable.layout_white_radius,
                    context.getResources().getColor(R.color.black_new),
                    context.getResources().getColor(R.color.black_new), "nothing");


        }


        holder.lnrMain.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                if (numCount <= 2) {

                    listeningOption.get(tempPosition).setSelected(false);
                    tempPosition = position;
                    listeningOption.get(tempPosition).setSelected(true);
                    notifyDataSetChanged();
                }
            }
        });

    }

    private void changeColors(ViewHolder holder,
                              int layout_true_color,
                              int optionColor, int optionTextColor,
                              String i) {
        //only 3 time select option


        holder.lnrMain.setBackgroundResource(layout_true_color);
        holder.txtOption.setTextColor(optionColor);
        holder.txtOptionText.setTextColor(optionTextColor);

        if (i.equalsIgnoreCase("nextScreen")) {
            numCount = 5;
            frgPositionAnimation.frgposition(i, 0);
        } else if (i.equalsIgnoreCase("OpenDialog")) {
            if (numCount >= 2) {
                frgPositionAnimation.frgposition(i, 0);
            }

        }


    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return listeningOption.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout lnrMain;
        TextView txtOption, txtOptionText;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            lnrMain = itemView.findViewById(R.id.lnrMain);
            txtOption = itemView.findViewById(R.id.txtOption);
            txtOptionText = itemView.findViewById(R.id.txtOptionText);


        }
    }
}
