package com.ieltsvocabulary.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Interface.Click_Listner_Interface;
import com.ieltsvocabulary.Model.HomeDataModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MenuListAdapterNew extends RecyclerView.Adapter<MenuListAdapterNew.ViewHolder> {

    Context context;
    ArrayList<HomeDataModel> homeModels;
    int current_position = 0;
    Click_Listner_Interface click_listner_interface;

    public MenuListAdapterNew(Context context, ArrayList<HomeDataModel> homeModels, int current_menu_position, Click_Listner_Interface click_listner_interface) {
        this.context = context;
        this.homeModels = homeModels;
        this.click_listner_interface = click_listner_interface;
        this.current_position = current_menu_position;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_menu_items, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        HomeDataModel homeDataModel = homeModels.get(position);
        holder.txtName.setText(homeDataModel.getMain_title());
        holder.imgLogo.setImageResource(homeDataModel.getMain_image_header());

        if (position == current_position) {
            holder.txtName.setTextColor(context.getResources().getColor(homeDataModel.getColor_code()));
            holder.imgLogo.setColorFilter(ContextCompat.getColor(context, homeDataModel.getColor_code()), android.graphics.PorterDuff.Mode.MULTIPLY);
            holder.viewDot.setImageResource(homeDataModel.getColor_code());
        } else {
            holder.txtName.setTextColor(context.getResources().getColor(R.color.task_gray_search));
            holder.imgLogo.setColorFilter(ContextCompat.getColor(context, R.color.new_light_gray), android.graphics.PorterDuff.Mode.MULTIPLY);
            holder.viewDot.setImageResource(R.color.white);
        }

        holder.lnrItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                current_position = position;
                notifyDataSetChanged();
                click_listner_interface.setAction(position);
            }
        });
    }

   /* private int getColorCode(int current_position) {
        int color = R.color.new_gray;
        switch (current_position) {
            case 0:
                color = R.color.education;
                break;
            case 1:
                color = R.color.graphs;
                break;
            case 2:
                color = R.color.environment;
                break;
            case 3:
                color = R.color.science;
                break;
            case 4:
                color = R.color.travel;
                break;
            case 5:
                color = R.color.health;
                break;
            case 6:
                color = R.color.work;
                break;
            case 7:
                color = R.color.money;
                break;
            case 8:
                color = R.color.crime;
                break;
            case 9:
                color = R.color.family;
                break;
            case 10:
                color = R.color.architechture;
                break;
            case 11:
                color = R.color.sport;
                break;
            case 12:
                color = R.color.adverti;
                break;
            case 13:
                color = R.color.geography;
                break;
            case 14:
                color = R.color.celebration;
                break;
            case 15:
                color = R.color.general;
                break;
            case 16:
                color = R.color.themedia;
                break;
            case 17:
                color = R.color.process;
                break;
            case 18:
                color = R.color.popularart;
                break;
        }
        return color;
    }*/

    @Override
    public int getItemCount() {
        return homeModels.size();
    }

    public void setCurrentPos(int firstVisiblePosition) {
        current_position = firstVisiblePosition;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView viewDot;
        LinearLayout lnrItems;
        ImageView imgLogo;
        TextView txtName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            viewDot = itemView.findViewById(R.id.viewDot);
            lnrItems = itemView.findViewById(R.id.lnrItems);
            imgLogo = itemView.findViewById(R.id.imgLogo);
            txtName = itemView.findViewById(R.id.txtName);

        }
    }
}
