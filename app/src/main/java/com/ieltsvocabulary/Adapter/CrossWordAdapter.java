package com.ieltsvocabulary.Adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Interface.CrossWordInterface;
import com.ieltsvocabulary.Model.CrossWordModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

public class CrossWordAdapter extends RecyclerView.Adapter<CrossWordAdapter.ViewHolder> {

    Context context;
    char[] strArray, wordArray;
    CrossWordInterface crossWordInterface;
    ArrayList<CrossWordModel> storeAndGetDataInModel;

    public CrossWordAdapter(Context context, char[] strArray, char[] wordArray,
                            ArrayList<CrossWordModel> storeAndGetDataInModel,CrossWordInterface crossWordInterface) {
        this.context = context;
        this.strArray = strArray;
        this.wordArray = wordArray;
        this.storeAndGetDataInModel = storeAndGetDataInModel;
        this.crossWordInterface = crossWordInterface;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.row_crossword_box, parent, false);
        return new ViewHolder(view);
    }


    @Override

    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        setDaynamicView(holder, position);


        holder.edtAddChar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    Log.e("Count", String.valueOf(count));
                    Log.e("start", String.valueOf(start));
                    Log.e("before", String.valueOf(before));
                    crossWordInterface.charWithPosition(s.charAt(0), position);
                }else{
                    crossWordInterface.charWithPosition('-', position);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

//        Log.e("DAtata", String.valueOf(storeCharWithNum));


    }

    private void setDaynamicView(ViewHolder holder, int position) {

        if (strArray[position] == '@' && wordArray[position] == '@') {
            holder.itemView.setVisibility(View.GONE);
//        } else if (strArray[position] == '@' || (strArray[position] != '@' && strArray[position] != '-')) {

        } else if (strArray[position] != '@' && strArray[position] != ' ') {

            if (wordArray[position] != '@') {

                viewVisibility(holder, position, "txtNumVisible", "edtGone",
                        "txtTextVisible");
            } else {
                viewVisibility(holder, position, "txtNumGone", "edtGone",
                        "txtTextVisible");
            }

        } else if (strArray[position] == ' ') {
            if (wordArray[position] != '@') {
                viewVisibility(holder, position, "txtNumVisible", "edtVisible",
                        "txtTextGone");
            } else {
                viewVisibility(holder, position, "txtNumGone", "edtVisible",
                        "txtTextGone");

            }

        }
    }

    private void viewVisibility(ViewHolder holder, int position,
                                String txtNum, String edt, String txtText) {

        if (txtNum.equalsIgnoreCase("txtNumVisible")) {
            holder.txtNumber.setVisibility(View.VISIBLE);
            holder.txtNumber.setText(String.valueOf(wordArray[position]));
        } else {
            holder.txtNumber.setVisibility(View.GONE);
        }
        if (edt.equalsIgnoreCase("edtVisible")) {
            holder.edtAddChar.setVisibility(View.VISIBLE);
        } else {
            holder.edtAddChar.setVisibility(View.GONE);
        }
        if (txtText.equalsIgnoreCase("txtTextVisible")) {
            holder.txtText.setVisibility(View.VISIBLE);
            holder.txtText.setText(String.valueOf(strArray[position]));
            storeAndGetDataInModel.add(new CrossWordModel(position, strArray[position]));
        } else {
            holder.txtText.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return strArray.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtNumber, txtText;
        EditText edtAddChar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtNumber = itemView.findViewById(R.id.txtNumber);
            edtAddChar = itemView.findViewById(R.id.edtAddChar);
            txtText = itemView.findViewById(R.id.txtText);

        }
    }
}
