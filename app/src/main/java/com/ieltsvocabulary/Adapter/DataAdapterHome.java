package com.ieltsvocabulary.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Activity.CrossWordScreen;
import com.ieltsvocabulary.Activity.GapFillScreen;
import com.ieltsvocabulary.Activity.ListeningScreen;
import com.ieltsvocabulary.Activity.MatchTheMeaning;
import com.ieltsvocabulary.Activity.MatchThePairsScreen;
import com.ieltsvocabulary.Activity.MissingLettters;
import com.ieltsvocabulary.Activity.PronunciationScreen;
import com.ieltsvocabulary.Activity.TranslationScreen;
import com.ieltsvocabulary.Activity.TrueFalseScreen;
import com.ieltsvocabulary.Activity.WordType;
import com.ieltsvocabulary.Interface.AdapterItemClickCallback;
import com.ieltsvocabulary.Model.HomeDataModel;
import com.ieltsvocabulary.Model.WordDataModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class DataAdapterHome extends RecyclerView.Adapter<DataAdapterHome.ViewHolder> implements View.OnClickListener {

    Context context;
    ArrayList<HomeDataModel> homeDataModels;
    ArrayList<WordDataModel> listWordData;
    ViewHolder holder;
    AdapterItemClickCallback adapterItemClickCallback;
    int check_count = 0;
    DataAdapter dataAdapter;

    public DataAdapterHome(Context context, ArrayList<HomeDataModel> homeDataModels, AdapterItemClickCallback adapterItemClickCallback) {
        this.context = context;
        this.homeDataModels = homeDataModels;
        this.adapterItemClickCallback = adapterItemClickCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.home_data_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        this.holder = holder;
        listWordData = new ArrayList<>();
//        holder.common_message.setMovementMethod(new ScrollingMovementMethod());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            holder.header_background.setClipToOutline(true);
        }

        HomeDataModel homeDataModel = homeDataModels.get(position);
        holder.header_background.setImageDrawable(context.getResources().getDrawable(homeDataModel.getBackground()));
        holder.header_image.setImageDrawable(context.getResources().getDrawable(homeDataModel.getMain_image_header()));
        holder.header_title.setText(homeDataModel.getMain_title());
//        holder.common_message.setText(homeDataModel.getCommonMesg());
        holder.checkbox_lay.setLayoutManager(new LinearLayoutManager(context));


        listWordData.add(new WordDataModel(R.string.to_enrol_on));
        listWordData.add(new WordDataModel(R.string.to_benefit_from));
        listWordData.add(new WordDataModel(R.string.to_study));
        listWordData.add(new WordDataModel(R.string.to_follow));

        check_count = listWordData.size();

        setValueInTextView(holder, check_count, listWordData.size());

        holder.checkAllItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataAdapter.setAllChecked(holder.checkAllItem.isChecked());
            }
        });
        dataAdapter = new DataAdapter(context, listWordData, new AdapterItemClickCallback() {
            @Override
            public void response(Object o, int type) {
                switch (type) {
                    case 0:
                        check_count++;
                        setValueInTextView(holder, check_count, listWordData.size());
                        break;
                    case 1:
                        check_count--;
                        setValueInTextView(holder, check_count, listWordData.size());
                        break;
                    case 2:
                        check_count = listWordData.size();
                        setValueInTextView(holder, check_count, listWordData.size());
                        break;
                    case 3:
                        check_count = 0;
                        setValueInTextView(holder, check_count, listWordData.size());
                        break;
                }
            }
        });
        holder.checkbox_lay.setAdapter(dataAdapter);

        holder.start_btn.setCardBackgroundColor(context.getResources().getColor(homeDataModel.getColor_code()));
        holder.play_btn_nxt_cir.setImageResource(homeDataModel.getColor_code());
        holder.play_btn_graph_cir.setImageResource(homeDataModel.getColor_code());
        holder.back_cir.setImageResource(homeDataModel.getColor_code());

        holder.crdDictionary.setOnClickListener(this);
        holder.crdPronunciation.setOnClickListener(this);
        holder.crdListning.setOnClickListener(this);
        holder.crdTrueFalse.setOnClickListener(this);
        holder.crdCrossword.setOnClickListener(this);
        holder.crdWordType.setOnClickListener(this);
        holder.crdSpelling.setOnClickListener(this);
        holder.crdMatching.setOnClickListener(this);
        holder.crdMatchPair.setOnClickListener(this);
        holder.crdGapFill.setOnClickListener(this);

        holder.header_background.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.checkbox_lay.getVisibility() == View.VISIBLE) {
                    holder.header_background.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    int max_width = context.getResources().getDimensionPixelSize(R.dimen._250sdp);
                    holder.header_title.setMaxWidth(max_width);
                    holder.motionLayout.setTransition(R.id.end, R.id.start);
                    holder.motionLayout.setTransitionDuration(500);
                    holder.motionLayout.transitionToEnd();
                    adapterItemClickCallback.response("header_background", 0);
                }
            }
        });
        holder.play_btn_nxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.header_background.setScaleType(ImageView.ScaleType.CENTER_CROP);
                int max_width = context.getResources().getDimensionPixelSize(R.dimen._160sdp);
                holder.header_title.setMaxWidth(max_width);
                holder.motionLayout.setTransition(R.id.start, R.id.end);
                holder.motionLayout.setTransitionDuration(500);
                holder.motionLayout.transitionToEnd();
            }
        });
        holder.start_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.motionLayout.setTransition(R.id.end, R.id.home_sub_item_scene);
                holder.motionLayout.setTransitionDuration(500);
                holder.motionLayout.transitionToEnd();
            }
        });
        holder.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.motionLayout.setTransition(R.id.home_sub_item_scene, R.id.end);
                holder.motionLayout.setTransitionDuration(500);
                holder.motionLayout.transitionToEnd();
            }
        });
    }

    private void setValueInTextView(ViewHolder holder, int check_count, int size) {


        holder.txtCount.setText(check_count + "/" + size);
    }

    @Override
    public int getItemCount() {
        return homeDataModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.crdDictionary:
                context.startActivity(new Intent(context, TranslationScreen.class));
                break;
            case R.id.crdPronunciation:
                context.startActivity(new Intent(context, PronunciationScreen.class));
                break;
            case R.id.crdListning:
                context.startActivity(new Intent(context, ListeningScreen.class));
                break;
            case R.id.crdTrueFalse:
                context.startActivity(new Intent(context, TrueFalseScreen.class));
                break;
            case R.id.crdCrossword:
                context.startActivity(new Intent(context, CrossWordScreen.class));
                break;
            case R.id.crdWordType:
                context.startActivity(new Intent(context, WordType.class));
                break;
            case R.id.crdSpelling:
                context.startActivity(new Intent(context, MissingLettters.class));
                break;
            case R.id.crdMatching:
                context.startActivity(new Intent(context, MatchTheMeaning.class));
                break;
            case R.id.crdMatchPair:
                context.startActivity(new Intent(context, MatchThePairsScreen.class));
                break;
            case R.id.crdGapFill:
                context.startActivity(new Intent(context, GapFillScreen.class));
                break;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView header_background;
        ImageView header_image;
        RelativeLayout play_btn_nxt, play_btn_graph, back;
        CircleImageView play_btn_nxt_cir, play_btn_graph_cir, back_cir;
        TextView header_title, txtCount;
//        common_message
        CheckBox checkAllItem;
        RecyclerView checkbox_lay;
        MotionLayout motionLayout;
        CardView start_btn;

        CardView crdDictionary, crdPronunciation, crdListning, crdTrueFalse, crdCrossword,
                crdWordType, crdSpelling, crdMatching, crdMatchPair, crdGapFill, crdStart;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            header_background = itemView.findViewById(R.id.card);
            header_image = itemView.findViewById(R.id.background);
            header_title = itemView.findViewById(R.id.txttitle);
//            common_message = itemView.findViewById(R.id.common_message);
            checkbox_lay = itemView.findViewById(R.id.checkbox_lay);
            motionLayout = itemView.findViewById(R.id.motion_layout);
            back = itemView.findViewById(R.id.back);
            play_btn_nxt = itemView.findViewById(R.id.play_btn_nxt);
            play_btn_graph = itemView.findViewById(R.id.play_btn_graph);
            start_btn = itemView.findViewById(R.id.start_btn);
            play_btn_nxt_cir = itemView.findViewById(R.id.play_btn_nxt_cir);
            play_btn_graph_cir = itemView.findViewById(R.id.play_btn_graph_cir);
            back_cir = itemView.findViewById(R.id.back_cir);
            txtCount = itemView.findViewById(R.id.txtCount);
            checkAllItem = itemView.findViewById(R.id.checkAllItem);
            crdDictionary = itemView.findViewById(R.id.crdDictionary);
            crdPronunciation = itemView.findViewById(R.id.crdPronunciation);
            crdListning = itemView.findViewById(R.id.crdListning);
            crdTrueFalse = itemView.findViewById(R.id.crdTrueFalse);
            crdCrossword = itemView.findViewById(R.id.crdCrossword);
            crdWordType = itemView.findViewById(R.id.crdWordType);
            crdSpelling = itemView.findViewById(R.id.crdSpelling);
            crdMatching = itemView.findViewById(R.id.crdMatching);
            crdMatchPair = itemView.findViewById(R.id.crdMatchPair);
            crdGapFill = itemView.findViewById(R.id.crdGapFill);
            crdStart = itemView.findViewById(R.id.crdStart);
        }
    }
}
