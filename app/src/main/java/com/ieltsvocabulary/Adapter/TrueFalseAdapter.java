package com.ieltsvocabulary.Adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.media.Image;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.R;

public class TrueFalseAdapter extends RecyclerView.Adapter<TrueFalseAdapter.ViewHolder> {

    Context context;
    int flag = 0;

    public TrueFalseAdapter(Context context) {
        this.context = context;
//        this.frgInRcyPosition = frgInRcyPosition;
//        this.pronunciationAdapter = pronunciationAdapter;

    }

    public TrueFalseAdapter() {
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.row_true_false, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {


        holder.rightcrd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (holder.lnrwrongdata.getVisibility() == View.VISIBLE){
                 holder.lnrwrongdata.setVisibility(View.GONE);
                 holder.lnrenterdata.setVisibility(View.VISIBLE);
                    cardAnimation(holder, "true");

                }else{
                    cardAnimation(holder, "true");
                }



            }
        });

        holder.falsecrd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.lnrwrongdata.getVisibility() == View.VISIBLE){

                    holder.lnrwrongdata.setVisibility(View.GONE);
                    holder.lnrenterdata.setVisibility(View.VISIBLE);
                    cardAnimation(holder, "false");

                }else{
                    cardAnimation(holder, "false");
                }




            }
        });


    }

    private void cardAnimation(final ViewHolder holder, final String msg) {

        final ObjectAnimator oa1 = ObjectAnimator.ofFloat(holder.lnrenterdata, "scaleX", 1f, 0.1f);
//        final ObjectAnimator oa2 = ObjectAnimator.ofFloat(holder.lnrenterdata, "scaleY", 0.2f, 1f);
        final ObjectAnimator oa3 = ObjectAnimator.ofFloat(holder.lnrwrongdata, "scaleX", 0.1f, 1f);
//        final ObjectAnimator oa4 = ObjectAnimator.ofFloat(holder.lnrwrongdata, "scaleY", 0.2f, 1f);
        oa1.setInterpolator(new DecelerateInterpolator());
        oa1.setDuration(400);
//        oa2.setDuration(400);
        oa3.setDuration(400);
//        oa4.setDuration(400);
//        oa2.setInterpolator(new AccelerateDecelerateInterpolator());
        oa3.setInterpolator(new AccelerateDecelerateInterpolator());
//        oa4.setInterpolator(new AccelerateDecelerateInterpolator());
        oa1.addListener(new AnimatorListenerAdapter() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                holder.lnrenterdata.setVisibility(View.GONE);
                holder.lnrwrongdata.setVisibility(View.VISIBLE);


                if (msg.equalsIgnoreCase("true")) {
                    holder.lnrwrongdata.setBackgroundColor(context.getColor(R.color.bluer_green));
                    holder.changeimg.setImageResource(R.drawable.trsmiley);
                    holder.txtchange.setText("Well done! That’s correct");

                } else {
                    holder.lnrwrongdata.setBackgroundColor(context.getColor(R.color.bluer_red));
                    holder.changeimg.setImageResource(R.drawable.whitewarning);
                    holder.txtchange.setText("Opps! That’s incorrect. Try again.");
                }


//                oa2.start();
                oa3.start();
//                oa4.start();
//                oa2.setDuration(700);
            }
        });
        oa1.start();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return 50;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        EditText edtfilldata;
        CardView rightcrd, falsecrd, crdinformation, lnrwrongdata;
        LinearLayout lnrenterdata;
        ImageView changeimg;
        TextView txtchange;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            edtfilldata = itemView.findViewById(R.id.edtfilldata);
            rightcrd = itemView.findViewById(R.id.rightcrd);
            falsecrd = itemView.findViewById(R.id.falsecrd);
            crdinformation = itemView.findViewById(R.id.crdinformation);
            lnrenterdata = itemView.findViewById(R.id.lnrenterdata);
            lnrwrongdata = itemView.findViewById(R.id.lnrwrongdata);
            changeimg = itemView.findViewById(R.id.changeimg);
            txtchange = itemView.findViewById(R.id.txtchange);


        }
    }
}