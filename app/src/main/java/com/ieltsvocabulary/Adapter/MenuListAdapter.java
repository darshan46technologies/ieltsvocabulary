package com.ieltsvocabulary.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Interface.MenuItemsInterface;
import com.ieltsvocabulary.Model.MenuItemModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MenuListAdapter extends RecyclerView.Adapter<MenuListAdapter.ViewHolder> {

    Context context;
    ArrayList<MenuItemModel> listMenuItems;
    MenuItemsInterface menuItemsInterface;

    public MenuListAdapter(Context context, ArrayList<MenuItemModel> listMenuItems, MenuItemsInterface menuItemsInterface) {
        this.context = context;
        this.listMenuItems = listMenuItems;
        this.menuItemsInterface = menuItemsInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.row_menu_items, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        holder.txtName.setText(listMenuItems.get(position).getMessage());
        holder.imgLogo.setImageResource(listMenuItems.get(position).getIcon());

        holder.lnrItems.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menuItemsInterface.items(position, holder.viewDot, holder.imgLogo, holder.txtName);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listMenuItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView viewDot;
        LinearLayout lnrItems;
        ImageView imgLogo;
        TextView txtName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            viewDot = itemView.findViewById(R.id.viewDot);
            lnrItems = itemView.findViewById(R.id.lnrItems);
            imgLogo = itemView.findViewById(R.id.imgLogo);
            txtName = itemView.findViewById(R.id.txtName);

        }
    }
}
