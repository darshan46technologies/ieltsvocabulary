package com.ieltsvocabulary.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Interface.FrgPositionAnimation;
import com.ieltsvocabulary.Interface.MatchThePairsInterface;
import com.ieltsvocabulary.Model.MatchTheMeaningPairModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

public class MatchTheMeaningPairAdapter extends RecyclerView.Adapter<MatchTheMeaningPairAdapter.MatchViewHolder> {

    Context context;
    ArrayList<MatchTheMeaningPairModel> listMeaningData;
    MatchThePairsInterface matchThePairsInterface;
    FrgPositionAnimation frgPositionAnimation;

    int tempPos=0,temPosTwo,currentPos;
    boolean flag = false;

    public MatchTheMeaningPairAdapter(Context context,
                                      ArrayList<MatchTheMeaningPairModel> listMeaningData,
                                      MatchThePairsInterface matchThePairsInterface,
                                      FrgPositionAnimation frgPositionAnimation) {
        this.context = context;
        this.listMeaningData = listMeaningData;
        this.matchThePairsInterface = matchThePairsInterface;
        this.frgPositionAnimation = frgPositionAnimation;
    }

    @NonNull
    @Override
    public MatchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.row_match_the_meaning_pairs_screen, parent, false);
        return new MatchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MatchViewHolder holder, final int position) {


/*        if (listMeaningData.get(position).isSelected()){
//            matchThePairsInterface.fillColorInView(position,holder.crdMeaning,holder.txtMeaning);
        }else{
//            holder.crdMeaning.setCardBackgroundColor(context.getResources().getColor(R.color.white));
//            holder.txtMeaning.setTextColor(context.getResources().getColor(R.color.sky_blue));
//            holder.crdMeaning.setCardBackgroundColor(context.getResources().getColor(R.color.white));
//            holder.txtMeaning.setTextColor(context.getResources().getColor(R.color.sky_blue));


        }*/
        frgPositionAnimation.frgposition("Meaning",0);
        holder.txtMeaning.setText(listMeaningData.get(position).getMsg());
        holder.crdMeaning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                matchThePairsInterface.fillColorInView(position,holder.crdMeaning,holder.txtMeaning);
               /* notifyDataSetChanged();
                listMeaningData.get(tempPos).setSelected(false);
                tempPos = position;
                listMeaningData.get(tempPos).setSelected(true);*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return listMeaningData.size();
    }

    public class MatchViewHolder extends RecyclerView.ViewHolder {

        CardView crdMeaning;
        TextView txtMeaning;

        public MatchViewHolder(@NonNull View itemView) {
            super(itemView);

            crdMeaning = itemView.findViewById(R.id.crdMeaning);
            txtMeaning = itemView.findViewById(R.id.txtMeaning);

        }
    }

    public void changeColor(boolean flag){



    }

}
