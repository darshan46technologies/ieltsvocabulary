package com.ieltsvocabulary.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Model.MatchTheMeaningModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

public class MeaningMatchAdapter extends RecyclerView.Adapter<MeaningMatchAdapter.ViewHolder> {

    private Context context;
    private ArrayList<MatchTheMeaningModel> listData;
    ViewHolder viewHolder;
    private int tempPostion = -1, tempTwo = -1, a = 0;
//    int a = 0, b = 0, c = 0, d = 0;


    /*public MeaningMatchAdapter(Context context, OptionAnimationMthMeaning optionAnimationMthMeaning) {
        this.context = context;
        this.optionAnimationMthMeaning = optionAnimationMthMeaning;

    }*/
    public MeaningMatchAdapter(Context context, ArrayList<MatchTheMeaningModel> listData) {
        this.context = context;
        this.listData = listData;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.row_meaning_matching, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        holder.txtOption.setText(listData.get(position).getOption());
        holder.txtOptionText.setText(listData.get(position).getOptionText());

        if (listData.get(position).isSelected()) {
//            startAnimations(holder.crdOption, tempPostion, holder.txtLayer, holder.txtOptionText);
            holder.motion_layout.setTransition(R.id.startViewInMatchMeaning, R.id.endViewInMatchMeaning);
            holder.motion_layout.setTransitionDuration(500);
            holder.motion_layout.transitionToEnd();
        } else if ((tempPostion != -1) && (tempTwo == position && tempTwo != -1)) {
//            startReverseAnimations(holder.crdOption, tempTwo, holder.txtLayer, holder.txtOptionText);
            holder.motion_layout.setTransition(R.id.endViewInMatchMeaning, R.id.startViewInMatchMeaning);
            holder.motion_layout.setTransitionDuration(500);
            holder.motion_layout.transitionToEnd();
        }

        holder.txtOptionText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(context, "kp", Toast.LENGTH_SHORT).show();

                if (tempPostion == -1) {
                    tempPostion = position;
                    listData.get(tempPostion).setSelected(true);
                } else {
                    tempTwo = tempPostion;
                    listData.get(tempPostion).setSelected(false);
                    tempPostion = position;
                    listData.get(tempPostion).setSelected(true);

                }
                notifyDataSetChanged();

            }

        });
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        CardView crdOption;
        MotionLayout motion_layout;
        TextView txtLayer, txtOption, txtOptionText;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            crdOption = itemView.findViewById(R.id.crdOption);
            motion_layout = itemView.findViewById(R.id.motion_layout);
            txtLayer = itemView.findViewById(R.id.txtLayer);
            txtOption = itemView.findViewById(R.id.txtOption);
            txtOptionText = itemView.findViewById(R.id.txtOptionText);
        }
    }
}


//This Code is select at the time one item same as radiou button
//            startAnimations(holder.crdOption,tempPostion,holder.txtLayer,holder.txtOptionText);

        /*holder.crda.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                a = 1;

                if (a == 1 && b == 0 && c == 0 && d == 0) {
                    optionAnimationMthMeaning.lefttoright(holder.crda,position, holder.latext, holder.laatext);

                } else {
                    if (b == 0 && c == 0) {
                        if (d == 0) {
                            optionAnimationMthMeaning.lefttoright(holder.crda,position, holder.latext, holder.laatext);
                        } else {

                            optionAnimationMthMeaning.righttoleft(holder.crdd,position, holder.ldtext, holder.lddtext);
                            optionAnimationMthMeaning.lefttoright(holder.crda,position, holder.latext, holder.laatext);
                            d = 0;
                            //method crdb

                        }
                    } else {

                        if (b == 1) {
                            optionAnimationMthMeaning.righttoleft(holder.crdb,position, holder.lbtext, holder.lbbtext);
                            optionAnimationMthMeaning.lefttoright(holder.crda,position,holder.latext, holder.laatext);
                            b = 0;
                            //method crdb

                        }
                        if (c == 1) {
                            //method crdc
                            optionAnimationMthMeaning.righttoleft(holder.crdc,position, holder.lctext, holder.lcctext);
                            optionAnimationMthMeaning.lefttoright(holder.crda,position,holder.latext, holder.laatext);
                            c = 0;
                        }
                    }
                }


            }
        });
        holder.crdb.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {

                b = 1;

                if (b == 1 && a == 0 && c == 0 && d == 0) {
                    optionAnimationMthMeaning.lefttoright(holder.crdb,position,holder.lbtext, holder.lbbtext);
                } else {
                    if (a == 0 && c == 0) {
                        if (d == 0) {
                            optionAnimationMthMeaning.lefttoright(holder.crdb,position, holder.lbtext, holder.lbbtext);
                        } else {


                            optionAnimationMthMeaning.righttoleft(holder.crdd,position,holder.ldtext, holder.lddtext);
                            optionAnimationMthMeaning.lefttoright(holder.crdb,position, holder.lbtext, holder.lbbtext);
                            d = 0;
                            //method crdb


                        }
                    } else {

                        if (a == 1) {
                            //method crdb
                            optionAnimationMthMeaning.righttoleft(holder.crda,position, holder.latext, holder.laatext);
                            optionAnimationMthMeaning.lefttoright(holder.crdb,position, holder.lbtext, holder.lbbtext);
                            a = 0;

                        }

                        if (c == 1) {
                            //method crdc
                            optionAnimationMthMeaning.righttoleft(holder.crdc,position, holder.lctext, holder.lcctext);
                            optionAnimationMthMeaning.lefttoright(holder.crdb,position, holder.lbtext, holder.lbbtext);
                            c = 0;
                        }


                    }
                }
            }
        });
        holder.crdc.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {

                c = 1;

                if (c == 1 && a == 0 && b == 0 && d == 0) {
                    optionAnimationMthMeaning.lefttoright(holder.crdc,position, holder.lctext, holder.lcctext);
                } else {
                    if (a == 0 && b == 0) {
                        if (d == 0) {
                            optionAnimationMthMeaning.lefttoright(holder.crdc,position, holder.lctext, holder.lcctext);
                        } else {
                            optionAnimationMthMeaning.lefttoright(holder.crdc,position, holder.lctext, holder.lcctext);
                            optionAnimationMthMeaning.righttoleft(holder.crdd,position, holder.ldtext, holder.lddtext);
                            d = 0;
                            //method crdb


                        }
                    } else {

                        if (a == 1) {
                            //method crdb
                            optionAnimationMthMeaning.righttoleft(holder.crda,position, holder.latext, holder.laatext);
                            optionAnimationMthMeaning.lefttoright(holder.crdc,position, holder.lctext, holder.lcctext);
                            a = 0;
                        }
                        if (b == 1) {
                            //method crdc
                            optionAnimationMthMeaning.righttoleft(holder.crdb,position, holder.lbtext, holder.lbbtext);
                            optionAnimationMthMeaning.lefttoright(holder.crdc,position, holder.lctext, holder.lcctext);
                            b = 0;
                        }

                    }
                }


            }
        });
        holder.crdd.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {

                d = 1;


                if (d == 1 && a == 0 && b == 0 && c == 0) {
                    optionAnimationMthMeaning.lefttoright(holder.crdd,position, holder.ldtext, holder.lddtext);
                } else {
                    if (a == 0 && b == 0) {
                        if (c == 0) {
                            optionAnimationMthMeaning.lefttoright(holder.crdd,position,holder.ldtext, holder.lddtext);
                        } else {

                            //method crdb
                            optionAnimationMthMeaning.righttoleft(holder.crdc,position, holder.lctext, holder.lcctext);
                            optionAnimationMthMeaning.lefttoright(holder.crdd,position, holder.ldtext, holder.lddtext);
                            c = 0;

                        }
                    } else {

                        if (a == 1) {
                            //method crdb
                            optionAnimationMthMeaning.righttoleft(holder.crda,position, holder.latext, holder.laatext);
                            optionAnimationMthMeaning.lefttoright(holder.crdd,position, holder.ldtext, holder.lddtext);
                            a = 0;

                        }
                        if (b == 1) {
                            //method crdc
                            optionAnimationMthMeaning.righttoleft(holder.crdb,position, holder.lbtext, holder.lbbtext);
                            optionAnimationMthMeaning.lefttoright(holder.crdd,position, holder.ldtext, holder.lddtext);
                            b = 0;
                        }
                    }
                }


            }
        });*/

        /*private void startAnimations(final CardView card, int position, final TextView txtOne, TextView txtTwo) {

        txtOne.setBackgroundResource(R.drawable.layout_select_color);
        TranslateAnimation animation = new TranslateAnimation(0.0f, 1000.0f, 0.0f, 0f); // new TranslateAnimation (float fromXDelta,float toXDelta, float fromYDelta, float toYDelta)
        animation.setDuration(1000); // animation duration
        animation.setFillAfter(true);
        txtOne.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                card.setClickable(false);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                card.setClickable(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        txtTwo.setTextColor(context.getResources().getColor(R.color.white));

    }*/

    /*private void startReverseAnimations(final CardView card, int tempPosition, TextView cardView, TextView view) {

        cardView.setBackgroundResource(R.drawable.layout_select_color);
        TranslateAnimation animation = new TranslateAnimation(900.0f, 0.0f, 0.0f, 0f); // new TranslateAnimation (float fromXDelta,float toXDelta, float fromYDelta, float toYDelta)
        animation.setDuration(1000); // animation duration
        animation.setFillAfter(true);

        cardView.startAnimation(animation);

        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                card.setClickable(false);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                card.setClickable(true);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


        view.setTextColor(context.getResources().getColor(R.color.black_new));


    }
*/