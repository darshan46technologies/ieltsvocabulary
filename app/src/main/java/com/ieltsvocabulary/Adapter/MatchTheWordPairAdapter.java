package com.ieltsvocabulary.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Interface.FrgPositionAnimation;
import com.ieltsvocabulary.Interface.MatchThePairsInterface;
import com.ieltsvocabulary.Model.MatchTheWordPairModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

public class MatchTheWordPairAdapter extends RecyclerView.Adapter<MatchTheWordPairAdapter.ViewHolder> {

    Context context;
    ArrayList<MatchTheWordPairModel>listWordData;
    MatchThePairsInterface matchThePairsInterface;
    FrgPositionAnimation frgPositionAnimation;
    int tempPosition = 0;

    public MatchTheWordPairAdapter(Context context,
                                   ArrayList<MatchTheWordPairModel> listWordData,
                                   MatchThePairsInterface matchThePairsInterface,
                                   FrgPositionAnimation frgPositionAnimation) {
        this.context = context;
        this.listWordData = listWordData;
        this.matchThePairsInterface = matchThePairsInterface;
        this.frgPositionAnimation = frgPositionAnimation;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.row_match_the_word_pair_screen,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.txtWordText.setText(listWordData.get(position).getMsg());
//        if (listWordData.get(position).isSelected()){
//            matchThePairsInterface.fillColorInView(position,holder.crdWord,holder.txtWordText);
//        }else{
//
//        }
        frgPositionAnimation.frgposition("Word",0);
        holder.crdWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                matchThePairsInterface.fillColorInView(position,holder.crdWord,holder.txtWordText);
//                notifyDataSetChanged();
//                listWordData.get(tempPosition).setSelected(false);
//                tempPosition = position;
//                listWordData.get(tempPosition).setSelected(true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listWordData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        CardView crdWord;
        TextView txtWordText;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            crdWord = itemView.findViewById(R.id.crdWord);
            txtWordText = itemView.findViewById(R.id.txtWordText);

        }
    }
}
