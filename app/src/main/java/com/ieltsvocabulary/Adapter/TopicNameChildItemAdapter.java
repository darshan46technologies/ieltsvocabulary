package com.ieltsvocabulary.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.R;

import java.util.ArrayList;

public class TopicNameChildItemAdapter extends RecyclerView.Adapter<TopicNameChildItemAdapter.TopicViewHolder> {

    Context context;
    ArrayList<String> childItem;
    int type;

    public TopicNameChildItemAdapter(Context context, ArrayList<String> childItem, int type) {
        this.context = context;
        this.childItem = childItem;
        this.type = type;
    }

    @NonNull
    @Override
    public TopicViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        context = parent.getContext();

        View view = LayoutInflater.from(context).inflate(R.layout.topic_item_list, parent, false);

        return new TopicViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TopicViewHolder holder, int position) {
        holder.txtdata.setText(childItem.get(position));
        if (type == 1) {
            holder.edit_img.setVisibility(View.VISIBLE);
        } else {
            holder.edit_img.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return childItem.size();
    }

    public class TopicViewHolder extends RecyclerView.ViewHolder {

        TextView txtdata;
        ImageView edit_img;

        public TopicViewHolder(@NonNull View itemView) {
            super(itemView);
            txtdata = itemView.findViewById(R.id.txtdata);
            edit_img = itemView.findViewById(R.id.edit_img);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
