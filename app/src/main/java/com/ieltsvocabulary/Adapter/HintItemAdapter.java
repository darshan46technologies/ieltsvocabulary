package com.ieltsvocabulary.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Model.HintItemModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

public class HintItemAdapter extends RecyclerView.Adapter<HintItemAdapter.ViewHolder> {

    Context context;
    ArrayList<HintItemModel> listHintItems;

    public HintItemAdapter(Context context, ArrayList<HintItemModel> listHintItems) {
        this.context = context;
        this.listHintItems = listHintItems;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.row_hint_item_list,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.txtNumOne.setText(listHintItems.get(position).getOption());
        holder.txtTextOne.setText(listHintItems.get(position).getOptionValue());
    }

    @Override
    public int getItemCount() {
        return listHintItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtNumOne,txtTextOne;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNumOne = itemView.findViewById(R.id.txtNumOne);
            txtTextOne = itemView.findViewById(R.id.txtTextOne);
        }
    }
}
