package com.ieltsvocabulary.Adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Model.PiHorizontalModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

public class TopicHorizontalAdpater extends RecyclerView.Adapter<TopicHorizontalAdpater.TopicViewHolder> {

    Context context;
    ArrayList<PiHorizontalModel> listHorizontalData;
    int tempPos = 0;

    public TopicHorizontalAdpater(Context context, ArrayList<PiHorizontalModel> listHorizontalData) {
        this.context = context;
        this.listHorizontalData = listHorizontalData;
    }

    @NonNull
    @Override
    public TopicViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.row_topic_horizontal_topic_name, parent, false);
        return new TopicViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TopicViewHolder holder, final int position) {

        Log.e("Flag", String.valueOf(listHorizontalData.get(position).isFlag()));
        Log.e("position", String.valueOf(position));
        if (listHorizontalData.get(position).isFlag()) {
            holder.lnrTopicList.setBackgroundResource(R.drawable.layout_blue_light_green_bg);
            holder.imgTopicIcon.setColorFilter(ContextCompat.getColor(context, R.color.white));
            holder.txtTopicName.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.lnrTopicList.setBackgroundResource(0);
            holder.imgTopicIcon.setColorFilter(ContextCompat.getColor(context, R.color.black_new));
            holder.txtTopicName.setTextColor(context.getResources().getColor(R.color.black_new));
        }
        holder.txtTopicName.setText(listHorizontalData.get(position).getText());
        holder.imgTopicIcon.setImageResource(listHorizontalData.get(position).getImage());
        holder.lnrTopicList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listHorizontalData.get(tempPos).setFlag(false);
                tempPos = position;
                listHorizontalData.get(tempPos).setFlag(true);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listHorizontalData.size();
    }

    public class TopicViewHolder extends RecyclerView.ViewHolder {
        LinearLayout lnrTopicList;
        ImageView imgTopicIcon;
        TextView txtTopicName;


        public TopicViewHolder(@NonNull View itemView) {
            super(itemView);
            lnrTopicList = itemView.findViewById(R.id.lnrTopicList);
            imgTopicIcon = itemView.findViewById(R.id.imgTopicIcon);
            txtTopicName = itemView.findViewById(R.id.txtTopicName);

        }
    }
}
