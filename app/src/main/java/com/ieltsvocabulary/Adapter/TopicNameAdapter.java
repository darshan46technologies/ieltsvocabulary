package com.ieltsvocabulary.Adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Model.TopicNameModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;


public class TopicNameAdapter extends RecyclerView.Adapter<TopicNameAdapter.ViewHolder> {

    Context context;
    ArrayList<TopicNameModel> listTopic;
    ArrayList<String> childItem;
    Integer currPoss;

    public TopicNameAdapter(Context context, ArrayList<TopicNameModel> listTopic, ArrayList<String> childItem) {
        this.context = context;
        this.listTopic = listTopic;
        this.childItem = childItem;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.topic_header_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        TopicNameModel topicNameModel = listTopic.get(position);
        holder.txtlremaing.setText(topicNameModel.getText());
//        holder.child_count.setText(childItem.size());


        if (currPoss != null) {
            if (currPoss == position && !topicNameModel.isExpanded()) {
                topicNameModel.setExpanded(true);
            } else {
                topicNameModel.setExpanded(false);
            }
        }

        int i = listTopic.get(position).getLayoutColor();
        switch (i) {
            case 1:
                GradientDrawable maincolorone = (GradientDrawable) holder.lnrremain.getBackground();
                maincolorone.setColor(context.getResources().getColor(R.color.red_color));
                GradientDrawable one = (GradientDrawable) holder.ltwo.getBackground();
                one.setStroke(2, context.getResources().getColor(R.color.red_color));
                break;
            case 2:
                GradientDrawable maincolortwo = (GradientDrawable) holder.lnrremain.getBackground();
                maincolortwo.setColor(context.getResources().getColor(R.color.bluer_green));
                GradientDrawable two = (GradientDrawable) holder.ltwo.getBackground();
                two.setStroke(2, context.getResources().getColor(R.color.bluer_green));
                break;
            case 3:
                GradientDrawable maincolorthree = (GradientDrawable) holder.lnrremain.getBackground();
                maincolorthree.setColor(context.getResources().getColor(R.color.diff_blue));
                GradientDrawable three = (GradientDrawable) holder.ltwo.getBackground();
                three.setStroke(2, context.getResources().getColor(R.color.diff_blue));
                break;
        }

        TopicNameChildItemAdapter topicNameChildItemAdapter = new TopicNameChildItemAdapter(context, childItem,i);
        holder.rcychilditem.setLayoutManager(new LinearLayoutManager(context));
        holder.rcychilditem.setAdapter(topicNameChildItemAdapter);

        boolean expanded = listTopic.get(position).isExpanded();
        if (expanded) {
            holder.ltwo.setVisibility(View.VISIBLE);
            holder.down_arrow.setRotation(90);
            GradientDrawable maincolorone = (GradientDrawable) holder.lnrremain.getBackground();
            maincolorone.setCornerRadii(new float[]{16, 16, 16, 16, 0, 0, 0, 0});
        } else {
            holder.ltwo.setVisibility(View.GONE);
            holder.down_arrow.setRotation(0);
            GradientDrawable maincolorone = (GradientDrawable) holder.lnrremain.getBackground();
            maincolorone.setCornerRadii(new float[]{16, 16, 16, 16, 16, 16, 16, 16});
        }

        holder.lnrremain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currPoss = position;
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return listTopic.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout lnrremain;
        TextView txtlremaing, child_count;
        RecyclerView rcychilditem;
        LinearLayout ltwo;
        ImageView down_arrow;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            lnrremain = itemView.findViewById(R.id.lnrremain);
            down_arrow = itemView.findViewById(R.id.down_arrow);
            ltwo = itemView.findViewById(R.id.ltwo);
            txtlremaing = itemView.findViewById(R.id.txtlremaing);
            rcychilditem = itemView.findViewById(R.id.rcychilditem);
            child_count = itemView.findViewById(R.id.child_count);
        }
    }
}
