package com.ieltsvocabulary.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Interface.AdapterItemClickCallback;
import com.ieltsvocabulary.Model.WordDataModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    Context context;
    ArrayList<WordDataModel> listWordData;
    AdapterItemClickCallback adapterItemClickCallback;
    Boolean allChecked;

    public DataAdapter(Context context, ArrayList<WordDataModel> listWordData, AdapterItemClickCallback adapterItemClickCallback) {
        this.context = context;
        this.listWordData = listWordData;
        this.adapterItemClickCallback = adapterItemClickCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_data, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.item_txt.setText(listWordData.get(position).getData());
        if (allChecked != null) {
            if (allChecked) {
                setLayoutAndCheckBox(holder, "for add", 2, "true");
            } else {
                setLayoutAndCheckBox(holder, "for remove", 3, "false");
            }
        }
        holder.main_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.chk.isChecked()) {
                    setLayoutAndCheckBox(holder, "for remove", 1, "false");
                } else {
                    setLayoutAndCheckBox(holder, "for add", 0, "true");
                }
            }
        });
    }

    private void setLayoutAndCheckBox(ViewHolder holder, String for_add, int i, String msg) {

        if (msg.equalsIgnoreCase("true")) {

            holder.chk.setChecked(true);
            holder.item_txt.setTextColor(Color.parseColor("#000000"));
            holder.view_one.setVisibility(View.GONE);
            holder.view_two.setVisibility(View.VISIBLE);
            adapterItemClickCallback.response(for_add, i);
        } else {

            holder.chk.setChecked(false);
            holder.item_txt.setTextColor(Color.parseColor("#bdbdbd"));
            holder.view_one.setVisibility(View.VISIBLE);
            holder.view_two.setVisibility(View.GONE);
            adapterItemClickCallback.response(for_add, i);
        }

    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return listWordData.size();
    }

    public void setAllChecked(boolean checked) {
        allChecked = checked;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox chk;
        TextView item_txt;
        LinearLayout main_lay;
        View view_two, view_one;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            chk = itemView.findViewById(R.id.chk);
            item_txt = itemView.findViewById(R.id.txt);
            main_lay = itemView.findViewById(R.id.lone);
            view_two = itemView.findViewById(R.id.view_two);
            view_one = itemView.findViewById(R.id.view_one);
        }
    }

}

