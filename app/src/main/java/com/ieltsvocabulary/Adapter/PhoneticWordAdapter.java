package com.ieltsvocabulary.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Interface.AdapterItemClickCallback;
import com.ieltsvocabulary.Model.PhoneticWordModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

public class PhoneticWordAdapter extends RecyclerView.Adapter<PhoneticWordAdapter.ViewHolder> {

    Context context;
    ArrayList<PhoneticWordModel> phoneticWordArrayList;
    private AdapterItemClickCallback clickCallback;

    public PhoneticWordAdapter(Context context, ArrayList<PhoneticWordModel> phoneticWordArrayList, AdapterItemClickCallback clickCallback) {
        this.context = context;
        this.phoneticWordArrayList = phoneticWordArrayList;
        this.clickCallback = clickCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.phonetic_word_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final PhoneticWordModel phoneticWordModel = phoneticWordArrayList.get(position);
        holder.word_image.setImageDrawable(context.getResources().getDrawable(phoneticWordModel.getDrawable()));
        holder.word_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickCallback.response(phoneticWordModel, 0);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return phoneticWordArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView word_image;
        CardView word_card;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            word_image = itemView.findViewById(R.id.word_image);
            word_card = itemView.findViewById(R.id.word_card);
        }
    }
}

