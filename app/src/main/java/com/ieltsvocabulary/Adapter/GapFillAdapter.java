package com.ieltsvocabulary.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ieltsvocabulary.Interface.MatchThePairsInterface;
import com.ieltsvocabulary.Model.SelectCorrectWordModel;
import com.ieltsvocabulary.R;

import java.util.ArrayList;

public class GapFillAdapter extends RecyclerView.Adapter<GapFillAdapter.ViewHolder> {

    Context context;
    ArrayList<SelectCorrectWordModel>listTheWord;
    MatchThePairsInterface matchThePairsInterface;
    int tempPosition = 0 ;

    public GapFillAdapter(Context context, ArrayList<SelectCorrectWordModel> listTheWord,
                              MatchThePairsInterface matchThePairsInterface) {
        this.context = context;
        this.listTheWord = listTheWord;
        this.matchThePairsInterface = matchThePairsInterface;
    }

    public GapFillAdapter() {
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.row_gap_fill, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        holder.txtText.setText(listTheWord.get(position).getMsg());
        if (listTheWord.get(position).isSelect()){
            matchThePairsInterface.fillColorInView(position,holder.crdOption,holder.txtText);
        }else{
            holder.crdOption.setBackgroundResource(R.color.white);
            holder.txtText.setTextColor(context.getResources().getColor(R.color.black_new));
        }
        holder.crdOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyDataSetChanged();
               listTheWord.get(tempPosition).setSelect(false);
               tempPosition = position;
               listTheWord.get(tempPosition).setSelect(true);

            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return listTheWord.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        CardView crdOption;
        TextView txtText;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            crdOption  = itemView.findViewById(R.id.crdOption);
            txtText  = itemView.findViewById(R.id.txtText);

        }
    }
}