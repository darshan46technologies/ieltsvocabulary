package com.ieltsvocabulary.Model;

public class WordDataModel {
    int data;

    public WordDataModel(int data) {
        this.data = data;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }
}
