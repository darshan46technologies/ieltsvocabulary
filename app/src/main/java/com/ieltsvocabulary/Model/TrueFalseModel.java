package com.ieltsvocabulary.Model;

public class TrueFalseModel {

    int msg,ans;

    public TrueFalseModel(int msg,int ans) {
        this.msg = msg;
        this.ans = ans;
    }

    public int getMsg() {
        return msg;
    }

    public void setMsg(int msg) {
        this.msg = msg;
    }

    public int getAns() {
        return ans;
    }

    public void setAns(int ans) {
        this.ans = ans;
    }
}
