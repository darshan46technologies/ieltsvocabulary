package com.ieltsvocabulary.Model;

public class PiHorizontalModel {
    int text,image;
    boolean flag;

    public PiHorizontalModel(int image, int text, boolean flag) {
        this.image = image;
        this.text = text;
        this.flag = flag;

    }

    public int getText() {
        return text;
    }

    public void setText(int text) {
        this.text = text;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
