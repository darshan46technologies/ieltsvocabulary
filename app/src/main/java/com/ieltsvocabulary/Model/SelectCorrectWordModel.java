package com.ieltsvocabulary.Model;

public class SelectCorrectWordModel {
    int msg;
    boolean isSelect;

    public SelectCorrectWordModel(int msg, boolean isSelect) {
        this.msg = msg;
        this.isSelect = isSelect;
    }

    public int getMsg() {
        return msg;
    }

    public void setMsg(int msg) {
        this.msg = msg;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }
}
