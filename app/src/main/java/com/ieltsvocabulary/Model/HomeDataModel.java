package com.ieltsvocabulary.Model;

public class HomeDataModel {

    int background, commonMesg, main_image_header, color_code, current_lay_state;
    int main_title;

    public HomeDataModel(int background, int commonMesg, int main_title, int main_image_header, int color_code, int current_lay_state) {
        this.background = background;
        this.commonMesg = commonMesg;
        this.main_image_header = main_image_header;
        this.main_title = main_title;
        this.color_code = color_code;
        this.current_lay_state = current_lay_state;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }

    public int getCommonMesg() {
        return commonMesg;
    }

    public void setCommonMesg(int commonMesg) {
        this.commonMesg = commonMesg;
    }

    public int getMain_image_header() {
        return main_image_header;
    }

    public void setMain_image_header(int main_image_header) {
        this.main_image_header = main_image_header;
    }

    public int getMain_title() {
        return main_title;
    }

    public void setMain_title(int main_title) {
        this.main_title = main_title;
    }

    public int getColor_code() {
        return color_code;
    }

    public void setColor_code(int color_code) {
        this.color_code = color_code;
    }

    public int getCurrent_lay_state() {
        return current_lay_state;
    }

    public void setCurrent_lay_state(int current_lay_state) {
        this.current_lay_state = current_lay_state;
    }
}
