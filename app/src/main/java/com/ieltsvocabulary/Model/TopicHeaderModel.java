package com.ieltsvocabulary.Model;

import com.ieltsvocabulary.Interface.TopicNameInterface;

public class TopicHeaderModel implements TopicNameInterface {

    String name;


    public TopicHeaderModel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getItemType() {
        return TopicNameInterface.TYPE_HEADER;
    }


}
