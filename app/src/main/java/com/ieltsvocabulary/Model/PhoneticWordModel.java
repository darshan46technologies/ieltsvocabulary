package com.ieltsvocabulary.Model;

public class PhoneticWordModel {
    int drawable;
    String word;

    public PhoneticWordModel(String word, int drawable) {
        this.drawable = drawable;
        this.word = word;
    }

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
