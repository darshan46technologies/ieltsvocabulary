package com.ieltsvocabulary.Model;

public class DataModel {
    int text;
    boolean flag;

    public DataModel(int text, boolean flag) {
        this.text = text;

    }

    public int getText(){
        return text;
    }

    public void setText(int text){
        this.text = text;
    }

    public boolean isFlag(){
        return flag;
    }

    public void setFlag(boolean flag){
        this.flag = flag;
    }


}
