package com.ieltsvocabulary.Model;

public class MenuItemModel {
    int icon,message;

    public MenuItemModel(int icon, int message) {
        this.icon = icon;
        this.message = message;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }
}
