package com.ieltsvocabulary.Model;

public class ListeningModel {
    String option;
    int tempColor;
    boolean selected;

    int message;

    public ListeningModel(String option, int message, boolean selected) {
        this.option = option;
        this.message = message;
    }

    public int getTempColor() {
        return tempColor;
    }

    public void setTempColor(int tempColor) {
        this.tempColor = tempColor;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public int getMessage() {
        return message;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
