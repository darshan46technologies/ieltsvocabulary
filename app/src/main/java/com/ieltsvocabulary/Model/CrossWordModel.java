package com.ieltsvocabulary.Model;

public class CrossWordModel {

    int position;
    char singleChar;

    public CrossWordModel(int position, char singleChar) {
        this.position = position;
        this.singleChar = singleChar;
    }


    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public char getSingleChar() {
        return singleChar;
    }

    public void setSingleChar(char singleChar) {
        this.singleChar = singleChar;
    }
}
