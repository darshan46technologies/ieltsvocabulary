package com.ieltsvocabulary.Model;

import com.ieltsvocabulary.Interface.TopicNameInterface;

public class TopicItemListModel implements TopicNameInterface {

    String name;
    boolean expanded;


    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public TopicItemListModel(String name) {
        this.name = name;
        this.expanded = false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getItemType() {
        return TopicNameInterface.TYPE_ITEM;
    }
}
