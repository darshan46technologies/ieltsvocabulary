package com.ieltsvocabulary.Model;

import org.json.JSONArray;

public class TopicNameModel {
    String text, data;
    int layoutColor;
    JSONArray jsonArray;

    boolean expanded;

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public TopicNameModel(String text, String data,int layoutColor) {
        this.text = text;
        this.data = data;
        this.layoutColor = layoutColor;

        this.expanded = false;
    }

    public int getLayoutColor() {
        return layoutColor;
    }

    public void setLayoutColor(int layoutColor) {
        this.layoutColor = layoutColor;
    }

    public JSONArray getJsonArray() {
        return jsonArray;
    }

    public void setJsonArray(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }


}
