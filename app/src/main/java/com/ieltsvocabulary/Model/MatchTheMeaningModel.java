package com.ieltsvocabulary.Model;

public class MatchTheMeaningModel {
    String option;
    int optionText;
    boolean isSelected = false;

    public MatchTheMeaningModel(String option, int optionText,boolean isSelected) {
        this.option = option;
        this.optionText = optionText;

    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public int getOptionText() {
        return optionText;
    }

    public void setOptionText(int optionText) {
        this.optionText = optionText;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
