package com.ieltsvocabulary.Model;

public class HintItemModel {
    String option, optionValue;

    public HintItemModel(String option, String optionValue) {
        this.option = option;
        this.optionValue = optionValue;
    }

    public void setOption(String option) {
        this.option = option;

    }

    public String getOption() {
        return option;
    }

    public void setOptionValue(String optionValue) {
        this.optionValue = optionValue;

    }

    public String getOptionValue() {
        return optionValue;
    }
}
