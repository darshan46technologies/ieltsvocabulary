package com.ieltsvocabulary.Interface;

import android.widget.ImageView;
import android.widget.TextView;

import de.hdodenhof.circleimageview.CircleImageView;

public interface MenuItemsInterface {
    public void items(int position, CircleImageView view, ImageView imageView, TextView textView);
}
