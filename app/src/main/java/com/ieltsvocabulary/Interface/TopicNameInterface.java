package com.ieltsvocabulary.Interface;

public interface TopicNameInterface {

    int TYPE_ITEM = 0;
    int TYPE_HEADER = 1;

    int getItemType();
}
