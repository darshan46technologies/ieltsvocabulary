package com.ieltsvocabulary.Interface;

import android.widget.TextView;

import androidx.cardview.widget.CardView;

public interface MatchThePairsInterface {
    public void fillColorInView(int position, CardView cardView, TextView textView);

}
