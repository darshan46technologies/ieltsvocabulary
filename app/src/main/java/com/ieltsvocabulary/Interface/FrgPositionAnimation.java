package com.ieltsvocabulary.Interface;

import android.app.Activity;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

public interface FrgPositionAnimation {

    public void frgposition(String flag, int currentPosition);
}
