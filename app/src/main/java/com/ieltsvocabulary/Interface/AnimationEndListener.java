package com.ieltsvocabulary.Interface;

public interface AnimationEndListener {
    void onAnimationEnd();
}