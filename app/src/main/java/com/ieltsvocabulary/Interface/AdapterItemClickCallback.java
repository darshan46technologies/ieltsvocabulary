package com.ieltsvocabulary.Interface;

public interface AdapterItemClickCallback<T> {
    void response(T t, int type);
}
